## Daily Notes

- Quantitative change to Qualitative change.
- From preview and review;

### 131107

#### case when

```sql
select 
    case 
    when id>10 then 'little'
    when id > 20 then 'middle'
    else large
    end as size 
from ..
```

#### concat

- `select concat(id, name) from ..`

#### current_date

#### current_timestamp

- CURRENT_TIMESTAMP returns the current date and time.

#### extract

- DATEPART allows you to retrieve components of a date.

#### day

- DAY allows you to retrieve the day from a date.

#### div

- a DIV b returns the integer value of a divided by b.

#### floor

- FLOOR(f) give the integer that is equal to, or just less than f. FLOOR always rounds down.

#### instr

- `INSTR('Hello world', 'll') -> 3 `

#### left

- `LEFT('Hello world', 4) -> 'Hell' `

#### len

- `LEN('Hello') -> 5  `

#### nullif

```sql
NULLIF(x,y) = NULL if x=y
NULLIF(x,y) = x if x != y   
```

#### position

```sql
POSITION('ll' IN 'Hello world') -> 3
```

#### quarter

- `select quarter(current_date - interval 9 month) ;`

#### replace

- `select replace('nihao', 'a', 'b');`

#### second

- `select second(current_timestamp);`

#### right

- `select right('ni hao, life!', 5);`

#### round

```sql
ROUND(2.7,0) ->  3
ROUND(2.71,1) ->  2.7
```

#### \+ interval

```sql
select current_date + 7 day from ...
select current_date + interval 2 month;
```

#### \%

- `27 % 2 -> 1`

#### \+

- `DATE '2006-05-20' + 7  -> DATE '2006-05-27'  `

#### coalesce - ifnull

- `select coalesce(price, 0) from ..`

#### sql: count - group by

- `select count(id) from .. group by ..`

#### sql: select sum()

- `select sum(money) ..`

#### select within select 

- `select id from (select id from ..)`

#### sql: in

- `id in (1, 2, 3)`

#### SQL:  like 

- `like 'abc%'`

#### SQL: between .. and ..

```sql
.. where area between 2 and 4;
```

### End

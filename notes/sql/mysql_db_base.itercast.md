### mysql 数据库字符编码设置

- `show character set` // 查看 mysql 支持的编码
- 不同的编码，以占用最小空间
- `show variables like 'character_set%'`  
查看当前 mysql 服务器默认编码

- mysql db default charset:

```sql
   character set: latin1
   collation: latin_swedish_ci
```

- 创建数据库时指定编码：

```sql
create database linuxset
   default character set utf8
   default collate utf8_general_ci;
```

- 修改一个已有数据库的编码
- `alter database linuxcast charcter set utf8 collate utf8_general_ci`
- 已有数据，可能显示不正常

- 设置数据库默认编码
- 修改 mysql 配置文件 my.cnf

```
[client]
default-character_set=utf8
[mysql]
default-character_set=utf8
[mysqld]
default-character_set=utf8
collation-server=utf8_general_ci
init-connect='SET NAMES utf8'
character-set-server=utf8

```


### mysql 简单的备份恢复

- mysqldump // 使用最为广泛
- `mysqldump -u root -p dbname > file.sql` // backup a db
- `mysql -u root -p dbname < file.sql` // restore a db
- drop table table_name

### mysql 权限管理基础

- mysql 权限控制包括两个阶段
   - 检查用户是否能连接
   - 检查用户是否具有执行动作的权限；

- mysql 授予权限可分为以下几个层级
   1. 全局层级
   2. 数据库层级
   3. 表层级
   4. 列层级
   5. 子程序层级

- mysql 通过 grant 授予权限， revoke 撤销权限
- grant all privileges on 层级 to 用户名@主机 identified by 密码

```sql
   grant all privileges on *(db).*(table) to 'sean'@% ...

   grant all ...           linuxcast.* to 'sean'@% ...

   revoke all privileges from sean;
```

- `'sean'@'%'` - % is all host // quote
- 精确的主机名或ip地址：www.linux.net or 192.168.1.1
- *.linuxcast.net - 使用通配符
- 指定网段： 192.168.1.0/255.255.255.0 192.168.1以下的都可以访问 
- 一般不建议使用 root 用户
- root 默认不支持远程登录，需要授权；

### mysql user management bases

- use mysql // 不要修改
- table user // user info.
- create user username identified by 'password' 
- rename user oldname to newname
- set password=password('new password'); // change current user
- set password for sean = password('sean');
- drop user sean

### sql lang base -4

- `distinct` for unique
- 逻辑组合
- 对结果进行排序

### SQL语言基础 - 3

- 于向表格中插入新的行

```sql
INSERT INTO 表名称 VALUES (值1, 值2,....)
INSERT INTO table_name (列1, 列2,...) VALUES (值1, 值2,....)
```

- 修改表中的数据

```sql
UPDATE 表名称 SET 列名称 = 新值 WHERE 列名称 = 某值
```

- 删除表中的行

```sql
delete from table_name where 列名称 = 某值
```

- delete one record

```sql
delete from person where lastname='adsf'
```

- delete all records

```sql
delete from table_name
```

### SQL语言基础 - 2

- DROP DATABASE LinuxCast;
- 重命名数据库没有直接的办法。
- 创建表格语法

```sql
CREATE TABLE 表名称
(
列名称1 数据类型,
列名称2 数据类型,
列名称3 数据类型,
....
)

```

- DESCRIBE Persons; // 查看一个表格结构
- 在表中添加列

```sql
ALTER TABLE table_name
ADD column_name datatype
```

- 删除表中的列

```sql
ALTER TABLE table_name 
DROP COLUMN column_name
```

- 改变表中列的数据类型

```sql
ALTER TABLE table_name
ALTER COLUMN column_name datatype
```


### SQL语言基础 - 1

- SQL 中最常用的 DDL 语句
      1. CREATE DATABASE - 创建新数据库
      2. ALTER DATABASE - 修改数据库
      3. CREATE TABLE - 创建新表
      4. ALTER TABLE - 变更（改变）数据库表
      5. DROP TABLE - 删除表
      6. CREATE INDEX - 创建索引（搜索键）
      7. DROP INDEX - 删除索引

- SQL 中最常用的 DML 语句
   1. SELECT - 从数据库表中获取数据
   2. UPDATE - 更新数据库表中的数据
   3. DELETE - 从数据库表中删除数据
   4. INSERT INTO - 向数据库表中插入数据

- 常用SQL语句分为以下大类
      Data Definition Language (DDL) // 数据库定义
      Data Manipulation Language (DML) // 数据库操作
      Data Control Language (DCL) // 授权，取消授权
      Transaction Control (TCL) // 事务控制

- SQL语言可以实现以下功能
      面向数据库执行查询
      可从数据库取回数据
      可在数据库中插入新的记录
      可更新数据库中的数据
      可从数据库删除记录
      可创建新数据库
      可在数据库中创建新表
      可在数据库中创建存储过程
      可在数据库中创建视图
      可以设置表、存储过程和视图的权

### MySQL数据库基本操作

- mysql -h localhost -u root -p
- DROP DATABASE linuxcast
- select version();
- select current_date;

### MySQL数据库安装及基本配置

- mysql community server - free
- mysql-server 服务端
- mysql-devel 库文件
- yum install mysql mysql-server mysql-devel
- mysqld
- server mysqld start
- 首次启动服务，初始化
- mysqladmin -u root password 'linuxcast'
- mysql -u root -p
- chkconfig mysqld on - mysql开机启动
- /etc/my.cnf - 配置文件
- /var/lib/mysql - 数据文件
- `CREATE DATABASE linuxcast`
- /var/log/mysqld
- 3306
- netstat -tupln

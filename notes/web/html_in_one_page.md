## HTML/XHTML in one page

### Standarte template (simple HTML/XHTML document)

```html
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

 <head>

  <title>... replace with your document's title ...</title>

  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

  <meta name="description" content="... replace with your document's description ..." />
  <meta name="keywords" content="... replace with your document's keywords ..." />
  <meta name="author" lang="en" content="... replace with your document's author ..." />
  <meta name="copyright" content="&copy; ... replace with your document's copyright ..." />
  <meta name="date" content="... replace with your document's date ..." />
  <meta name="reply-to" content="... replace with your reply adrress ..." />

  <link rel="stylesheet" type="text/css" href="... replace with your document's stylesheet location ..." />

  <style type="text/css">
   body { background: white; color: black}
   a:link { color: red }
   a:visited { color: maroon }
   a:active { color: fuchsia }
  </style>

  <script type="text/vbscript" src="... replace with your script's location ..."></script>
  <script type="text/javascript" src="... replace with your script's location ..."></script>

  <script type="text/javascript">
   <![CDATA[
   ... unescaped script content ...
   ]]>
  </script>

 </head>


 <body>
  ... replace with your document's content ...
 </body>

</html>
```

### Comments: `<!-- -->`

- `<!--This is a comments-->`

### Breaks

- `<br />`

###  Block-level grouping elements: `<div> </div>`

###  Headings: `<h1> </h1>`

```html
<h1>h1</h1>
<h2>h2</h2>
<h3>h3</h3>
<h4>h4</h4>
<h5>h5</h5>
<h6>h6</h6>
```

### Inline grouping elements

- `<span> </span>`

### Unordered list 

- `<ul><li> </li></ul>`

### Emphasis

- `<em></em>; <strong></strong>`

### Citation

- `<cite> </cite> <q> </q>`

```html
As <cite>Harry S. Truman</cite> said: <q lang="en-us">The buck stops here</q>.
```

### Definition list

- `<dl><dt> </dt> <dd> </dd></dl>`

```html
<dl>
 <dt>first term</dt>
 <dd>definition</dd>
 <dt>second term</dt>
 <dd>definition</dd>
 <dt>third term</dt>
 <dd>definition</dd>
</dl>
```

### Abbreviation

- `<abbr lang="en" title="World Wide Web">WWW</abbr>`

- `<acronym lang="en" title="North Atlantic Treaty Organization">NATO</acronym>`

### Code

- `<code>code</code>`

### blockquote

```html
<blockquote cite="http://www.mycom.com/tolkien/twotowers.html">
    They went in single file, running like hounds on a strong scent, and an eager light was in their eyes. Nearly due west the broad swath of the marching Orcs tramped its ugly slot; the sweet grass of Rohan had been bruised and blackened as they passed.
</blockquote>
```

### Select

```html
<select name="" id="">
 <option>element_1</option>
 <option>element_2</option>
 <option>element_3</option>
 <option>element_4</option>
 <option>element_5</option>
</select>
```

### Subscript

- `H<sub>2</sub>O`

### Superscript

- `E=mc<sup>2</sup>`

### Document changes

- `<ins></ins><del></del>`

### Font style

- `tt, i, b, big, small, strike, s, u`

### From

- http://www.html.su/

### End

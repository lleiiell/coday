## HTML Lang Base

### HTML列表标签

- 有序列表 'ol'
- 无序列表 `ul`
- 用户导航栏
- 定义列表 `dl, dt, dd` // dt is title, dd is desc 

### HTML文本标签

- `span` // 标记内容
- `blockquote` // 引用

```
<blockquote cite="http://www.linuxcast.net">
</blockquote>
```

- `pre` // code
- 'hr' // 分割线
- `p` // 段落
- 'h1, h2, h3' // 标题
- `b, i` // 改变文字显示

### HTML基本概念汇总

- 标记内容类型

### HTML架构

- keywords
- description

### HTML语言简介

- tag 标识

### Ref

- [HTML语言基础](http://itercast.com/library/3/course/44)

### end

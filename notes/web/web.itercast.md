## WEB - itercast

### 现代网站架构

- 现代网站几乎均为动态网站，内容均为动态生成，数据保存在数据库中；
- 动态代码由计算机执行后返回信息
- 动态代码的执行很消耗资源，相对静态网页要慢很多；
- 现代网站架构 = web服务 + 应用服务 + 数据库服务；
- web服务：实现http协议，处理静态网页；
- 应用服务：执行动态语言；

#### 常用服务

- web服务：apache iis nginx lighttpd
- 应用服务： tomcat php unicorn
- 数据库服务：mysql, postgresql, oracle, mongodb

### 静态网页与动态网页

- 标记和架构一般不变 - 静态网页

### web 基本概念

- uri // 统一资源标识符 // uniform resource 
- uri = url + urn
- urn // 统一资源名称 // isbn 0-486-27557-4 唯一标识
- http://(协议)www.linuxcast.net(domain)/cast_img/1.png(路径)
- file:///boot/kernel.img // 本地文件
- ftp://www.linuxcast.net/test.rar // ftp 服务器
- http - hypertext transfer protocol - 客户端，服务端，英文对英文
- w3.org

### end

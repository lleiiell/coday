## Website Safety

### AIDE 入侵检测系统

- advanced intrusion detection environment
- aide 主要对文件完整性进行检测
- 完整性主要包括，配置文件和二进制命令
- 如 ls 命令被更改，差不出问题所在
- permissions, inode number, user, group, file size ...

#### aide setup

- vi /etc/aide.conf
- yum install aide
- /etc NORMAL // # Next decide what directories/files you want in the database
- /var/log // 绝对不能监控， 应该监控不该变的
- init db
- `aide --init` 
- cd /var/lib/aide/ > mv aide.db.new.gz aide.db.gz
- 检查 aide 监控的文件
- `aide --check`
- update aide db // 改变文件后
- `aide --update`
- check aide periodic
- crontab -e
- `* 3 * * * /usr/sbin/aide --check | mail -s "aide report" linuxcast.net@gmail.com`

### 使用 iptables 防护网站安全

- linux 自带防火墙

#### iptables 策略

- 只允许特定流量通过，禁用其他流量
- 允许 ssh 流量
   - iptables -A INPUT -p tcp --dport 22 -j ACCEPT
- 允许 DNS 流量
   - iptables -I INPUT 1 -p tcp --sport 53 -j ACCEPT
   - iptables -I INPUT 1 -p udp --sport 53 -j ACCEPT
- 允许业务流量 // web
   - iptables -I INPUT 1 -p tcp --dport 80 -j ACCEPT 
- 禁用其他所有流量
   - iptables -A INPUT -j DROP
- show

```
[root@centos ~]# iptables -L
Chain INPUT (policy ACCEPT)
target     prot opt source               destination         
ACCEPT     tcp  --  anywhere             anywhere            tcp dpt:http 
ACCEPT     udp  --  anywhere             anywhere            udp spt:domain 
ACCEPT     tcp  --  anywhere             anywhere            tcp spt:domain 
ACCEPT     tcp  --  anywhere             anywhere            tcp dpt:ssh 
DROP       all  --  anywhere             anywhere          
```

#### iptables 命令

- iptables -L // show current iptables rules
- iptables -F // remove current iptables rules
- iptables -D INPUT 3  //删除input的第3条规则
- iptables -F INPUT   //清空 filter表INPUT所有规则  
- iptables -t nat -D POSTROUTING 1  //删除nat表中postrouting的第一条规则
- iptables -t nat -F POSTROUTING   //清空nat表POSTROUTING所有规则
- iptables -P INPUT DROP  //设置filter表INPUT默认规则是 DROP
- service iptables restart

#### 阻拦攻击流量

- netstat -tn // 输出所有连接服务器的信息
- netstat -tn | grep 106.187.96.29 | awk '{print $5}' | ask -F ":" '{print $1} | sort | uniq -c | sort -r -n'
- iptables -I INPUT -s 121.83.220.33 -j DROP // drop 某ip流量
- 一般 ip 临时禁止，除非定时有规律

### 安全防护基本概念

- 底层平台安全
- 网站程序安全

#### 底层平台安全

- ddos(拒绝服务) // 通过正常/非正常流量迫使服务器资源耗尽
   - 三次握手
   - 百万 tcp 连接；
   - 大量肉鸡，向特定服务器发起攻击
- 扫描攻击
   - 有什么服务，端口号，软件，IP地址
- 暴力破解
   - ssh // 尝试密码
   - 不使用root登录
   - 失败次数限制，禁用ip

#### 网站程序安全

- 注入攻击
- 代码漏洞
- 欺骗攻击
- xss 跨站攻击

### end

## HTML Div CSS

### 代码编写

- 一般设置固定宽度，不设置固定高度
- 浮动元素脱离文档流 // 背景消失
- `clear:both;` // 清除浮动
- `ul {list-style:none}` // 小原点消失
- `li {display:inline}` // li 行显示
- `a {color:gray; text-decoration:none;}` // 全局链接样式
- `ul {margin-top: 90px;}` // 外边距
- `li margin-right: 20px;`
- `a border:solid 1px white; padding: 4px;`
- `border-radius: 6px;` // 弧度
- `.log h1 font-size: 30px`
- 先把 div 框架搭起来
- `img alt`
- 设置内填充，div 宽度被撑宽
- `dashed` // 虚线
- 参差不齐的美感
- 美学角度，网页不要纯黑字体；

### 项目介绍

- 所有跟布局相关的都运用 `div`
- 跟内容相关 `html`

### end

## CSS Style

### 为 PHP 添加 SNMP 支持

- 编译 php 安装 snmp
- ./configure --with-snmp=shared

### Cacti 监控系统基础

- cacti 监控软件
- 基于 bs 架构
- service snmpd restart
- snmpwalk -v1 -c public 127.0.0.1 .1
- php -m // 查看php支持模块
- pkgs.org for php-snmp
- rrdtool - 动态图形绘制
- create crontab
- vi /etc/crontab
- */5 * * * * root php /var/www/html/cacti/poller.php > /dev/null 2>&1
- service crond restart
- rra data 目录

### 日志分析基础

- web log `/var/log/httpd/access_log`
- mail log `/var/log/maillog`
- every server, 大型app all should have log;
- linux 文本组合命令 for 日志分析
- `grep, wc, tr, sort, awk`
- `/var/log/secure` // 找出攻击信息
- `du -sh secure` // file size
- `wc -l secure` // total lines
- ssh port attack
- tail secure
- `sshd, tty=ssh rhost=61.236.64.56`
- `grep "authenticatio failure" secure | wc -l`
- `grep "authenticatio failure" secure | awk '{print $14}'` // 提取第14段
- `grep "authenticatio failure" secure | awk '{print $14}' | awk -F "=" {print $2} | sort -n | uniq -c | sort -n -c` 

### SNMP 协议介绍

- snmpv1
- snmpv2c
- snmpv3 > v3 支持加密
- net-snmp // 开源实现
- `yum install net-snmp net-snmp-devel net-snmp-libs net-snmp-utils`
- `yum install net-snmp*`
- `161, 162` // 使用端口
- `/etc/snmp/snmpd.conf`
- 使用协议 `udp`
- 使用 community 作为认证

#### community setup

- com2sec notConfigUser default linuxcast // 设置 security name 和 community
- 将设置的 security name 绑定到一个组中
- group notConfigGroup v2c/v1 notConfigUser 
- 指定组的权限到一个权限视图
- access notConfigGroup "" any noauth exact all none none
- 指定权限视图的权限 // 视图名取 all
- view all included .1(最顶层)      80

#### snmpget, snmpwalk

- snmpget 获取一个指定 oid 的值
- snmpget -v1(version) -c(password) linuxcast 127.0.0.1(server ip) 1.3.6.1.2.1.1.1.0
- snmpget -v 2c(version) -c(password) linuxcast 127.0.0.1(server ip) 1.3.6.1.2.1.1.1.0
- snmpwalk 获取从一个 oid 开始之后所有的值
- snmpwalk -v1 -c linuxcast 127.0.0.1 .1

### CSS层叠样式表基础

- 极简化原则
- 内容与样式分离

### end

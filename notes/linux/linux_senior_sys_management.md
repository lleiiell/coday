## Linux Senior System management

### Linux网卡绑定、子接口

- mii-tool eth0
- ethtool -i eth0
- ip 别名
- 一块网卡绑定多ip
- service NetworkManager stop 
- chkconfig NetworkManager off 永久禁用
- ip addr add ... 添加别名
- eth0:0 别名
- ip addr add 192.168.1.200/24 dev eth0 label eth0:0 - 别名创建
- /etc/sysconfig/network-scripts/
- vim ifcfg-eth0:0 - 重启有效

```
DEVICE=eth0:0
IPADDR=192.168.1.200
PREFIX=24
ONPARENT=YES
```

### 多网卡绑定

- 千兆网卡的速度最高是千兆
- 多块物理网卡绑定为一个逻辑网卡，绑定后支持并行
- 绑定后物理网卡不再直接使用


### Linux高级权限管理 - ACL

- UGO 权限模型缺陷
- access control list
- 对文件进行灵活的权限限制；
- `getfacl file` 查看文件的acl设置；
- `setfacl -m u:sean:rwx file` 
- `setfacl -x u:sean:rwx file` - 删除权限设置
- `setfacl -m g:sean:rwx file` 

### Ref

- [Linux高级系统管理](http://study.163.com/course/courseMain.htm?courseId=232008#/courseMain)

### end

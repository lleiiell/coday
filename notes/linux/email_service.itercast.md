## 电子邮件

- 存档功能
- 电子邮件服务器
- 司法依据
- mail user agent // mua
- mail transfer agent // mta
- mail deliver agent // mda
- mail receive agent // mra
- simple mail transfer protocal // smtp
- imap, pop3 负责从服务器抓取邮件 // pop3, imap
- imap 所有客户端操作，跟服务器端同步
- 邮件传输通过 smtp 协议实现
- sendmail，Postfix linux最主流的mta // sendmail, Postfix
- mta 只负责邮件传输
- **邮件保存** 是由mda完成的
- linux 默认保存目录 `/var/spool/mail/username`
- mda 实现 **垃圾文件处理**, **病毒扫描**
- **procmail**, **maildrop** linux最常用的mda
- mta mra
- **dovecot** - linux下使用最广泛的mra
- `pop3s imaps`
- **域** 不代表具体的主机
- `dns` 记录域的邮件服务器地址

### Postfix

- `rpm -q postfix`
- `cd /etc/postfix/` - set up page
- `service postfix start`
- `chkconfig --list | grep postfix`
- port 25 smtp
- `netstat -tupln | grep master` - postfix process
- `mail -vs 'subject' user@host`
- `cd /var/spool/mail/user`
- `/etc/postfix/main.cf` - main set up
- `postconf -d` - show default config
- `postconf -n` - show current config
- `postconf -e key=value`
- `host lleiiell.info`
- `postconf -e "inet_interfaces = all"` - listen all api
- `myhostname=training.linuxcast.net`
- `mydomain=linuxcast.net`
- `mydestination=$myhoustname,localhost,$mydomain,localhost` - can receive from these host
- `service postfix restart`
- 域伪装 username@hostname to username@domain-name
- `postconf -e "myorigin=$mydomain"`
- `postconf -e "masquerade_exceptions=root"` - except 
- `mynetworks = 127.0.0.0/8` - send email without auth.
- `postqueue -p` - email send queue
- `postqueue -f` - refresh send queue
- `tail -f /var/log/maillog` - mail log

### End

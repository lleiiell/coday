### BIND服务基础及域主服务器配置

- mostly used dns server
- berkeley internet name domain
- **bind** service is named
- `yum install bind bind-chroot bind-utils`
- dns use udp, tcp protocol default, ports are 53(domain), 953(mdc)

#### bind setup

- /etc/named.conf // main sets 
- /var/named/ // zone file
- if installed bind-chroot, these will change:
- /var/named/chroot/etc/named.conf
- /var/named/chroot/var/named
- chroot is change root, and so called 伪根, for safety
- no default sets
- sets refer to /usr/share/doc/bind-9.8.2
- all apps has doc at usr/share/doc/

```
options{
   listion-on port 53 {127.0.0.1;};
   directory "/var/named";
}
```

### DNS基础及域名系统架构

- 每个域名代表一个ip，而 dns 就是用来在ip与域名之间进行转换
- 计算机一般都是作为一个 dns 客户端使用，
- gethostbyname() // domain analytic
- /etc/hosts
- 127.0.0.1 lleiiell.com // ping

#### dig

- dig -t mx gmail.com // for mail server
- dig -x www.linux.net // reverse refer
- dig -t soa linx.net // for soa info

#### dns server type

- primary dns server (master)
- seconday dns server (slave)
- caching only server
- master has all sets of the domain, setup here
- slave for 冗余负载, sets sync from master
- cache for 负载均衡 and speed up query, LAN;
- zone, a zone has one domain info.

#### dns cmd

- host www.lleiiell.com
- dig www.lleiiell.com // more info than host
- host, dig query by /etc/resolv.conf, not /etc/nsswitch.conf

#### dns query

- query from right to left
- www.itercast.com. (. is root dns server)
- com, net, org, cn .. = top level server
- itercast = authoritative server
- www, mail, ftp = resource record
- dig +trace www.itercast.com
- recursive, iterative

#### resource record = rr

- name class type rdata
- www  IN    A    192.168.1.1 // IN is internet
- mail IN    A    192.168.1.1 // A is ipv4, AAAA ipv6
- server1 In CNAME www // 别名 server1.itercast.com = www...
-      IN    MX 10 mail.linuxcast.net. // mail xchange // for mail server
- RR type = A AAAA MX CNAME PTR SRV

#### set dns

- /etc/resolv.conf
 
#### 解析方式

- /etc/hosts, /etc/networks
- dns
- nis // now is rarely used
- set refer orders by `/etc/nsswitch.conf` 
- like `hosts: files dns`

### end

### postfix 邮件服务基础配置

### 邮件服务原理及架构

- mua - mail user agent  
as outlook, thunderbird ..

- mta - mail transfer agent  
mail transfer by smtp

- mda - mail deliver agent  
save email to server, spam and virus settle

- mra - mail recieve agent  
send email to user agent by imap or pop3

- smtp - simple mail transfer protocol  
email send by smtp protocol

- imap - internet message access  
user email agent fetch email from mail server by imap

- pop3 - post office protocol 3  
user email agent fetch email from mail server by imap

#### IMAP, POP3

- imap advantages:  
   - ue better;
   - sync with server;

#### MTA

all MTAs are tools according smtp;

popular apps at linux:  
sendmail, postfix

#### MDA

in linux, emails are saved at:  
`/var/spool/mail/username`

mda functions:  
save emails, prevent spam, virus

popular apps in linux:  
procmail(mostly), maildrop

#### MRA

popular apps:  
dovecot

MRA needs:  
imap, pop3, imaps, pop3s

### email address format

sean@mail.qq.com  
qq.com is domain  
mail is host

query the mx server:  
`dig -t mx qq.com`
#### postfix

- is **MTA**
- for improve sendmail
- advantages:
   - 3 times faster than sendmail
   - work well with sendmail
   - safer

check if installed: `rpm -q postfix`

default config at: `/etc/postfix/`  
and main config file is: `main.cf`

start postfix by `service postfix start`

`chkconfig postfix on`  
`chkconfig --list | grep postfix`

postfix use **port:25** of tcp

default listen *127.0.0.1:25*, just serve for current server user,  
`etstat -tupln | grep master`

send mail by:  
mail -vs "test mail from sean" sean@mail.centos  
hello, sean!  
(ctrl +d)

mails are saved at:  
`/var/spool/mail/username`

#### postfix setup

- show default config: `postconf -d`
- show current config: `postconf -n`
- change config: `postconf -e key=value`

   - listen all interface: `postconf -e "inet_interfaces = all"`

   - define two variables:  
     `postconf -e "myhostname = .."`  
     `............"mydomain = .."`

   - `mydestination = $myhostname, localhost.$mydomain, localhost`

   - username@domain instead of user@hostname:  
   `postconf -e "myorigin = $mydomain"`
   - ignore check:  
   `mynetworks = 127.0.0.0/8` 

#### postfix management

show current mail queue:  
`postqueue -p`

refresh current mail queue:  
`postqueue -f`

show mail service log:  
`tail -f /var/log/maillog`


### YUM软件管理

- yellowdog updater modified
- yum is rpm 的前端程序
- 自动解决依赖关系
- 引入仓库（repo）概念
- 仓库存储所有的rpm包
- 仓库可以是本地的，也可能是网络的（163，sohu...）

#### yum 仓库

- 配置文件 `/etc/yum.repos.d/`
- gpgcheck > rpm 校验, 生产环境中一般打开；
- yum 必需以repo后缀结尾；

#### yum 基本命令

- yum install soft
- yum remove soft
- yum update soft
- yum search soft
- yum list (all/installed/updates)
- yum info soft
- @ has installed
- yum whatprovides /etc/../filename

#### 创建yum仓库

- 创建rpm文件夹
- 创建索引文件 `createrepo`
- file:///etc/...
- yum clean all - clean 缓存
- comps.xml - 分组信息

### RPM软件包管理

- 开源软件一般直接以源代码形式发布；
- 程序源文件需要编译成二进制可执行文件，才能运行；
- 开源软件会有大量的依赖关系
- 大型软件编译时间长


#### 源代码基本编译过程如下

- ./configure
- make
- make install

#### rpm

- redhat package manage
- .rpm
- 追踪软件依赖关系
- 可升级
- 支持多平台，不同平台一般发布不同rpm包
- install by `rpm -i file.fpm`
- uninstall by `rpm -e file`
- update install by `rpm -U newfile.fpm`
- http install by `rpm -ivh http://www.../file.fpm`
- `-v` view detail info
- `-h` show 进度条
- install need 管理员权限
- `rpm -qa` show all installed
- `rpm -qi software` show software detail
- `rpm -ql software` show all files of this software
- `rpm -qf file` which soft set this
- 查询未安装文件 `-p` 

#### rpm 验证

- import 密钥 `rpm --import the-key`
- `rpm -K soft` check
- `rpm -V soft` checd installed soft

### Linux系统启动详解

- bios 自检
- mbr 引导
- grub 引导程序
- 加载内核
- 执行 init
- runlevel

#### bios

- bios 保存在主板 bios芯片中
- bios 笛一声，正常
- find 可启动设备

#### mbr

- 引导代码为前446字节
- bios 查找可启动设备后，执行其引导代码
- 加载引导程序

#### grub

- grub 是linux主流的引导程序
- grub 保存在 `/boot/grub`

#### kernel

- dmesg 查看启动时，内核输出信息；

#### init

- 操作系统启动的第一个进程
- 是所有进程的父进程
- 运行直到关机
- top
- linux 运行级别 3 多用户，5 多用户&图形
- 查看当前运行级别 runlevel
- init 3 切换运行级别
- 忘记root密码后，登录单用户级别，passwd密码

#### grub 加密

- `grub-md5-crypt`
- vi /boot/grub/grub.conf
- password --md5 ...


### Linux 命令行文本存储工具

- `grep` text search by keyword
- `find / -user sean | grep video`
- `-i` 忽略大小写
- `-n` show line number
- `-v` show the lines without keyword

#### 基于列文本处理 - 切割

- `cut -d: -f3 /etc/passwd` 分割符':', 只显示第3列；
- `-c` cut by char
- `cut -c2-6 file`

#### 文本统计

- `wc file`
- `-l` just lines
- `-w` just words
- `-c` just 字节数
- `-m` just 字符数

#### 文本排序

- `sort file` 
- `-r` 倒序
- `-n` sort by number
- `-f` 忽略大小写
- `-u` ignore repeat line

#### delete repeat line

- `uniq` - default 相邻重复行

#### file compare

- `diff file1 file2`
- `-b` ignore 空格影响
- `-u` 统一显示比较信息（一般用以生成patch文件）
- `diff -u file1 file2 > final.patch`

#### 处理文本内容

- `tr -d sensor < file` delete sensor words
- `tr 'a-z''A-Z' < file` 转换大小写

#### 搜索替换

- `sed 's/linux/unix/g' file` search 'unix' from file, replace with 'linux'
- `sed '1,50s/linux/unix/g' file` 
- `sed -e 's/linux/unix/g' -e 's/apple/Apple/g' file` 同时搜索替换多个 
- `sed -f sedfile file` 从文件执行
- `s` is search


### Linux多命令协作：管道及重定向

- 基本所有命令的返回都是纯文本的；
- 命令协作通过 **管道和重定向**
- shell数据流定义：`STDIN` **标准输入（键盘）**，`STDOUT` **标准输出（终端）**，`STDERR` **标准输出（终端）**； 
- STDIN is no.0, STDOUT is no.1, STDERR is no.2;
- 重定向 `>` 将STDOUT输出到文件，覆盖；
- 重定向 `<` 重定向STDIN；
- 重定向 `>> ` 将STDOUT输出到文件，追加；
- 重定向 `2> ` 将STDERR输出到文件，覆盖；
- 重定向 `2>> ` 将STDERR输出到文件，追加；
- 重定向 `2>&1 ` 将STDOUT, STDERR输出到文件；
- `echo 22 > try.php`
- `ls > try.php`

#### 管道

- 将一个命令的STDOUT，作为另一个命令的STDIN
- `find \ -user sean 2> /etc/null | grep video`

### Linux网络基础配置

- Ethernet 以太网
- the interface of Ethernet is named eth0(第一块网卡), eth1 ...
- `lspci` 查看网卡硬件信息
- `lsusb`
- `ifconfig -a` show 网卡 info.
- `ifconfig eth0`
- `ifup, ifdown eth0` open/close the interface
- test 网络连通性 `ping`
- `host www.baidu.com` 测试dns解析
- `dig www.baidu.com`
- show 路由表 `ip route`
- 追踪到达目标地址经过的网络路径 `traceroute www.baidu.com`
- use `mtr` for network quality `mtr www.douban.com`
- ping 网关ip，查看是否连通，从底层到外层，从自身到外部；


### 网络基础

- 一个ip地址，标识一个主机（或一个网卡接口）
- ip4 地址32位长， ip6 地址128位长 
- ip6 地址空间 2^128
- 32 = 网络部分 + 主机部分
- 子网掩码 确定网络部分和主机部分的分割
- 子网掩码 32位
- ...      为1的部分为网络部分
- 192.168.1.1/24 // 前24位为子网掩码，子网前24位都是相同的
- 比较ip的网络部分，区分所否是一个网络
- 同一个网络之内主机之间通信，需要mac地址 
- mac 网卡的硬件地址
- ARP // Address Resolution Protocol
- 不同网络之间通信 - 需要路由器
- 路由器 -> 转发功能
- 路由器有一张路由表
- 路由器接口 - eth0，eth1..
- 实现路由功能的就是网关，网关是跨区域通信；
- 每个域名对应一个ip地址；
- www(主机名)google(域名)com(类型)
- rent domain
- www just the one host. // other like mail/ftp host
- first search domain, then for host name.
- host name can be customed; www is accustomed 
- data is tranfered by ip;
- domain to ip by dns;
- dns = domain name system;
- dns is supported by dns server of 电信运营商;
- pc(www.google.com) -> dns server(google.com = ip) -> pc(get ip) -> use ip for data;
- can qq, but not web, dns wrong.

### Linux 扩展权限

- 文件夹默认都是有`x`权限，否则无法查看文件夹内容；
- `umask` 属性确定默认权限
- 目录默认权限 `777-mask`
- 文件目录权限 `666-mask`
- umask default 002
- `umask 022`
- `suid` // -rwsr-xr-x 1 root root 42824 Sep 13  2012 /usr/bin/passwd
- root 用户不受任何权限限制
- `ls -ld dir`
- 目录继承所属组 `sgid`
- `sticky` 
- set suid `chmod u+s file`
- set sgid `chmod g+s dir`
- set sticky `chmod o+t file`
- `suid = 4, sgid = 2, sticky = 1` 
- suid 通常设置给可执行文件

### Linux 权限机制

- `chown -R(递归) sean file/` - 更改文件所属用户
- `chgrp -R sean fiel/` - 更改所属组

#### 修改文件权限

- `chmod 模式 文件`
- u, g, o ，分别代表用户，组和其他；
- a 可代指ugo
- `+, -`代表加入或删除对应权限
- `r, w, x` 代表3种权限
- `chmod u+rw file` 所属用户加rw权限
- `chmod g-x file` 所属组减x权限
- `chmod a-x file`
- r=4, w=2, x=1
- r=2^2, w=2^1, x=2^0
- 660 = rw-rw----
- 数字修改权限，为一并修改ugo;

### Linux用户基础

- `id` 显示用户信息
- `/etc/passwd, /etc/shadow, /etc/group` 分包保存用户信息, 密码, 组信息
- `whoami` 显示用户名
- `who` 登录用户
- `w` 登录用户, 及在干什么
- `useradd sean` 添加用户
- `passwd sean` 创建密码
- 新建用户之初始化内容于目录: `/etc/skel`
- `usermod -u 777 sean` 修改用户id
- `usermod -l newuser olduser` 修改用户名
- `userdel -r user` 删除用户, 同时删除其家目录
- `groupdel` 类似 `userdel` 使用

####用户分组

- `groupadd group1`
- `groupadd group2`
- `useradd -G group1 user1`
- `useradd -G group2 user2`

### Linux下获取帮助

- 没必要记住所有东西, 养成查文档习惯
- `-h` or `--help`
- 百度不出来什么专业信息

#### man

- `man ls` 查看详细使用方式
- man 文档分为很多类型, 如 `man 1 ls`
- `man -k ...` 查看包含关键字的文档
- `/-l` 查找相关信息

#### info

- `info ls`
- 比 man 更详细的帮助文档信息

#### doc

- /usr/share/doc
- `nautilus .` 已图形界面打开当前目录

### Linux文件系统挂载管理

- 创建好文件系统后, 需要**挂载**到一个目录才能够使用
- 挂载到 `mnt` 目录

#### mount

- `mount /dev/sda3 /mnt` 挂载
- `mount` 显示挂载详情
- 只读 `ro`, 读写 `rw` 方式挂载
- `mount -o remont` 重新挂载
- `mount -o remont, ro /dev/sda3 /mnt` 多个参数, 使用 ","号分隔
- `sync` 不使用内存缓存, 用于数据需要很安全的时候
- 默认为 `async`, 使用缓存
- `atime`
- `noatime` 不更新文件访问时间

#### umount

- `umount dev/sdb1` === `umount mnt`
- `fuser -m /mnt` 查看哪些进程在占用
- `lsof /mnt` 列出打开的文件
- 配置 `/etc/fstab` 定义自动挂载的文件系统

### Linux文件系统

- 分区后并不能直接使用, 需要创建**文件系统**后才能使用
- 文件系统创建过程即为**格式化**
- 没有文件系统的设备, 成为裸设备 `raw`
- 常见的文件系统有 `fat32, ntfs, ext2, ext3, ext4, xfs, hfs`
- linux 默认不支持 `ntfs` 文件系统, 需要安装驱动
- `mke2fs` 用于创建文件系统
- `mke2fs -t ext4 /dev/sdb1` 创建 ext4 文件系统
- `-c` 建立文件系统时检查**坏损块**
- `ext2` 默认不带日志
- `mkfs` 创建文件系统
- `mkfs -ext4 /dev/sdb1` 不能精细化操作
- `dumpe2fs /dev/sdb1` 查看**文件系统类型**
- 带**日志的文件系统**出错后, 可以恢复
- 事务执行过程中出现故障, 日志恢复

#### e2label

- `e2label` 给文件系统添加标签
- `e2label /dev/sdb1 HLL` - 打标签, 标签建议大写

#### FSCK

- `fsck` 检查并修复损坏的文件系统, 如 `fsck /dev/sdb1`
- 检查文件系统时, 必须要先卸载
- 添加 `-y` 参数, 不需要提示, 直接修复
- 使用 `-t` 指定文件系统类型

### 使用fdisk进行磁盘管理

- **fdisk** 老牌分区软件
- 基于 MBR 的分区工具, 不支持 gpt
- `fdisk -l` 查看当前磁盘分区信息
- `fdisk /dev/sdb`
- extended 扩展, primary 主分区
- `+2G` 分配2G空间
- `n` 创建新分区
- `p` 打印结果
- `l` logical 逻辑分区
- `5 or over` 5或更高, 逻辑分区永远是从5开始
- `Hex code` 16制编码
- `m` 查看帮助
- 修改 id 选择分区类型, linux 类型是83, **交换分区**(虚拟内存), 为82
- 分区之后使用 `partprobe` 让内核更新分区信息
- `cat /proc/partitions` 查看分区信息

### 磁盘基本概念

- 机械硬盘, ssd,
- 磁盘转速
- 固态硬盘 flash读取结构
- 盘片
- 柱面 cylinder
- 扇区 sector
- 磁头, 每一个通常为512字节
- 磁盘一般命名, sda, sdb, hda, hdb; a, b 为第一块, 第二块;
- 传统ide硬盘hd
- 现代 sata接口 sda, sdb
- U盘等都被认为 sd..
- 分区, sda1, sda2 .. ; 设备名称 + 分区号
- 主流的分区机制, MBR + GBT
- MBR 传统分区机制, 通常bios之pc设备,
- 引导, efi 用以取代 bios;
- mbr 支持分区数量有限
- mbr 只支持不超过2T的硬盘;
- mbr 占据 头512字节
- mbr 最多创建4个主分区,  逻辑分区是使用扩展分区创建的;
- 主分区, 扩展分区, 逻辑分区;
- 扩展分区不能用, 只能基于其创建逻辑分区;
- scsi 分区, 最多创建15个分区 和 63个ide分区;
- gpt 支持超过2T的硬盘
- gpt 只用于支持uefi的硬件上
- **uefi** 用于取代 **bios**
- gpt **寻址空间**是64位, 只支持64位操作系统
- **windows7 64**位 支持 gpt
- mac, linux 都支持 gpt

### vim 文本编辑器

- :x = :wq
- :sh 切换到shell, ctrl + d 跳回到vim;
- :! 系统命令

### linux 常用命令

- utc 
- date -s "20:20:20" => 设置时间
- date +%Y--%d -> 格式化时间
- uptime 启动时间, 负载等;
- clock 硬件时钟
- head 文件头几行
- tail 尾几行
- more 翻页显示, 下翻页
- less 翻页显示, 上下翻页
- tail -f 追踪显示文件的更新; -n 指定显示的行数; 默认10行;
- lspci 查看pci硬件设备; -v 查看详细信息
- lsusb 查看usb设备;
- lsmod 查看系统加载模块, 即驱动
- shutdown -h 关闭计算机 -r 重新启动; now 立即关机; +10 10分钟后关机; 23:30 定时关机; 
- poweroff 关机
- reboot - 重启
- rar 有版权限制; zip 是开源;
- zip test.zip test
- du -sh file 查看文件大小
- unzip test.zip
- tar 用作打包归档, 并非压缩, 释放一个归档
- tar -cvf file.tar file 归档
- tar -xvf file.tar 释放归档
- tar -cvzf 调用gzip命令, 归档压缩
- locate 快速查找已建立索引的文件, 文件夹.. 索引默认每天更新一次.. 未索引文件找不着; updatedb, 手动更新数据库;
- find 查找位置 查找参数
- `find . -name *file*`
- find / -perm 777
- find . -name *.conf
- find . -type d 查找所有目录, l 所有链接;
- find . -name "a*" -exec ls -l {} \; => 查找出来的结果,作为参数,传递给 ls -l;
- find -group -user -ctime -size;

### 系统目录架构

- bin 可执行的二进制文件, 即命令
- bin 下命令, 所有用户都有权限执行
- boot 引导目录, 操作系统引导启动
- boot/vmlinux 操作系统内核文件; - 启动第一个加载;
- boot/grub引导文件  
- boot/inthird -> 驱动等..
- dev - 设备 - device - 硬盘, 网卡等硬件设备,  linux 所有硬件设备都被抽象成一个文件;
- dev/cdrom 光驱, fb 是软盘, sda就是硬盘, sda1就是硬盘的第一个区,... tty1等是终端 -> 底层的硬件, 包括显卡, 声卡等;
- etc 所有配置文件, 基本存文本, .conf结尾, 配置文件; 
- home 用户私有目录于home/sean
- lib library 所有相关库文件, .so 相当于win .dll文件
- media 挂载使用, 挂载目录, mnt 挂载U盘
- opt 用于装大型软件
- proc 实时信息, 加载在内存里,  虚拟数据.
- meminfo 内存信息;
- 每次启动都会创建新proc
- sbin super binary, 只有root用户才能使用, 危险命令
- tmp 文件会自动删除
- sys 系统文件
- usr 装的一般应用软件, 大型软件装opt
- var 装一般会变的东西, 如 log, 系统所有日志信息, 而bin下一般是不变的; 保存经常变化的信息; 服务器的信息;
- var/mail 邮件在var下;


### rm -ri 

- `-i` 提示是否删除;
- `rm -rif` - *f* is force, 掩盖i, 强制删除;
- Linux下删除文件, 几乎不可能恢复;
- 删除非空目录: rm -r ..

### cd - -> 上一个目录;

- root 目录的家目录 在'/'目录下
- 普通用户家目录在 /home 下

### ls -R

- 递归显示子目录结构
- `ls -ld` 显示目录信息;
- file 文件类型;

### 隐藏文件 大多是 配置文件

- 一般用户不需知道其存在;

### 名称最多可以为255个字符

- 除了正斜线外, 都是有效字符
- 通过touch命令可以创建一个空白文件, 或更新已有文件的时间
- 以 \.开头的文件为隐藏文件;

### /var

- /var/log
- /var/tmp

### linux目录 大小写敏感;

- window 目录 大小写 不敏感;
- Linux 路径使用 `/` 分割, window路径使用 `\`

### linux 文件系统是 单根树状结构;

### jobs

- 查看当前后台运行的作业
- `sleep 5000` 死5000s; 
- `ctrl + z` = 暂停; 
- `ctrl + c` 跳出;
- `bg 2`; -> 控制进程继续在后台运行;
- `fg 2` 进程拉倒前台运行;

### id - 显示当前用户信息;

- passwd 修改当前用户密码;
- bad password: it is based on your username;

### su - liangliang -> 切换到lianglaing用户

- su - = 切换到root用户
- su = 不跳转目录;

### bash shell 支持使用通配符;

- * 匹配零个或多个
- ? 匹配任意一个字符
- [0-9] -> 其中
- [^0-9] -> 其外
- ls c*
- ls j[abs]

### clear 清除当前屏幕

### tab不补充命令行参数;

### !! - 执行上一次命令

- 一般写脚本时, 使用;
- ! 字符 - 重复历史匹配命令;
- ! num;
- !?ea - 匹配如, clear
- !-n - 执行前面第几个命令
- esc . -> 上一个命令之参数

### firefox

- 单进程; Linux 为 多任务, 多进程终端;
- 终止当前命令 ctrl + c
- firefox & -> 命令放在后台运行;

### uname 底层内核信息

- `uname -r` = 内核版本号
- `uname --all` = `uname -a` - all

### ls = list

### 命令行 = 命令 + 选项 + 参数;

### terminal 是模拟终端

- 默认使用的命令行程序 是 bash;
- 提示符 $ 为普通用户, # 为root用户 

### shell分为cli和gui;

- linux gui 为 gnome
- linux cli 为 bash

### 不允许用户直接操控 kernel;

### Ref

- [Linux 入门基础] (http://study.163.com/course/courseMain.htm?courseId=232007)

### end

## Linux Daily Podcast

### dstat综合Linux系统监控软件

- iostat
- vmstat
- dstat by python
- `-c` c is cpu
- `dstat --list`
- `dstat -c --output linuxcast.csv`
- `dstat -c 2 5` 收集5次信息，间隔2s；

### iptables

- `iptables -L`
- `iptables -A INPUT -p tcp --dport 80 -m time --timestart 09:00 --timestop 18:00 -j DROP`

### optipng压缩PNG图片大小

- 无损压缩
- repoforge
- optipng file.png

### agedu分析Linux磁盘使用空间信息

- `du -sh` 
- du -sh /home/
- http://dl.fedoraproject.org/pub/epel/6/x86_64/ - install epel源 
- `agedu -s(scan) / `
- agedu.dat - 索引信息
- `agedu -w`
- `--exclude "*" --include "*.conf"` 排除与包括

### Linux下计算文件的MD5、SHA校验值

- 单向散列算法 
- md5sum file
- sha1sum file

### WinFF - FFmpeg图形化界面工具

- 视频转换 

### Linux下将HTML转换为PDF格式的文件

- wkhtmltopdf
- https://code.google.com/p/wkhtmltopdf/
- wkhtmltopdf-0.11.0_rc1-static-amd64.tar.bz2
- /usr/local/bin 移动到...
- `wkhtmltopdf-i386 --use-xserver http://w... filename.pdf` 

### 使用 netstat 监控网络连接信息

- netstat | less 分页显示
- tcp/udp 互联网链接
- `netstat -at` - t is tcp, a is all, u is udp
- udp 是无状态协议 
- `-l` 正在监听的程序
- `-c` c is continous
- `netstat -tn` 当前连接用户
- `netstat -tn | wc -l` 当前连接用户数
- `netstat -tn | grep :80 | wc -l` 当前连接port:80用户数
- `netstat -tn | grep :80 | awk '{print $5}' | awk -F ":" '{print $1}' | sort | uniq -r -n` 

### FFmpeg开源视频格式转换软件

- RepoForge 源
- libcppunit1.12... 依赖包 手动安装
- 需要很多编码，解码器
- `ffmpeg -i test.rmvb -threads 0 ...` 

### IPTraf - 命令行网络实时监控

- `ping 192.168.1.1 -f`
- 死亡之ping

### wireshark - Linux网络流量监控分析

- 网络抓包
- `yum install wireshark wireshark-gnome`

### Linux进程实时监控 - htop

- top 实时进程管理，界面非人性化
- ps 非实时 
- repoforge 互联网源
- 空格键，标记多个进程；
- nice 改变优先级
- `l` 进程打开的文件
- `s` 进程追踪，分析进程调用指令；

### Linux硬盘性能检测 - 高级

- bonnie++
- `bonnie++ -u root` 默认写8G文件...
- iozone
- `iozone -l 1 -u 1 -r 16k -s 8G -F tempfile`
- `-l` 指定线程 最小线程
- `-u` ...     最大线程
- `-r` 默认读取块大小, 参考相关应用
- `-s` 默认读取文件大小, 一般两倍内存大小
- `-F` temp file

### Linux硬盘性能检测 - 基础

- 硬盘io低
- `dd if=/dev/zero of=testfile bs=1M count=512 conv=fdatasync` - 从zero读取512M的数据，写入testfile, 数据直接写入硬盘，非内存；
- `echo 3 > /proc/sys/vm/drop_caches` - clear 缓存
- `haparm -t /dev/sda` - 管理员用户运行 `-t` 硬盘 `-T` 内存
- `dmesg | grep sata` 当前正在运行的硬盘信息

### 如何Linux下查看CPU信息

- `/proc/` - 存于内存，保存当前系统状态，进程信息
- `cat /proc/cpuinfo`
- `lscpu`

### 使用httperf对Web服务进行压力测试

- ab test 功能较简单
- https://code.google.com/p/httperf/
- tar -xvf httperf-0.9.0.tar.gz
- ./configure
- make - 编译
- sudo make install - 安装
- httperf --hog - 尽可能多的产生链接
- `httperf --hog --server=127.0.0.1 --uri=/index.html --num-conns=10000`
- `--wsess=10,10,0.1` 10个会话连接，10次请求，每一次请求间隔

### 监控Linux系统当前监听使用的端口

- 查看已打开的端口
- 查看哪些端口被打开，哪些程序在监听
- `netstat -tupln` 查看网络状态 - 列出所有当前系统正在监听的端口以及使用的协议；
- 0 0.0.0.0:3306
- :::22 ipv6
- `sudo netstat -tupln | grep 22` 管道

#### lsof

- 列出所有打开的文件
- `lsof -i` - 所有正在监听的端口

### 使用iotop实时监控系统磁盘IO

- 磁盘io是瓶颈
- top 任务管理器
- yum list | grep iotop
- `dd if=/dev/zero of=testfile` - test 

### Linux下创建定时任务

- atd
- crond
- ls -ld cron*
- 绿色的都是可执行文件, 一般都是脚本；
- `/etc/cron.darly` - 每天四点左右
- `cat crontab` - 创建语法 
- `crontab -e` - use default editor
- `* * * * * echo "this is lleiiell"` - 每分钟运行
- `*/5 * * * * echo "this is lleiiell"` - 每5分钟运行
- `crontab -l`
- `mail` - 查看

### 使用Nginx高性能Web服务器

- 并发高 参考50000
- 安装开发套件`sudo yum groupinstall "Development Tools"` 
- tar -xvf nginx-1.4.2.tar.gz

#### errors

- `./configure` // 检查配置文件
   - `yum list | grep -i PCRE`
   - checking for PCRE library ... not found
   - `./configure` // error: the HTTP gzip module requires the zlib library.
- make 编译
- `sudo make install` - install
- `cd /usr/local/nginx/` - 安装目录
- sudo service httpd stop

#### nginx.conf

- `worker_proccesser 1` - 默认运行进程
- `server{...}` // 每一个配置文件 

#### 启动

- `cd sbin/`
- `sudo ./nginx` 
- `sudo netstat -tupln | grep 80` - 查看80端口 - nginx 在监听

#### ab test

- `ab -c 2 -n 10000 http://127.0.0.1/index.html` - 参考 4000/s

#### stop nginx

- `ps aux | grep nginx` - 查看进程id
- `kill -s QUIT 6088`

### 使用ab对Web服务进行压力测试

- web 最大 **负载** 量测试
- `rpm -qf /bin/ab` 属于哪个软件包
- `httpd` is apache server
- `service httpd start`
- `systemctl status httpd.service`
- `systemctl status nginx.service`
- `ab -c 2(2 users) -n(次数) 10000 http://127.0.0.1/index.php`
- `time per request` // 每s请求数，一般20左右
- `failed reqeust`

### Ref

- [LinuxCast.net每日播客] (http://study.163.com/course/courseMain.htm?courseId=221001#/courseMain)

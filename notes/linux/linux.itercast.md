## Linux - Itercast

### apache服务配置 - HTTP认证

- 适用于安全性能要求不高的时候 // 用户名，密码直接在网络传播危险
- 创建apache密码文件
   - htpasswd -c(create)m /etc/httpd/.htpasswd sean
   - htpasswd -m /etc/httpd/.htpasswd sean
- 修改apache配置文件, 为特定目录配置认证

```
<Directory /var/www/linuxcast-1>
   AuthName "LinuxCast Auth"
   AuthType basic
   AuthUserFile /etc/httpd/.htpasswd # 绝对路径
   Require valid-user # 打开密码功能
</Directory>
```

- service httpd restart

### apache服务配置 - 虚拟主机

- 一个物理服务器只能服务一个网站
- 配置一台apache服务器为多个网站提供服务，即为虚拟主机
- 虚拟主机包括
   - 基于ip的虚拟主机
   - 基于域名的虚拟主机

> 一般使用基于域名的虚拟主机，但是如果需要搭建ssl的虚拟主机，
> 则必须使用基于ip的虚拟主机
- 一般网站往往不需要一个独立服务器的硬件资源，使用虚拟主机技术在一个服务器上运行多个网站可以节省成本

#### 基于域名的虚拟主机

- 确保dns服务器解析到同一ip -> `host www...`
- 在配置文件中添加一下配置 // 保存在最后

```
<VirtualHost *.80>
ServerName www1.....net
ServerAdmin root@linxu.net
DocumentRoot /var/www/linux-1
</VirtualHost>
```

- chown apache file // apache 访问权限

### apache服务基础配置

- 发行版默认支持
- 动态的，预创建的进程(多进程等待)
- 动态模块加载 // 扩展功能 -- 服务器无需重启
- 虚拟主机
- ssl主机 // https

#### 语法检查

- service httpd configtest
- apachectl configtest
- httpd -t

#### apache setup

- KeepAlive off // tcp 持续连接功能 - by KeepAliveTimeout 
- MaxKeepAliveRequests 100 // 对一个连接的请求限制
- KeepAliveTimeout 15
- Listen 80 // 默认监听端口 - Listen ip:80
- LoadModule auth_basic_module modules/mod_auth_basic.so
- User apache // apache 默认运行用户
- Group apache // 默认组
- ServerAdmin root@linuxcast.net // admin email
- DocumentRoot '/var/www/html'

```
<Directory '/var/www/html'>
</Directory>
```

- DirectoryIndex index.html index.html.var // 默认显示文件，从左向右寻找
- ServerName= www.linuxcast.net
- /etc/init.d/httpd restart - 重启apache

#### apache

- yum install httpd
- service httpd start
- 443 port // https
- 80 port // http
- /etc/httpd/conf/httpd.conf // 配置文件
- /etc/httpd/conf.d // 模块配置文件
- /var/log/httpd // log

#### 网页服务

- /var/www/html // 默认根目录

### Web服务 - Apache

- iis, apache, nginx, lighttpd // web server
- web service = web cliend + server;
- web server -> client by http
- most is http/1.1
- http use tcp protocol, port is 80

#### web server 架构

- web server(html) + 应用服务器 app server(apache, nginx) 

#### html 

- hypertext markup language
- html // 内容存储
- div // 网页架构
- css // 网页外观
- html形式的页面我们称之为静态页面
- asp, asp.net, jsp, php // 常见动态语言
- python, ror // 新兴，敏捷

#### http status

- 200 // 正常，请求成功
- 301 // 永久移动，一般用于域名重定向
- 304 // 未修改，一般用于缓存
- 401 // 禁止访问，未授权
- 403 // 已认证，但没有访问权限
- 404 // not found
- 500 // 服务器内部错误

#### http functions

- get
- post
- put // 向服务器提交数据
- delete // 删除指定资源
- head // 请求网页的头部信息

### 日志基本概念及rSyslog服务配置

- log 纯文本
- 内核信息，服务信息，应用程序信息
- rsyslog - centos6
- 监控围绕日志进行

#### rsyslog

- service rsyslog start
- service rsyslog status
- /etc/rsyslog.conf - 配置文件
- tail -f /var/log/secure
- tail -f /var/log/messages

##### setup

- kern.error /var/log/lernal.log
- mail.* /var/log/maillog
- `-/var/log/maillog` -> `-`表示不需要等待数据是否写到硬盘
- 邮件默认加短杠
- 将日志发送到统一的日志服务器
   - *.* @192.168.1.1 -- 使用udp协议发送 -- udp不可靠传输
   - *.* @@ip -- use tcp

#### facility

- 定义日志消息的来源

#### priority/severity level - 信息分级

- emergency - 系统已经不可用
- alert - 必须立即进行处理
- critical - 严重错误
- error - 错误
- warning - 警告
- notice - 正常信息，但较为重要
- informational - 正常信息
- debug - debug信息

### end

## Linux 远程管理

### common remote manage ways

- **RDP** (remote desktop protocol), at win

- **telnet** (cli), transfer by clear text

- **ssh** (secure shell), encryption transmission by RSA

- **RFB** (remote framebuffer), **VNC** (virtual network computing)

### SSH

- **SSH2** is mostly used, and asymmetric encryption

- **openssh** is popular in linux as ssh app

- ssh is **CS architecture**, cilent & server(sshd)

- `service sshd status`

- login by `ssh sean@192.168.1.103`

- **scp** (secure copy) data transmission by ssh between PCs  
`scp file sean@192..:/root/`

- **rsync** files synchronization by ssh between PCs  
`rsync *.* root@192.168.1.1:/root/`  
always used for backup

### VNC

- CS architecture

- tigervnc-server:  
`yum install tigervnc-server`

 - setup at '/etc/sysconfig/vncservers'
 - `VNCSERCERS="1:sean"`
 - `su sean`
 - `vncpasswd` - set vnc password for the default user
 - `service vncserver start`
 - `iptables -F` - close firewall

- tigervnc client:  
`yum install tigervnc`

- login: `192.168.1.1:1`

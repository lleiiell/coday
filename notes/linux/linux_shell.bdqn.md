## Linux Shell

### Shell命令的组合运用

- `&>, &>>` // 混合输出 
- `echo 1 && echo 2`
- `echo 1 || echo 2`
- `cd /boot/grub ; ls -lh grub.conf` - 先，后

### stdin

- `echo 1234 | passwd --stdin zhangsan` 管道
- `#!/bin/bash` // 执行环境说明 

```bash
#!/bin/bash
echo 'creating'
useradd zhangsan
echo 'passwding'
echo 123456 | passwd --stdin zhangshan
echo 'finish, okay!'
```

### end

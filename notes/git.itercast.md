### git remote repo

- 远程文件没有工作目录
- 远程仓库放的是 `.git`
- github
- bitbucket
- 搭建自己的 git 仓库

#### 远程仓库 - 访问协议

- local
- ssh // 可提交
- git // 效率高
- http/https // 开放

### git init and repo creation

- git config --list

### git 版本控制系统简介

- 速度块
- 有能力管理类似 linux 内核之超大规模系统
- git 原理：快照，而非保存区别（补丁）
- 几乎所有操作都是在本地执行
- git 使用 sha-1 算法识别文件变化
- 多数操作均为添加数据

#### 文件状态

- working directory - 工作区
- staging area - 暂存区
- git repository - 保存区

### 版本控制系统介绍

- vcs
- version control system
- 记录文件的所有历史变化
- 随时恢复到任何一个历史状态
- 多人写作开发或修改
- 错误恢复
- 多功能并行开发
- 版本控制系统分类
   - 本地版本控制系统 local vcs - rcs
   - 集中化版本控制系统 centralized vcs - cvs, svn
   - 分布式版本控制系统 distributed vcs - git, mercurial

#### cvcs

- 适合多人团队协作开发
- 代码集中化管理
- 单点故障
- 必须联网工作，无法单机本地工作

#### dvcs

- 适合多人团队协作开发
- 代码集中化管理
- 每个计算机都是一个完整仓库
- 可以离线工作

#### vcs 基本概念

- repository - 存放所有文件及其历史信息
- checkout - 取出或切换到指定版本的文件
- version
- tag - 里程碑 版本号

## Crazy Python

### 列表 list

- `a=[]` - type = list
- 切片 和 索引
- `a[0] = 'ad'` - 重新赋值
- `a.append(13)`
- `a.remove(13)`
- `help(list.remove)`
- `del(a[2])`
- 对象都有属性和方法
- 列表对象

### 元组

- 数据类型，序列
- 数字是可选的，冒号是必须的；
- len()
- `+` 序列拼接
- `*5` 重复5次
- `'c' in str1` 
- `max(), min()`
- `cmp(a,b)`
- `t=("1",2,3)` - 元组
- `a,b,c=("1",2,3)` - 元组
- `a,b,c=1,2,3` - 元组
- type() - tuple
- 元组中存的值不能随便更改；

### python 数据类型

- `type()`
- type: int, long 长整型
- a=123L - 强制定义为长整型
- 小数为浮点型
- c=3.14j -> type is complex 复数类型
- `""" """` - 三重引号，记录格式化
- `a='bcbsafd'` -> a[1]+a[2] - 字符拼接 - 每一个字符索引
- `a[1:4]` - 切片
- `a[:4], a[4:]` 
- `a[::1]` - 步进
- `a[-1]` 
- `a[-4:-1]`
- `a[-2:-4:-1]` -1 is 步长值

### 运算符和表达式

- 根据类型，调整运算的结果；
- `3.0//2` - 整除法
- `17%6` - 求余运算
- `3**2` - 平方运算
- `3***2` - 立方运算
- 关系运算符 - `<, > ...`
- `2>1 and/or 3>2` - 逻辑运算符
- `not 1>2`
- `1>>1 2>>2` - 移位运算符
- `raw_input()` - 获取用户输入，类型为字符串
- `int()`
- `raw_input('please input num1:')` - 输出提示

### python 变量

- `a=1`
- 变量存储在内存中；
- `id(a)` - 查看变量在内存中的地址编码；
- 变量重新赋值，内存地址改变；
- `a=1 b=1` => `id(a)=id(b)`

#### error

- NameError: name 'c' is not defined

### 开始编程吧

- 交互模式，文本模式
- `1.py` 格式
- `print 'hello'` 语法
- 异常模式
- `exit()`
- `#! /usr/bin/python` - 添加python命令路径
- py扩展名，由python程序解释，不需要编译；
- 编译后生成pyc的文件
- 模块中有很多方法
- `import py_compile` 导入模块
- 经过优化优化的源文件 - `pyo`
- `python -O -m py_compile 1.py`

### code

```python
#! /usr/bin/python
import py_compile

py_compile.compile('1.py'); // 生成 1.pyc

```

### Ref:
- [疯狂的Python：快速入门精讲] (http://study.163.com/course/courseMain.htm?courseId=302001#/courseMain)

### end

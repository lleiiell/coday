### LAMP 部署安装

- conf // 主配置文件
- conf.d // 模块配置文件，通常一个模块有一个配置文件
- service httpd start/restart
- ls -ld /etc/php.ini
- `/etc/php.d/` - php modules info
- yum install php-mysql 
- create database linuxcast_wp
- 隐私协议之搜索引擎索引 - 君子协议

#### httpd.conf

- ServerAdmin liangliang@163.com
- ServerName www.baidu.com
- DocumentRoot "/home/..."
- <Directory "/home/...">

### LAMP 及 LAMP 架构介绍

- mod_php - 调用效率高

### LAMP 优化简介

- php is 脚本语言，每次运行的时候编译
- request -> apache -> mod_php -> 编译并执行...
- php 加速器原理：编译并缓存php代码
- 耗时都花费在编译过程中

#### php加速器

- apc // alternative php cache
- eaccelerator // 综合性能较好 速度和内存使用率
- xcache

### PHP 性能优化 - 1

- yum groupinstall "D T"
- pkgs.org // php-devel
- rpm -qi php // 查看当前php版本
- rpm -ihv ..

#### install eaccelerator

- phpize // 必须在 eaccelerator-master 文件夹下运行
- yum list | grep php-devel
- ./confi...
- make 
- make install
- /etc/php.d/
- vim eaccelerator.ini
- configuration according homepage;

### PHP 性能优化 - 2

- apache 配置优化
- /etc/httpd/conf/httpd.conf
- ps aux | grep httpd // 查看已启动进程数

#### httpd.conf

- prefork 模式
- StartServers 8 // default 启动进程
- MaxClients 256

#### php.ini

- 最大2M // upload_max_filesize = 2M
- post_max_size = 8M
- memory_limit = 128M // 进程最大消耗内存
- 若超过内存负荷，可能使用交换分区或报错，此时效率下降

#### eaccelerator.ini

eaccelerator.shm_size="128M" // 共享内存大小

### LAMP 架构性能测试

- 10.1.1.1
- 物理 linux
- 无线网络 不能测试
- 购买千兆网线，连接两台电脑；
- wget 10.1.1.1/dl.rar // 85M/s // 带宽高
- ping 10... // 0.25 // 延时低
- ab // 压力测试 
- ab -c 10 -n 1000
- 静态网页 处理速度非常块 // 千次每秒
- httpd 多进程 // ab 测试时
- mysqld one // ab 测试时

### end

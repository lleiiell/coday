### LNMP架构介绍

- 高并发
- nginx use epoll 机制
- apache use select 机制
- 俄罗斯
- cpu 使用率不能高于60%；
- apache + mod_php + mysql
- nginx + php-fpm + mysql // web service + 应用服务 + db
- 网页静态版本性能很高
- php-fpm // php独立进程

### Nginx 服务安装

- yum remove httpd // 移除httpd
- yum groupinstall -y "Development Tools"
- ./configure // check bind
- make // 编译
- make install // 安装 /usr/local/nginx
- ps aux | grep nginx
- netstat -tupln

#### nginx.conf

```
server{
   listen 80;
   server_name www.linuxcast.net;
   location / {
      root html;

   }
}
```

### LNMP PHP-FPM 优化

- 不止一个 php-fpm 进程启动
- top
- ab test
- pm.max_children = 5 -> 50 // 最大进程 - 注意内存
- pm.start_servers = 2 -> 10 // default 启动数量
- pm.min_spare_servers = 10
- pm.max_spare_servers = 30
- killall php-fpm
- ps aux | grep php-fpm
- 提升高并发的响应时间

### LNMP PHP 加速

- eaccelerator
- /usr/local/php/bin/phpize 
- ./configure --with-php-config=/usr/local/php/bin/php-config
- php.ini
- ps aux | grep php-fpm
- kill -9 7518
- killall php-fpm
- kill all nginx
- /usr/local/nginx/sbin/nginx
- /usr/local/php/sbin/php-fpm

### LNMP 性能测试

### php-fpm 安装

- repoforge
- el6
- wget // download
- rpm -ivh // install
- yum clean all
- yum list // refresh yum cache
- yum list | grep php // check php installed
- yum remove php php-devel php-mysql // if any
- yum install -y libxml2-devel libjpeg-devel libpng-devel freetype-devel openssl-devel libcurl-devel libmcrypt-devel // 库文件，供php安装编译
- tar -xvf php // 解压 // 编译过程可能比较长
- ./configure // 需要很多参数，指定相关功能 // 编译生成makeinstall

```
./configure --prefix=/usr/local/php --with-config-file-path=/usr/local/php/etc --with-mysql=/usr/ --with-iconv-dir --with-freetype-dir ...
```

- yum -y install mysql mysql-server mysql-devel
- make // 
- make test
- make install
- cd /usr/local/php/sbin //php-fpm
- ../etc/php-fpm.conf.default -> php-fpm.conf
- php.ini-production -> php.ini
- default php-fpm 以服务形式存在
- listen = 127.0.0.1:9000
- ./php-fpm
- ps aux | grep php
- iptables -L
- /usr/local/nginx // conf // 注释

```
fastcgi_param SCRIPT_FILENAME /usr/local/nginx/html$fastcgi_script_name; // 网站根目录路径 // 使用绝对路径
```

- killall nginx // nginx 重启
- configure command ... // 编译参数
- server api // fpmfastcgi // 当前运行的模式是fpm

### end

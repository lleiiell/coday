## PHP.net

### reserved PHP variable

- $_SERVER is a special reserved PHP variable that contains all web server information.
- It is known as a superglobal.
- Several predefined variables in PHP are "superglobals", which means they are available in all scopes throughout a script.   

#### superglobal variables

- $GLOBALS // An associative array containing references to all 
variables which are currently defined in the global scope of the script.

```
   function test() {
       $foo = "local variable";

       echo '$foo in global scope: ' . $GLOBALS["foo"] . "\n";
       echo '$foo in current scope: ' . $foo . "\n";
   }

   $foo = "Example content";
   test();
```

- $_SERVER
- $_GET
- $_POST
- $_FILES // An associative array of items uploaded to the current script via the HTTP POST method. - uploaded vs post
- $_COOKIE // An associative array of variables passed to the current script via HTTP Cookies.
- $_SESSION
- `$_REQUEST` - An associative array that by default contains the 
contents of **$_GET, $_POST and $_COOKIE**.
- $_ENV


### end

- http://www.php.net/manual/en/ref.session.php session;
- http://www.php.net/manual/en/ini.core.php#ini.variables-order variable;
- http://www.php.net/manual/en/faq.using.php#faq.register-globals global;

## 细说PHP

### $_REQUEST

- $_REQUEST 的速度比较慢，不推荐使用
- contains `$_GET, $_POST, $_COKKIE`

### $_COOKIE

- setCookie() - set cookie to client

### $_SESSION

- session_start() - open session

### array

- array_search() - search array
- array_key_exists()
- array_flip() - exchange key with value
- array_reverse() 
- array_count_values() - value occur times

- array_filter() - array to callback
- array_walk() - array to callback

```php
array_walk($array, "myfun", "has the value")

function myfun($value, $key, $thirdpara){
}
```

- array_map()

### end

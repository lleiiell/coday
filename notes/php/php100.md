### CodeIgniter框架教程

- 轻量
- index.php 主入口文件
- 核心代码 - `systems`
- URI route -> 安全过滤 -> 控制器
- 控制器 -> view -> 缓存
- localhost/index.php(入口)/welcome(controller)/index(function)/a1(参数1)/a2（参数2）
- 形参

#### 视图

- 视图
- `$this->load->view('view_name')`
- `$data = array('a'=>$aaa);
- `$this->load->view('view_name', $data)`
- `$a` in view;`
- file_get_contents('./file.php');
- $file = fopen('./num.txt', 'w');
- fwrite($file, $count);
- fclose($file);
- 可调用多视图
- 在首个视图调入动态变量即okay！
- 模型是一个数据库类
- 一个模型针对一张表
- 重载构造函数
- 继承核心model；

#### model

- parent::_contstrut();
- $this->load->database();
- $this->load->model(modelname)
- $this->modelname->modelfunction($arr)

#### db

- $this->db->方法名()
- insert($tablename, $data); - data is array
- key is field name, value is the record;
- $this->db->where(字段名，字段值)
- $this->db->update(tablename，$arr)
- $this->db->delete(tablename)
- $this->db->select(fieldname)
- $query = $this->db->get(tablename);
- return $query->result();
- config/database

### PHP 与OAuth 接口

- `<?=$a?>` - 过时
- `if(): ... endif;` - 过时

### Apache Mysql 搭配与多站点配置详解

### 开启PHP学习之路

### Ref

- [PHP100视频教程2012版](http://study.163.com/course/courseMain.htm?courseId=224012)

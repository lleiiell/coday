### ajax 

### Bugzilla

> Bugzilla is server software designed to help you manage software development.

- http://www.bugzilla.org/

### doPDF

- doPDF is a free PDF converter for both personal and commercial use.
- print;
- http://www.dopdf.com/

### Firebug

- https://getfirebug.com/

### freemind

- http://freemind.sourceforge.net/wiki/index.php/Main_Page

### jquery

#### jquery cookie

- a simple, lightweight JQuery plugin for reading, writing, and deleting cookies;

#### jquery corner

- Corner is a simple plugin for creating rounded (or other styled) corners on elements;

#### jquery datepicker

- select a date from a popup or inline calender

#### jquery-file-upload

#### jquery-iframe-transport

#### jquery Metadata

- extract metadata from a dom element and return it as an object;

#### jquery Tag-it

#### jquery UI

#### jquery UI - Slider

- drag a handle to select a numeric value

#### jquery Validation

- provides drop-in validation for your existing forms;

#### jquery-visibility

### netbeans

- 快捷键

          错误提示：alt + enter
          格式化代码：alt+shift+F
          插入代码: Alt+insert 
          转到类: Alt+Shift+O  
          调试程序: Ctrl+Shift+F5 
          查找: Ctrl+F 
          shortcut: Ctrl+Delete 
          向右选中: Ctrl+Shift+Right 
          向左选中: Ctrl+Shift+Left 
          复制当前行: Ctrl+Shift+UP/DOWN 
          转到源: Ctrl+O或Ctrlt+单击 
          注释代码：ctrl+/
          增加空白行: ctrl+enter 
          自动完成字符串： ctrl+L ctrl+k
          导入所需包：ctrl+shift+i
          页面切换: Ctrl+Tab  
          运行程序: Shift+F6 

### node.js

### zsh

- Zsh is a shell designed for interactive use

### autojump

- A cd command that learns - easily navigate directories from the command line

### Firefox

- firefox-throttle - 限制 firefox 上传/下载速率工具;

### oh-my-zsh

- A community-driven framework for managing your zsh configuration.

### linux

#### dovecot

- Secure imap and pop3 server
- mra

#### maildrop

- mail delivery agent with filtering abilities
- mda

#### postfix

- Postfix Mail Transport Agent

#### procmail

- Mail processing program
- mda

#### sendmail

- A widely used Mail Transport Agent (MTA)

### Thunderbird

> Thunderbird is a free email application that's easy to set up and customize - and it's loaded with great features!

- http://www.mozilla.org/zh-CN/thunderbird/

### end

## PHP Lib

### 计算相对路径

```php
$a = 'aa/bb/cc/dd/a.php';
$b ='aa/bb/11/22/33/b.php';

//写一个函数,数出二个文件的相对路径。
function GetNum($variant,$variant2){
	$pth1 = null;
	$pth2 = null;
	$tmp = array();
	//分别判断路径下面的文件是不是存在.
	if(is_file($variant) && is_file($variant2)){
		$len1 = count($pth1 = explode('/',dirname($variant))); 	
		$len2 = count($pth2 = explode('/',dirname($variant2))); 
		$maxlen = max($len1,$len2);
		for($i=1;$i<$maxlen;$i++){
			if($pth1[$i] != $pth2[$i] && isset($pth1[$i])){
				if(isset($pth2[$i])) $tmp[] = $pth2[$i];
			}else{
				$tmp[] = $pth2[$i];
				$pathe .= '../';
			}
		}
		return $pathe.implode('/',$tmp).'/'.basename($variant2);
	}else{
		return '路径不合法!';
	}

}

print_r(GetNum($a,$b));
```

### php采集器

```php
//**************************************************************** 
$url = "http://book.sina.com.cn/nzt/lit/zhuxian2/index.shtml";// 图书地址 
$ver = "old"; //新旧版本 
//**************************************************************** 

$r = file_get_contents($url); //用file_get_contents将网址打开并读取所打开的页面的内容 
preg_match("/<meta name=\"description\" content=\"(.*?)\">/is",$r,$booktitle);//匹配此页面的标题 
$bookname = $booktitle[1];//取第二层数组 
$preg = '/<li><a href=(.*).shtml target=_blank class=a03>/isU'; 
preg_match_all($preg, $r, $zj); //将此页面的章节连接匹配出来 
$bookzj = count($zj[1]);// 计算章节标题数量 
if ($ver=="new"){ 
$content_start = "<!--正文内容开始-->"; 
$content_end = "<!--正文内容结束-->"; 
} 
if ($ver=="old"){ 
$content_start = "<\/table><!--NEWSZW_HZH_END-->"; 
$content_end = "<br>"; 
} 

header("Content-Type:text/html;charset=gb2312"); 

writer($bookname." 共".$bookzj."节\r\n帅哥刘并于".date("D M j G:i:s T Y")."为了毕业而设计小说整理收集\r\n", "./ailaopo/".$bookname.".txt","w+"); 
for ($i=0;$i<$bookzj;$i++) { 
//echo "http://book.sina.com.cn".$zj[1][$i]".shtml";die(); 
//用file_get_contents将章节连接打开并读取所打开的页面的内容 
$str = file_get_contents("http://book.sina.com.cn".$zj[1][$i].".shtml"); 
preg_match("/(<title>)(.*?)(<\/title>)/is",$str,$title);//匹配此连接页面的标题 
$title = str_replace("_读书频道_新浪网","",$title[2]);//把$title[2]里面有_读书频道_新浪网的换成空 
preg_match("/(".$content_start.")(.*?)(".$content_end.")/is",$str,$content);//匹配此连接页面的内容 
$content = preg_replace("/<(.*?)>/s","",str_replace("</p>","\r\n",$content[2]));//用str_replace把$content[2]里有</p> 的换成\r\n 
print_r($content); 
exit; 
//把第".($i+1)."节和标题与内容连接在一起放在变量 
$result = " \r\n第".($i+1)."节--------".$title."_汪老师就是帅 --------- \r\n".$content; 
writer($result, "./ailaopo/".$bookname.".txt","a+");//调用函数把$result 
echo "小说".$bookname."共".$bookzj."节，现在整理到第".$i."节 _".$title."<br>"; 
} 
echo "小说".$bookname."共".$bookzj."节 已全部整理完成！"; 

function writer($content,$url,$mode)//定义函数名 writer 参数$content,$url,$mode 
{ 
$fp = fopen($url, $mode);//打开文件$url 
fwrite($fp, $content);//把$content放入到$fp 
fclose($fp); //关闭$fp 
} 
```

### 实现php文件安全下载

```php
public function downloads($name){
		$name_tmp = explode("_",$name);
		$type = $name_tmp[0];
		$file_time = explode(".",$name_tmp[3]);
		$file_time = $file_time[0];
		$file_date = date("Y/md",$file_time);
		$file_dir = SITE_PATH."/data/uploads/$type/$file_date/";	
		
		if (!file_exists($file_dir.$name)){
			header("Content-type: text/html; charset=utf-8");
			echo "File not found!";
			exit; 
		} else {
			$file = fopen($file_dir.$name,"r"); 
			Header("Content-type: application/octet-stream");
			Header("Accept-Ranges: bytes");
			Header("Accept-Length: ".filesize($file_dir . $name));
			Header("Content-Disposition: attachment; filename=".$name);
			echo fread($file, filesize($file_dir.$name));
			fclose($file);
		}
}
```

### 图片下载

```php
<?php
function GrabImage($url, $filename = "") {
   if ($url == "")
      return false;
   if ($filename == "") {
      $ext = strrchr ( $url, "." );
      if ($ext != ".gif" && $ext != ".jpg" && $ext != ".png" && $ext != ".bmp")
         return false;
      $filename = date ( "dMYHis" ) . $ext;
   }
   ob_start ();
   readfile ( $url );
   $img = ob_get_contents ();
   ob_end_clean ();
   $fp2 = @fopen ( $filename, "a" );
   fwrite ( $fp2, $img );
   fclose ( $fp2 );
   return $filename;
}
GrabImage();
?>
<html>
<head>
<title>图片下载</title>
</head>
<body>
   <form method="POST" action="index.php">
      图片URL: <input type="text" name="url" size="80" /> <input type="submit"
         name="submit" value="提交" /><br />
<?php
if ($_POST ['submit'] != NULL) {
   $img = GrabImage ( $_POST ['url'] );
   if ($img)
      echo '<pre><img src="' . $img . '"></pre>';
   else
      echo "下载失败。";
}
?>
</form>
</body>
</html>
```

### 统计在线人数

```php
<?php
function getip() {
   if (getenv ( "http_client_ip" ) && strcasecmp ( getenv ( "http_client_ip" ), "unknown" ))
      $ip = getenv ( "http_client_ip" );
   else if (getenv ( "http_x_forwarded_for" ) && strcasecmp ( getenv ( "http_x_forwared_for" ), "unknown" ))
      $ip = getenv ( "http_x_forwarded_for" );
   else if (getenv ( "remote_addr" ) && strcasecmp ( getenv ( "remote_addr" ), "unknown" ))
      $ip = getenv ( "remote_addr" );
   else if (isset ( $_SERVER ["REMOTE_ADDR"] ) && $_SERVER ["REMOTE_ADDR"] && strcasecmp ( $_SERVER ["REMOTE_ADDR"], "unknown" ))
      $ip = $_SERVER ["REMOTE_ADDR"];
   else
      $ip = "unknown";
   return ($ip);
}

$user_online = 'count.txt';
touch ( $user_online );
$timeout = 30;
$user_arr = file_get_contents ( $user_online );
$user_arr = explode ( '#', rtrim ( $user_arr, '#' ) );
$temp = array ();
foreach ( $user_arr as $value ) {
   $user = explode ( ",", trim ( $value ) );
   if (($user [0] != getip ()) && ($user [1] > time ())) {
      array_push ( $temp, $user [0] . "," . $user [1] );
   }
}
array_push ( $temp, getip () . "," . (time () + ($timeout)) . '#' );
$user_arr = implode ( "#", $temp );
$fp = fopen ( $user_online, "w" );
flock ( $fp, LOCK_EX );
fputs ( $fp, $user_arr );
flock ( $fp, LOCK_UN );
fclose ( $fp );
echo "当前有" . count ( $temp ) . "人在线";
?>
```

### sendmail

```php
<?php
function postmail_jiucool_com($to,$subject = "",$body = ""){
    //Author:Jiucool WebSite: http://www.jiucool.com 
    //$to 表示收件人地址 $subject 表示邮件标题 $body表示邮件正文
    //error_reporting(E_ALL);
    error_reporting(E_STRICT);
    date_default_timezone_set("Asia/Shanghai");//设定时区东八区
    require_once('class.phpmailer.php');
    include("class.smtp.php"); 
    $mail             = new PHPMailer(); //new一个PHPMailer对象出来
    $body             = eregi_replace("[\]",'',$body); //对邮件内容进行必要的过滤
    $mail->CharSet ="UTF-8";//设定邮件编码，默认ISO-8859-1，如果发中文此项必须设置，否则乱码
    $mail->IsSMTP(); // 设定使用SMTP服务
    $mail->SMTPDebug  = 1;                     // 启用SMTP调试功能
                                           // 1 = errors and messages
                                           // 2 = messages only
    $mail->SMTPAuth   = true;                  // 启用 SMTP 验证功能
    // $mail->SMTPSecure = "ssl";                 // 安全协议
    $mail->Host       = "210.51.48.184";      // SMTP 服务器
    $mail->Port       = 25;                   // SMTP服务器的端口号
    $mail->Username   = "liangliang@pipapai.com";  // SMTP服务器用户名
    $mail->Password   = "pipa246437";            // SMTP服务器密码
    $mail->SetFrom('liangliang@pipapai.com', 'sean');
    $mail->AddReplyTo("liangliang@pipapai.com","sean");
    $mail->Subject    = $subject;
    $mail->AltBody    = "To view the message, please use an HTML compatible email viewer! - From www.jiucool.com"; // optional, comment out and test
    $mail->MsgHTML($body);
    $address = $to;
    $mail->AddAddress($address, "lleiiell");
    //$mail->AddAttachment("images/phpmailer.gif");      // attachment 
    //$mail->AddAttachment("images/phpmailer_mini.gif"); // attachment
    if(!$mail->Send()) {
        echo "Mailer Error: " . $mail->ErrorInfo;
    } else {
        echo "Message sent!恭喜，邮件发送成功！";
        }
    }
    
postmail_jiucool_com('tcll_22@163.com', 'love', 'you');

```

### file upload

```php
<?php

set_time_limit(0);
$target = basename($_FILES['uploaded']['name']);
if(move_uploaded_file($_FILES['uploaded']['tmp_name'], $target))
{
    echo "The file " . basename( $_FILES['uploaded']['name']) . " has been uploaded";
} else {
    echo "Sorry, there was a problem uploading your file.";
}
?>
```

```html
<html>
<body>
    <form enctype="multipart/form-data" action="__uploader.php" method="POST">
    Please choose a file: <input name="uploaded" type="file" /> <br/>
    <input type="submit" value="Upload" />
    </form>
</body>
</html>
```

### Defined or Die

```php
defined('_JEXEC') or die('Restricted access'); 
```

### postToURL

```php
function postToURL($theURL, $theData){
    try {
        $ch = curl_init();

        if (FALSE === $ch)
            throw new Exception('failed to initialize');

        curl_setopt($ch, CURLOPT_URL, $theURL);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $theData);
        //  curl_setopt($ch, CURLOPT_PROXY, '<proxy info goes here>:<port info goes here>');
        //  curl_setopt($ch, CURLOPT_PROXYUSERPWD, '<login goes here>:<password goes here>');

        $result = curl_exec($ch);

        if (FALSE === $result)
            throw new Exception(curl_error($ch), curl_errno($ch));

        curl_close($ch);
        
        return $result;

    } catch (Exception $e) {
        trigger_error(sprintf(
            'Curl failed with error #%d: %s',
            $e->getCode(), $e->getMessage()),
            E_USER_ERROR);
    }

}
```

### php random characters

```php
function genRandomPassword($length = 8)
{
        $salt = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $len = strlen($salt);
        $makepass = '';
 
        $stat = @stat(__FILE__);
        if(empty($stat) || !is_array($stat)) $stat = array(php_uname());
 
        mt_srand(crc32(microtime() . implode('|', $stat)));
 
        for ($i = 0; $i < $length; $i ++) {
                $makepass .= $salt[mt_rand(0, $len -1)];
        }
 
        return $makepass;
}
```

### validate email

```php
    function validateEmail($email){
        if(filter_var($email, FILTER_VALIDATE_EMAIL)){
            $host = explode('@', $email);
            if(!checkdnsrr($host[1])){
                exit(json_encode('Email invalid'));
            } else {
                exit(json_encode(true));
            }
            exit(json_encode($email)); 
            var_dump(filter_var($email, FILTER_VALIDATE_EMAIL)); 
        }else{ 
            exit(json_encode('Email invalid'));   
        }
    }
```

### end

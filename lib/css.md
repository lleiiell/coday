## CSS

### 灰色网站

```css
html{
   filter: grayscale(100%);
   -webkit-filter: grayscale(100%);
   -moz-filter: grayscale(100%);
   -ms-filter: grayscale(100%);
   -o-filter: grayscale(100%);
   filter: url("data:image/svg+xml;utf8,<svg xmlns=\'http://www.w3.org/2000/svg\'><filter id=\'grayscale\'><feColorMatrix type=\'matrix\' values=\'0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0 0 0 1 0\'/></filter></svg>#grayscale");
   filter:progid:DXImageTransform.Microsoft.BasicImage(grayscale=1);
   -webkit-filter: grayscale(1);
}  #grayscale"); filter:progid:DXImageTransform.Microsoft.BasicImage(grayscale=1); -webkit-filter: grayscale(1); }
```

### 梯形

```css
.css-arrow-multicolor {
    width: 50px;
    height: 50px;
    background: #2c3e50;
    border-color: #1abc9c #f1c40f #3498db #9b59b6;
    border-style: solid;
    border-width: 25px;
}
```

### 绘制“空心”三角形

```css
.arrow-bottom {
    position: relative;
    width: 0;
    height: 0;
    border: 20px solid #1abc9c;
    border-right-color:transparent;
    border-left-color: transparent;
    border-bottom-color: transparent;
}
.arrow-bottom:after {
    content:'';
    position:absolute;
    width: 0;
    height: 0;
    border: 18px solid #fff;
    right: -18px;
    top: -19px;
    border-right-color:transparent;
    border-left-color: transparent;
    border-bottom-color: transparent;
}
```

### 圆环

```css
.css3-ring {
    width: 100px;
    height: 100px;
    border: 10px solid red;
    border-radius: 70px;
}
```

### End

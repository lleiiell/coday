## Attack VS Preserve

### Zero-day attack

- an attack that exploits a previously unknown vulnerability 
in a computer application

#### preserve

- selinux

### SQL injection

- used to attack data driven applications, in which malicious 
SQL statements are inserted into an entry field for execution

#### preserve

- `magic_quotes_gpc = off` - deprecated
- Sets the `magic_quotes` state for GPC (Get/Post/Cookie) operations. 
When magic_quotes are on, all ' (single-quote), " (double quote), 
\ (backslash) and NUL's are escaped with a backslash automatically.
- http://php.net/manual/en/security.magicquotes.php

- addslashes()/stripslashes()

### Cross-site scripting(xss)

- XSS enables attackers to inject client-side script into Web pages 
viewed by other users.

#### preserve

- htmlentities()/htmlspecialchars() // 將使用者所提供的內容進行過濾

- 很多时候可以使用HTTP头指定内容的类型，使得输出的内容避免
被作为HTML解析。
- header('Content-Type: text/javascript; charset=utf-8');

- 通常来说，一個經常有安全更新推出的瀏覽器，在使用上會比
很久都没有更新的浏览器更为安全。

- 大多数瀏覽器皆有關閉JavaScript的选项，但關閉功能并非是最好的方法

### Denial-of-service attack (dos)

- distributed denial-of-service attack (DDoS attack)

- an attempt to make a machine or network resource unavailable 
to its intended users.

> 分散式阻斷服務攻擊（英文：Distributed Denial of Service，縮寫：DDoS）亦称洪水攻击。顧名思義，即是利用網絡上已被攻陷的電腦作為「殭屍」，向某一特定的目標電腦發動密集式的「拒绝服务」式攻击，用以把目標電腦的網絡資源及系統資源耗盡，使之无法向真正正常请求的用户提供服务。黑客通过将一个个“丧尸”或者称为“肉鸡”组成殭尸网络，就可以发动大规模DDoS或SYN洪水网络攻击，或者将“丧尸”们组到一起进行带有利益的刷网站流量、Email垃圾邮件群发，瘫痪预定目标受雇攻击竞争对手等商业活动。

> 有的朋友也许会问道："为什么黑客不直接去控制攻击傀儡机，而要从控制傀儡机上转一下呢？"。这就是导致DDoS攻击难以追查的原因之一了。做为攻击者的角度来说，肯定不愿意被捉到（我在小时候向别人家的鸡窝扔石头的时候也晓得在第一时间逃掉，呵呵），而攻击者使用的傀儡机越多，他实际上提供给受害者的分析依据就越多。在占领一台机器后，高水平的攻击者会首先做两件事：1. 考虑如何留好后门（我以后还要回来的哦）！2. 如何清理日志。这就是擦掉脚印，不让自己做的事被别人查觉到。比较不敬业的黑客会不管三七二十一把日志全都删掉，但这样的话网管员发现日志都没了就会知道有人干了坏事了，顶多无法再从日志发现是谁干的而已。相反，真正的好手会挑有关自己的日志项目删掉，让人看不到异常的情况。这样可以长时间地利用傀儡机。

### Ref

- http://www.ibm.com/developerworks/cn/security/se-ddos/

### end

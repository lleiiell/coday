## PHP

### list()

```php
$cmd = JRequest::getCmd('task', 'products.list');
list($control, $task) = explode('.', $cmd);
```

### time()

- 返回当前时间的 Unix 时间戳

```php
$t=time();
echo($t . "<br />"); // 1138618081
echo(date("D F d Y",$t)); // Mon January 30 2006
```

### chr()

- 从指定的 ASCII 值返回字符

```php
 echo chr(52); // 4
 echo chr(052); // *
 echo chr(0x52); // R
```

### mb_strlen 

- Get string length  
mb_strlen($input, 'ASCII');

### php

- 不能跳过可选参数的可选参数

```php
string htmlspecialchars ( string $string [, int $flags = ENT_COMPAT | ENT_HTML401 [, string $encoding = 'UTF-8' [, bool $double_encode = true ]]] )
string htmlspecialchars ( string $string [, int $flags = ENT_COMPAT | ENT_HTML401 ] )
```

### round

- Rounds a float

```php
echo round(3.4);         // 3
echo round(3.5);         // 4
echo round(3.6);         // 4
echo round(3.6, 0);      // 4
echo round(1.95583, 2);  // 1.96
echo round(1241757, -3); // 1242000
echo round(5.045, 2);    // 5.05
echo round(5.055, 2);    // 5.06
```

### preg_replace 

- 执行一个正则表达式的搜索和替换

```php
$string = 'April 15, 2003';
$pattern = '/(\w+) (\d+), (\d+)/i';
$replacement = '${1}1,$3';
echo preg_replace($pattern, $replacement, $string); // April1,2003
```

### strtr

- Translate characters or replace substrings

### rawurldecode

- 对已编码的 URL 字符串进行解码  
`echo rawurldecode('foo%20bar%40baz'); // foo bar@baz`

### rawurlencode 

- 按照 RFC 1738 对 URL 进行编码

```php
echo '<a href="ftp://user:', rawurlencode('foo @+%/'),
     '@ftp.example.com/x.txt">'; // <a href="ftp://user:foo%20%40%2B%25%2F@ftp.example.com/x.txt">
```

### urlencode()

- 除了 `-_.` 之外的所有非字母数字字符都将被替换成百分号（%）后跟两位十六进制数，空格则编码为加号（+）
- 此编码与 WWW 表单 POST 数据的编码方式是一样的;
- 此函数便于将字符串编码并将其用于 URL 的请求部分，同时它还便于将变量传递给下一页。

### htmlspecialchars_decode

- 把一些预定义的 HTML 实体转换为字符

### $GLOBALS 
- 引用全局作用域中可用的全部变量

```php
function test() {
    $foo = "local variable";
    echo '$foo in global scope: ' . $GLOBALS["foo"] . "\n"; // $foo in global scope: Example content
    echo '$foo in current scope: ' . $foo . "\n"; // $foo in current scope: local variable
}
$foo = "Example content";
test();
```

- set

```php
function config($key, $value) {
   $GLOBALS[$key] = $value;
}
```

### htmlentities

- 把字符转换为 HTML 实体

### array_walk_recursive(array,function,userdata)

- 对数组中的每个元素应用回调函数

```php
function sanitizeHTML( &$value ) {
    if (is_array($value)) {
       array_walk_recursive($value, 'sanitizeHTML');
    } else {
       $value = htmlentities((string)$value, ENT_QUOTES, 'UTF-8'); //Specify encoding for PHP<5.4
    }
    return $value;
}
```

- 返回值 true/false

```php
function funa(){
   return 1+1+1;
}
$a[] = array_walk_recursive($m, 'funa'); // true;
```

### htmlspecialchars

- 把一些预定义的字符转换为 HTML 实体

>  & （和号） 成为 &amp;
>  " （双引号） 成为 &quot;
>  ' （单引号） 成为 &#039;
>  < （小于） 成为 &lt;
>  > （大于） 成为 &gt;

### is_null

### json_encode

```php
$a = array(1=>'普通',2=>'熟练',3=>'精通',);
var_dump(json_encode($a)); 
// {"1":"\\u6b63\\u5e38","2":"\\u719f\\u7ec3","3":"\\u7cbe\\u901a"}
```

### strpos 

- find the postion of the first occurrence of a substring in a string

```php
$findme = '-';
$mystring = '江苏南京';
$pos = strpos($mystring, $findme);
var_dump($pos);
```

### json怎么做成[]形式?

```php
// 1. 为什么要做成[]形式?
// 2. hr 是否选择了 面试题 不需要做记录? online require?
$a = array("a"=> 1, "b"=> 1,);
$c = array(array("a"=> 1, "b"=> 1,));
class b{};
$b = new b();
$b->a = 1;
$b->b = 2;
var_dump(json_encode($c)); 
```

### json_decode - decodes a json string

```php
$json = '{"a":1,"b":2,"c":3,"d":4,"e":5}';
var_dump(json_decode($json));
var_dump(json_decode($json, true));
```

### json_encode 

- return the json representation of a value;

```php
$arr = array('a' => 1, 'b' => 2, 'c' => 3, 'd' => 4, 'e' => 5);
echo json_encode($arr);
```

### array_unique - remove duplicate values from an array

```php
$input = array("a" => "green", "red", "b" => "green", "blue", "red");
$result = array_unique($input);
```

### implode() 

- join array elements with a string

```php
$array = array('lastname', 'email', 'phone');
$comma_separated = implode(",", $array);
echo $comma_separated; // lastname,email,phone
```

### array_push 

- push one or more elements onto the end of array

### int

```php
$num="24linux"+6; //30
$num2=(int)("24linux"); //24
echo $num2;
```

### strtolower()

strtolower('HELLO WORK');

### str_replace()

`str_replace(' ', '_','do you love blue sky');`

### sprintf 

- Return a formatted string

```php
$num = 5;
$location = 'tree';
$format = 'There are %d monkeys in the %s';
echo sprintf($format, $num, $location);
```

### comma array

>  最后一个数组单元之后的逗号可以省略。通常用于单行数组定义中，例如常用 array(1, 2) 而不是 array(1, 2, ). 对多行数组定义通常保留最后一个逗号，这样要添加一个新单元时更方便。

```php
$options = array(
   'foo'    => 'foo',
   'spam'    => 'spam',
);
```

### curl

```php
// 1. 初始化
$ch = curl_init();
// 2. 设置选项，包括URL
curl_setopt($ch, CURLOPT_URL, "http://www.jb51.net");
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_HEADER, 0);
// 3. 执行并获取HTML文档内容
$output = curl_exec($ch);
// 4. 释放curl句柄
curl_close($ch);
```

### PostToUrl()

```php
function postToURL($theURL, $theData, $useProxy = false, $proxyAddress = 'proxynet.caldera.mgmmirage.org:8080'){
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $theURL);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $theData);

    if ( $useProxy ) {
        curl_setopt($ch, CURLOPT_PROXY, $proxyAddress);
        curl_setopt($ch, CURLOPT_PROXYUSERPWD, 'mgmmirage\ven.logiclink.lv' . ':' . 'Password1');
    }

    $result = curl_exec($ch);
    curl_close($ch);
    return $result;
}
```

### match time

`([0-9]{3}[1-9]|[0-9]{2}[1-9][0-9]{1}|[0-9]{1}[1-9][0-9]{2}|[1-9][0-9]{3})-(((0[13578]|1[02])-(0[1-9]|[12][0-9]|3[01]))|((0[469]|11)-(0[1-9]|[12][0-9]|30))|(02-(0[1-9]|[1][0-9]|2[0-8])))`

### PHP_EOL

```php
echo 'adsfadsfa asdfsd sd sdf asd fasd '.PHP_EOL.'asdfasdfasd asd fsd fsd ';
echo "adsfadsfa asdfsd sd sdf asd fasd <br /> asdfasdfasd asd fsd fsd ";
?>
asdfasd
asdfa
asd

```

### where is php modules?

- `which curl.so`
- `/usr/lib64/php/modules`

### $_SERVER

- 服务器和执行环境信息

#### PHP_SELF

- 当前执行脚本的文件名 

### php close warning

- `error_reporting = E_ALL & ~E_STRICT & ~E_NOTICE & ~E_WARNING`

### php query sql from file

- `mysql -u username --password=password -h localhost dbname < dumpfile.sql`

### curl

- wrong - `$server45Url = 'http://localhost:8080/logiclink/wo/';`
- right - `$server45Url = 'http://192.168.42.34:8080/logiclink/wo/';`

### html support php

- `AddType application/x-httpd-php .html` to apache

### mt_rand

- mt_rand() 比rand() 快四倍 

### checkdnsrr

- 检查指定网址的 DNS 记录。
- `bool checkdnsrr ( string $host [, string $type = "MX" ] )`

```php
$a = '163.com';
$a2 = 'adsasdf.sfsdf';
$a3 = 'baidu.com';
$b = checkdnsrr($a);
$b2 = checkdnsrr($a2);
$b3 = checkdnsrr($a3);
var_dump($b);   // true
var_dump($b2);  // false
var_dump($b3); // true
```

### end

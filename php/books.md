## PHP Books

### 网络最强PHP开发工具+电子书+视频教程等资料下载汇总

- http://club.topsage.com/thread-362964-1-1.html

### PHP Cookbook

- http://commons.oreilly.com/wiki/index.php/PHP_Cookbook

### PHP 5 Power Programming

- http://ptgmedia.pearsoncmg.com/images/013147149X/downloads/013147149X_book.pdf

### PHP Reference: Beginner to Intermediate PHP5

- http://www.phpreferencebook.com/

### Ref

- http://blog.longwin.com.tw/2013/02/free-ebook-php-2013/

### end

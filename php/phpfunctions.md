## PHP Functions

### array_change_key_case(array, case)
- changes all keys in an array

### array_diff

- computes the difference of arrays

### array_flip 

- 交换数组中的键和值

### basename()

- 返回路径中的文件名部分

### date_default_timezone_set 

- 设定用于一个脚本中所有日期时间函数的默认时区

### define(name, value, case_insensitive);

### error_log()

- 发送错误信息到某个地方

### eval()

- 把字符串按照 PHP 代码来计算

### explode

- split a string by string // implode

### filter_var()

```php
$email = 'zeentang@gmail.com';
$email2 = 'zeentang@gmailcom';

var_dump(filter_var($email, FILTER_VALIDATE_EMAIL)); // string(18), ..@gmail.com
var_dump(filter_var($email2, FILTER_VALIDATE_EMAIL)); // bool(false)
```

### floadval()

- 获取变量的浮点值

### gethostbyname()

```php
$ip = gethostbyname('www.example.com');
```

### get_include_path()

- 获取当前的 include_path 配置选项

```php
// 自 PHP 4.3.0 起有效
echo get_include_path();

// 在所有 PHP 版本中均有效
echo ini_get('include_path');
```

### glob()

- look for files with php suffix

```php
var_dump(glob('*.php'));
```

### implode

- join array elements with a string // explode

### ini_set

- ini_set — 为一个配置选项设置值

### isset

```php
$a = '';
$b = null;
var_dump(isset($a)); // true
var_dump(isset($b)); // false
var_dump(isset($c)); // false

```

### md5

- md5 — Calculate the md5 hash of a string

### move_uploaded_file

- 将上传的文件移动到新位置

### mysql_real_escape_string

-  转义 SQL 语句中使用的字符串中的特殊字符
- http://php.net/manual/zh/function.mysql-real-escape-string.php

### parse_ini_file()

- Parse a configuration file

### set_time_limit

> 设置允许脚本运行的时间，单位为秒。如果超过了此设置，脚本返回一个致命的错误。默认值为30秒，或者是在php.ini的max_execution_time被定义的值，如果此值存在。

### time 

- Return current Unix timestamp

### unset 

- 释放给定的变量

### end

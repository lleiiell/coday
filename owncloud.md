### route

`$this->create('hello_world', 'hw')->get()->action('helloword', 'hello'); `

> hello_world -> link name;
> hw -> url name
> helloword -> controller name
> hello -> controller method

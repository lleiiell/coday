### link

- `[lleiiell]: http://lleiiell.info`

### 水平线

- `----------`

### \$表示行内公式    

- 质能守恒方程可以用一个很简洁的方程式 `$E=mc^2$` 来表达

### \$\$整行公式

- `$$\sum_{i=1}^n a_i=0$$`
- `$$f(x_1,x_x,\ldots,x_n) = x_1^2 + x_2^2 + \cdots + x_n^2 $$`

### 加强的代码块，支持四十一种编程语言的语法高亮的显示，行号显示

非代码示例：

```
$ sudo apt-get install vim-gnome
```

Python 示例：

```python
@requires_authorization
def somefunc(param1='', param2=0):
    r'''A docstring'''
    if param1 > param2: # interesting
        print 'Greater'
    return (param2 - param1 + 1) or None

class SomeClass:
    pass

>>> message = '''interpreter
... prompt'''
```

JavaScript 示例：

``` javascript
/**
* nth element in the fibonacci series.
* @param n >= 0
* @return the nth element, >= 0.
*/
function fib(n) {
  var a = 1, b = 1;
  var tmp;
  while (--n >= 0) {
    tmp = a;
    a += b;
    b = tmp;
  }
  return a;
}

document.write(fib(10));
```

### 表格支持

```
| 项目     | 价格    |  数量  |
| ------   | -----:   | :----:  |
| 计算机  | $1600  |   5     |
| 手机     |   $12   |   12    |
| 管线     |    $1    |  234   |
```

### 定义型列表

```
名词 1
:    定义 1（左侧有一个可见的冒号和四个不可见的空格）
```

### 加粗

- `**加粗**`

### 斜体

- `_斜体_` 
- `*斜体*`

###  一级标题

- 这是一个一级标题
- `==============`
- 这是一个二级标题
- `--------------`

###  外链接

- 使用 \[描述](链接地址) 为文字增加外链接
- `这是去往 [本人博客](http://ghosertblog.github.com) 的链接。`

###  换行

- 在行末加两个空格表示换行

```
第一行(此行最右有两个看不见的空格)  
第二行
```

### 无序列表

```
- 无序列表项 一
- 无序列表项 二
- 无序列表项 三
```

### 有序列表

```
1. 有序列表项 一
2. 有序列表项 二
3. 有序列表项 三
```

### 文字引用

- 使用 > 表示文字引用
- `> 野火烧不尽，春风吹又生`

### 行内代码块

- 行内代码块
- 让我们聊聊 `html`

### 代码块

- 使用 4个缩进空格 表示代码块
- `    这是一个代码块，此行左侧有四个不可见的空格`

### 插入图像

- 使用 \!\[描述](图片链接地址) 插入图像  
- `![我的头像](http://tp3.sinaimg.cn/2204681022/180/5606968568/1)`

### 一级标题

- `#`

### 二级标题

- `##`

### code

```
``
```

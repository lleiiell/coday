## Joomla

- Joomla notes here;

### JPATH_COMPONENT

> The path to the current component being executed.

- http://docs.joomla.org/Constants

> These constants are defined in _path_/includes/defines.php`

### JURI 

- JURI::base() - returns the base uri of site;
- http://docs.joomla.org/JURI/base

``` 
$u = &JURI::getInstance();
$u->toString();
$u->setFragment('fragment');
JURI::getPath();
JURI::current();
JURI::base();
JRequest::getURI();
$u->getFragment();
```

### getConfig()

- `$config = JFactory::getConfig();`
- `return ($config->get('development_mode') === null || $config->get('development_mode') != 0);`

### getString

- `$key = JRequest::getString('key','');`

### getInt

- `$id = JRequest::getInt('id','');`
- `$db->loadColumn()` 
- `$db->loadResult()` // Method to get the first field of the first row 
of the result set from the database query

### execute the query in Joomla2.5

- `$db->query()`
- `$db->loadAssocList();`
- `$db->loadResult()` 
- returns the first column from a row or a single specified column 
to 'mysql_fetch_row()'

### getParams

- `$params = $app->getParams();`

- Easy way to get “params” from Joomla menu table
- http://stackoverflow.com/questions/4957889/easy-way-to-get-params-from-joomla-menu-table

### Submit form by joomla

- `submitform(pressbutton);`

### JRequest/getWord

- Fetches and returns a given filtered variable. The word filter only allows the characters [A-Za-z_].

### jquery noConflict

- `$document->addScriptDeclaration("jQuery.noConflict();");`

### Redirect

```php
global $mainframe;
$msg    = JText::_( 'Saved succeed' );
$mainframe->redirect( 'index.php?option=com_user&task', $msg );
```

### joomla setlayout

- `$this->view->setLayout('add');`

### joomla get post

- `$post = JRequest::get('post');`

### joomla getmodel

- `$model = &$this->getModel();`

### AssignRef

- `$this->view->assignRef('outdate',$outDate);`

### joomla registerTask

- $this->registerTask( 'add'  ,     'display'  );
- Register (map) a task to a method in the class.
- http://docs.joomla.org/API15:JController/registerTask

### joomla getTask

- `switch($this->getTask())`
- Get the last task
- http://docs.joomla.org/API15:JController/getTask

### joomla setVar

- `JRequest::setVar( 'hmenu', 1 );`
- http://docs.joomla.org/JURI/setVar

### joomla checkToken

- `echo JHtml::_('form.token');`
- The above code will output the return value of JHtmlForm::token
- Checks for a form token in the request
- `JRequest::checkToken() or jexit( 'Invalid Token' );`
- http://docs.joomla.org/API15:JRequest/checkToken

### Jversion

```php
$version = &new Jversion;
var_dump($version);exit();
```

### getPageTitle

```php
$title = & Jfactory::getApplication();
echo $title->getPageTitle();
```

### setPageTitle

```php
    $title = & Jfactory::getApplication();
    $title->setPageTitle('Hello ');
    echo $title->getPageTitle();
```

### 数据库驱动的名字

```php
$dbo = &Jfactory::getDBO();
$db = $dbo->name;
echo $db;
```

### 返回当前的数据库是否连接成功.

```php
$dbo = & Jfactoty::getDBO();
$db = $dbo->connected();
echo $db;
```

### 文件语言的设置.

```php
$doc = & JFactory::getDocument();
$do = $doc->language;
echo $do;
```

### 返回文档的字符集

```php
$doc = & Jfactory::getDocument();
$doc->getCharset();
echo $doc;
```

### 编辑器插件的名字.

```php
$editor = &Jfactory::getEditor();
$edr = $editor->_name;
echo $edr;
```

### 列出被装载的语言文件.

```php
$lan = & Jfactory::getLanguage();
$language = $lan->_paths;
print_r($language);
```

### 当前语言环境

```php
$lan = & Jfactory::getLanguage();
$language = $lan->getLocale();
print_r($language);
```

### 未使用的 session 的最大时间

```php
$session = & Jfactory::getSession();
$se = $session->_expire;
echo $se;
```

### 检查会话是否是当前创建的.

```php
$new = & Jfactory::getSession();
$new1 = $new->isNew();
if($new1){
    echo “new”;
} else {
    echo “old”;
}
```

### 显示 uri.

```php
$temp = & Jfactory::getURI();
$uri = $temp->_uri;
echo $uri;
```

### 请求返回根 URI.

```php
$temp = & Jfactory::getURI();
$tem = $temp->root();
echo $tem;
```

### 得到用户参数..

```php
$temp = & Jfactory::getUser();
$tem = $temp->getParameters();
print_r($tem);
```

### 得到用户表对像.

```php
$temp = & Jfactory::getUser();
$tem = $temp->getTable();
print_r($tem);
```

### 读取 xml 文件.

```php
$xmldoc = & Jfactory::getXMLParser('Simple');
$xmlstring = "<product>
                <name>Telephone</name>
                <price>$29.95</price>
                <manufacturer>CAMNAI Manufacturing Company</manfacturer>
             </product>";
if ($xmldoc->loadString($xmlstring)){
echo "String sucessfully loaded!";
} else {
echo "Something didn't work!";
}

### 用于产生网站链接地址.当启用了 SEF,生成的链接就是重写后的地址,反之则为原地址

```php
$temp = & new JRoute;
$url = “index.php?Itemid=29”;
$tem = $temp->_($url,true);
echo $tem;
```

### 把$html 中的内容增加到 head 块.

```php
$doc =& Jfactory::getDocument();
$html = "<style>.abacsdf{color:red;}</style>";
$doc->addCustomTag($html);
```

### 增加一个 meta tag 到当前运行界面的 head 块.

```php
$doc =& Jfactory::getDocument();
$doc->setMetaData(“metatagname”,”metatagcontent”);
```

### 在配置文件 configuration.php 中得到相应变量的值.

```php
$temp =& Jfactory::getApplication();
$tem = $temp->getCfg('ftp_host');
echo $tem;
```

### 判断是否后台接口.

```php
$temp =& Jfactory::getApplication();
$tem = $temp->isAdmin();
echo $tem;
```

### 判断是否前台接口.

```php
$temp =& Jfactory::getApplication();
$tem = $temp->isSite();
echo $tem;
```

### 登录认证函数.

```php
$temp =& Jfactory::getApplication();
$usr = Array('username'=>'admin','password'=>'adsmin');
$tem = $temp->login($usr);
if($tem){
echo 'success to login';
} else {
echo 'fail to login';
}
}
```

### 登出认证函数.

```php
$temp =& Jfactory::getApplication();
$tem = $temp->logout();
if($tem){
echo "“logout”";
} else {
echo "“not logout”";
}
```

### 按顺序显示堆栈模型.

```php
jimport('joomla.application.component.controller');
$temp =& new Jcontroller;
print_r($temp->_path)
$temp->addModelPath(JPATH_COMPONENT.DS.'othermodel');
print_r($temp->_path);
```

### 检查用户是否有权限执行指定的任务.

```php
$temp =& JFactory::getUser();
if($user->authorize('com_content','edit','content','all')){
echo “you may edit all content.”;
}else {
echo “you may not edit all contnet.”;
}
```

### Joomla image modal

```html
JHTML::_('behavior.modal', 'a.modal');

<a class='modal' href='..'><img width='60px' src='..'></a>
```

### End

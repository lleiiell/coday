## New Knowledge

- How long will new knowledge be?
- From Qs;

### sql server convert()

```sql
select pk_id, CONVERT(char(10), DF_OrderTime,120) as DF_OrderTime, CONVERT(char(10), DF_PayTime,120) as DF_PayTime, DF_PosName,DF_waiterName,DF_Yjine, DF_PayJine,DF_DishPresentJine,FK_Status 
from tbl_foo_opmaster 
where DF_PayDate >= '2013-10-21' and DF_PayDate <= '2013-10-28' 
order by pk_id
```

### json gbk

> 有很多情况下我们会遇到项目的编码是gbk（gb2312）的情况，
大家都知道php的jsonencode函数只支持utf编码，转换gbk编码会把汉字转换为null

- iconv

```php
    function gbk2utf8($data)
    {
        if(is_array($data))
        {
            return array_map('gbk2utf8', $data);
        }
        
        return iconv('gbk','utf-8',$data);
    }
```

### php parse_ini_file()

### dirname

- `require_once(dirname(__FILE__)."../../include/cfg.php");`

### is_object

```php
if(is_object($value))
{
    $values[]= "'" . $value->date . "'";
}
```

### file_put_contents()

```php
$now = date('Y/m/d H:i:s', time());
file_put_contents($time_file, $now);
```

### file_get_contents() return

- $homepage = file_get_contents('http://www.example.com/');

### timestamp null

- `DF_AuthorTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
- `DF_AuthorTime` datetime NULL DEFAULT NULL ,

### mysql datetime

- `1292 - Incorrect datetime value: '' for column 'DF_AuthorTime' at row 1`

```php
function db_quote($v){
   return isset($v) ? "'".$v."'" : 'null' ;
}
```

### php date()

- $now = date('Y/m/d H:i:s', time());


### PDO:mysql

```php
$host = "127.0.0.1";
$dbname = "cooker_cutter_pos";
$user = "root";
$pass = "root";

try {
    $db = new PDO("mysql:host=$host;dbname=$dbname", $user, $pass);
} catch (PDOException $e) {
    print "Error!:". $e->getMessage();
    die();
}
```

### SQL Server: compare by char time

```sql
select pk_id, CONVERT(char(10), DF_OrderTime,120) as DF_OrderTime, CONVERT(char(10), DF_PayTime,120) as DF_PayTime, DF_PosName,DF_waiterName,DF_Yjine, DF_PayJine,DF_DishPresentJine,FK_Status 
from tbl_foo_opmaster  
where DF_PayDate >= '2013-05-01' and DF_PayDate <= '2013-05-06' 
order by pk_id
```

### mysql ddl

```sql
CREATE TABLE `location` (
  `id` int(11) DEFAULT NULL,
  `name` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
```

### mysql restore db

- `mysql -u root -p cooker_cutter_pos < cooker_cutter_pos.sql`

### mysql create db by utf8

- `CREATE DATABASE IF NOT EXISTS cooker_cutter_pos default charset utf8 COLLATE utf8_general_ci;`

### $_SERVER

```html
    <form action="<?php echo $_SERVER['PHP_SELF']?>">
        <input type="submit" name="submit" value="submit" />
    </form>
```

### mysql

- Linux下mysql安装完后是默认：区分表名的大小写，不区分列名的大小写

### php -i

- `php -i | grep -i "iconv support"`

### Linux version

- `cat /proc/version`
- `lsb_release -a`

### linux unzip

### PHP GLOBALS

- `$GLOBALS["foo"]`

### PHP set_time_limit

- `set_time_limit(0); `

### JS - window location blank

- `window.open('http://www.smkproduction.eu5.org','_blank');`

### SQL Server create db

- IDENTITY - 自动增长 
- UNIQUE - unique index

```sql
CREATE TABLE RankListCategory
(
id int PRIMARY KEY IDENTITY,
itemid varchar(255) NOT NULL UNIQUE,
rankcategoryname varchar(255) NOT NULL
)

insert into RankListCategory (itemid, rankcategoryname) values ('ac27', 'base');
insert into RankListCategory (itemid, rankcategoryname) values ('CA13', 'base');
insert into RankListCategory (itemid, rankcategoryname) values ('AC23', 'size');
insert into RankListCategory (itemid, rankcategoryname) values ('CA12', 'Flavors / Mix-in-Topping');
```

### PHP excel set color

```php
function cellColor($cells,$color){
        global $objPHPExcel;
        $objPHPExcel->getActiveSheet()->getStyle($cells)->getFill()
        ->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID,
        'startcolor' => array('rgb' => $color)
        ));
    }
    cellColor('B5', 'F28A8C');
    cellColor('G5', 'F28A8C');
    cellColor('A7:I7', 'F28A8C');
    cellColor('A17:I17', 'F28A8C');
    cellColor('A30:Z30', 'F28A8C');
```

### PHP Excel set border

```php
$styleArray = array(
  'borders' => array(
    'allborders' => array( // outline
      'style' => PHPExcel_Style_Border::BORDER_THIN
    )
  )
);

$objPHPExcel->getActiveSheet()->getStyle('A2:B5')->applyFromArray($styleArray);
unset($styleArray);
```

### PHP Excel set font size

```php
$objPHPExcel->getActiveSheet()->getStyle("A1:G1")->getFont()->setSize(16);
```

### PHP Excel set font bold

```php
$objPHPExcel->getActiveSheet()->getStyle('A1:H1')->getFont()->setBold(true);
```


### Mysql case

- `select PK_Table as 'PK_Table', 'Status' = case when FL_CurStatus is null then '00' else FL_CurStatus end from tbl_ssb_table`
- `select PK_Table as 'PK_Table', case when FL_CurStatus is null then '00' else FL_CurStatus end as 'Status' from tbl_ssb_table`

### Running multiple SQL files at once

- `cat *.sql | mysql -u root -p db_name`

### SQL Server select views

```sql
Select * from VIE_TMP_FastFoodTimeCard VTF;
```

### 长尾关键词

- 网站上非目标关键词但也可以带来搜索流量的关键词，称为长尾关键词。 长尾关键词的特征是比较长，往往是2-3个词组成，甚至是短语，存在于内容页面，除了内容页的标题，还存在于内容中。 

### CC, BCC

- 抄送
- 暗送

### virtualbox port forwarding

- `VBoxManage.exe modifyvm "centos64" --natpf1 "guesthttp,tcp,,8800,,80"`

### apache

```bash
<Directory />
    Options FollowSymLinks
    AllowOverride None
    Order deny,allow
    Deny from all
</Directory>
```

```bash
<Directory />
    Options Indexes FollowSymLinks Includes ExecCGI
    AllowOverride All
    Order allow,deny
    Allow from all
</Directory>
```

### virtual ssh

- The best way to login to a guest Linux VirtualBox VM is port forwarding. By default, you should have one interface already which is using NAT. Then go to the Network settings and click the Port Forwarding button. Add a new Rule:
- Host port 3022, guest port 22, name ssh, other left blank.
- That's all! Please be sure you don't forget to install an SSH server:
 -`-sudo apt-get install openssh-server`
-To SSH into the guest VM, write:
 - `ssh -p 3022 user@127.0.0.1`

### php curl 

- `curl -s http://getcomposer.org/installer| php `

### Moving command to bin

- `sudo mv composer.phar /usr/bin/composer `

### sql server to mysql 

```sql
DECLARE @p_date DATETIME
SET  @p_date = CONVERT( DATETIME, '2013-11-01 00:00:00', 120 ) 
SELECT TOP 1000 PK_Id as orderNO, FK_Tbl as TableNo, FK_Status as Status, DF_OrderTime as OrderTime, DF_PayTime as PayTime, DF_YJine as Total, DF_MEMO1 as MEMO1 FROM TBL_FOO_OpMaster  WHERE 1=1  and DF_OrderTime >=@p_date and DF_OrderTime < DATEADD(d, 1, @p_date) ORDER BY PK_Id DESC

SELECT PK_Id as orderNO, FK_Tbl as TableNo, FK_Status as Status, DF_OrderTime as OrderTime, DF_PayTime as PayTime, DF_YJine as Total, DF_MEMO1 as MEMO1 
From TBL_FOO_OpMaster 
WHERE 1=1  and DF_OrderTime >= '2013-5-01 00:00:00' and DF_OrderTime < '2013-11-02 00:00:00' 
ORDER BY PK_Id DESC 
LIMIT 1000;
```

### Algo

```php
    foreach($staff as $k=>$v){
        $i = 0;             
        foreach($v as $item){
            $theTable .= '<tr>';
            
            if($i == 0)
                $theTable .= '<td rowspan="'.count($v).'">'.$k.'</td>';
            
            $i++;
            $theTable .= '<td>'.date('m/d/Y H:i:s', date_normalizer($item['DF_CheckInTime'])).'</td>
                          <td>'.date('m/d/Y H:i:s', date_normalizer($item['DF_CheckOutTime'])).'</td>
                          <td>'.$item['DF_LabourTime'].'</td>
                          <td>'.$item['DF_UserWage'].'</td>
                          </tr>
                         ';
        }
        
    }
```

### CSS table border

- `border-collapse:collapse;` - 合并边框

### CSS border-top

- `border-top: solid 1px;`

### css table td height

```css
    #grid td, #grid th{
        height: 50px;
    }
```

### PHP upload size

-  `post_max_size = 128M`

### Database Error: Unable to connect to the database:The MySQL adapter "mysql" is not available.

- `sudo yum install php54w-mysql.x86_64 php54w-cli php54w-common php54w-devel`

### yum uninstall 

- `yum remove php`

### centos php upgrade

- `sudo rpm -Uvh http://mirror.webtatic.com/yum/el6/latest.rpm` - add the Webtatic EL yum repository for centos 6
- `sudo yum remove php-common-5.3.3-23.el6_4.x86_64`
- `sudo yum install php54w`

### postal code

- 邮政编码是用阿拉伯数字组成，代表投递邮件的邮局的一种专用代号

- 1963年6月，邮政编码在美国诞生，用以应对当时急速增长的邮件寄递需求，美国由此成为世界上最早一批拥有邮政编码的国家。

> 中国采用四级六位编码制，
前两位表示省（直辖市、自治区），
第三位代表邮区，第四位代表县（市），
最后两位数字是代表从这个城市哪个投递区投递的，即投递区的位置。

- 例如：邮政编码“130000”“13”代表吉林省，“00”代表省会长春，“00”代表所在投递区。

### table border

- wrong `border=1`
- right `border="1"`

### Linux iptables

- `sudo /etc/init.d/iptables stop`

### Linux chkconfig

- `sudo chkconfig iptables off`

### linux 更改文件用户

- chown -R sean file

### PHP form action self

- `echo $_SERVER['PHP_SELF']`

### noarch

> noarch是no architecture的缩写，说明这个包可以在各个不同的cpu上使用。

### gpg

> gpg: key 3E9D8CBD marked as ultimately trusted

```
-----BEGIN PGP PUBLIC KEY BLOCK-----
Version: GnuPG v2.0.14 (GNU/Linux)
Comment: A revocation certificate should follow

iQEmBCABAgAQBQJSd3GVCR0CR2l2ZSB1cAAKCRBKQQr7Pp2MvYI1B/4qCu3gBgKH
4CfOE1tnlTuy86C44bpLS6tMiCgBlo99Ntz9ltQ2ysiekL2lCFHWgohCD9zWmDoX
qw6FZQ9cudh3wpOw4nu/mtLrMtCVl4hk5mCWSuWKqbxtduoHMIgpNk4PgzN3JRuP
bMjWL7/eIBpoIFHA0+e3F1XNu0JQgkfer0RQlQK1oOiC5RTS+k1RbAGEhNI105/k
hgtCuWdzdG++5BXWw6/jGzTBN19YOPnbnp1E6YpabCixWG4pPxe/UtEzssL8C94a
NZ5Sawk7XyH4jxbuVdkeiyIlK2F+Imt8Y1yVpb+OZ/MhVk2XtevhmutrM6JEgTvK
yErT/4DphAE+
=ZCeO
-----END PGP PUBLIC KEY BLOCK-----
```

- `gpg --send-keys lleiiell --keyserver hkp://subkeys.pgp.net`

### php function description

```php
/**
 * Active Company Controller
 */
```

### cp: cannot create regular file `try2.php': Permission denied

- php exec()
- echo exec('cp ../public-key.txt public-key.txt');

### php time zone

- http://us3.php.net/manual/zh/datetime.configuration.php#ini.date.timezone
- date.timezone = Asia/Chongqing

### unzip

- `unzip jquery-validation-1.11.1.zip -d jquery-validation2`

### PHP dirname()

- 返回路径中的目录部分。

### PHP include require

- include()会产生一个警告，而require()则导致一个致命的错误（出现错误，脚本停止执行）
 - require() :如果文件不存在，会报出一个fatal error.脚本停止执行
 - include() : 如果文件不存在，会给出一个 warning，但脚本会继续执行

### Linux find and delelte 

- `find -name .svn -exec rm -rf {} \;`

### php change variable of one file

- I Succeed by this:

```php
    $file = 'file.php';
    $content = file_get_contents($file);
    $replacement = 'replace';
    $content = preg_replace('/find/', $replacement, $content);
    file_put_contents($file, $content);
```

### Linux zip

- 将当前目录下的所有文件和文件夹全部压缩成myfile.zip文件,－r表示递归压缩子目录下所有文件.
- `zip -r myfile.zip ./*`

- `unzip -d mysl.zip myfile` - wrong

### PHP Turn off all error reporting

- `error_reporting(0);`
- `error_reporting(E_ALL);`

### php pdo

```php
    $db = new PDO('mysql:host=localhost;dbname='.$dbName, $GLOBALS['dbAdminUser'], $GLOBALS['dbAdminPass']);
    $sql = file_get_contents($dbFile);
    if($db->exec($sql)){
        echo 'Init DB: Query sql file failed. <br />';
        return false;
    } else {
        return true;
    }
```

### mysql quote db name - wrong!

```sql
CREATE DATABASE IF NOT EXISTS 'showcase_menu_23' default charset utf8 COLLATE utf8_general_ci;
```

### php create mysql db

```php
$con=mysqli_connect("example.com","peter","abc123");
// Check connection
if (mysqli_connect_errno())
  {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  }

// Create database
$sql="CREATE DATABASE my_db";
if (mysqli_query($con,$sql))
  {
  echo "Database my_db created successfully";
  }
else
  {
  echo "Error creating database: " . mysqli_error($con);
  }
```

### PHP define

```php
// Set flag that this is a parent file
define( '_JEXEC', 1 );
defined('_JEXEC') or die('Restricted access');
```

### PHP display errors

- `display_errors = Off`

### ssh remove passphrase

- `ssh-keygen -p`
- http://stackoverflow.com/questions/112396/how-do-i-remove-the-passphrase-for-the-ssh-key-without-having-to-create-a-new-ke

### linux: processor

- `cat /proc/cpuinfo |grep processor`

### INginx

#### ab test

- `ab -c 1 -n 10000 http://127.0.0.1/index.html`

#### ps

- `ps aux | grep nginx`

#### kill

- `kill -s QUIT 14011`

#### Nginx restart

- `/usr/local/nginx/sbin/nginx -s  reload`

### windows 7 temp dir

- `C:\Users\sean\AppData\Local\Temp\`
- `C:\Users\sean\AppData\Local\Temp\scp45310` - winscp 

### linux zip except file

- `zip -r bitvolution.zip bitvolution -x \*.git\*`

### Linux chown

- `chown -R apache:apache file`

### Joomla password

    $originPassArr = explode(':', $passDecode);
    if(md5($pass.$originPassArr[1]) != $originPassArr[0]){

### SElinux

- `sudo vim /etc/selinux/config`
- `setenforce 0`

### ssh 登录服务器时的防掉线设置

- `ssh -o ServerAliveInterval=60 -C user@host`

### End

### table-layout

- fixed, 列宽由表格宽度和列宽度设定。
- automatic, 默认。列宽度由单元格内容设定。
- inherit, 规定应该从父元素继承 table-layout 属性的值。

```
table
{
   table-layout:fixed;
}
```

### cursor

cursor: pointer;

### end

- https://github.com/styleguide/css - css guide

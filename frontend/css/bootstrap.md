## Bootstrap Twitter

### Way

- http://www.bootcss.com/ 

### Reset via Normalize

- With Bootstrap 2, the old reset block has been dropped in favor of Normalize.css

### tooltip

```
<a data-toggle="tooltip" data-placement="bottom" data-trigger="manual" title="请等待,top5简历匹配中.." href="##">
```

### Default grid system

#### Basic grid HTML

```html
<div class="row">
   <div class="span4">...</div>
   <div class="span8">...</div>
</div>
```

#### Offsetting columns

```html
<div class="row">
   <div class="span4">...</div>
   <div class="span3 offset2">...</div>
</div>
```

#### Nesting columns

```html
<div class="row">
    <div class="span9">
        Level 1 column
        <div class="row">
            <div class="span6">Level 2</div>
            <div class="span3">Level 2</div>
        </div>
    </div>
</div>
```

### Fluid grid system

#### Basic fluid grid HTML

```html
<div class="row-fluid">
    <div class="span4">...</div>
    <div class="span8">...</div>
</div>
```

#### Fluid offsetting

```html
    <div class="row-fluid">
    <div class="span4">...</div>
    <div class="span4 offset2">...</div>
    </div>
```

#### fluid Nesting

```html
<div class="row-fluid">
 <div class="span12">
    Fluid 12
    <div class="row-fluid">
       <div class="span6">
       Fluid 6
          <div class="row-fluid">
             <div class="span6">Fluid 6</div>
                <div class="span6">Fluid 6</div>
                </div>
             </div>
          <div class="span6">Fluid 6</div>
       </div>
    </div>
</div>
```

### Layouts

#### Fixed Layout

```html
    <body>
    <div class="container">
    ...
    </div>
    </body>
```

### bootstrap padding-left 20px

> Try adding this CSS somewhere after the Bootstrap CSS:

```css
@media (max-width: 767px) {
  body {
    padding-right: 0;
    padding-left: 0;
  }
  .navbar-fixed-top,
  .navbar-fixed-bottom,
  .navbar-static-top {
    margin-right: 0;
    margin-left: 0;
  }
}
```

- http://stackoverflow.com/questions/14109717/when-viewing-my-twitter-bootstrap-site-on-a-mobile-a-20px-gutter-is-added-on-the

### media

- tablet

```css
@media (min-width: 768px) and (max-width: 979px) {
  body {
    background: url("img/body_bg.png") repeat scroll 0 0 transparent;
  }
}
```

### base css

- muted
- pull-right
- text-center
- text-error
- text-info
- text-left
- text-success
- text-warning

#### Abbreviations

- `<abbr title="attribute">attr</abbr>`

#### Blockquotes

- Wrap `<blockquote>` around any HTML as the quote. For straight quotes we recommend a `<p>`.

#### Lists

- inline
- ordered
- unordered

### End

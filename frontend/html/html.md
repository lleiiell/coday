### Html

- small

### bold

- strong

### italics

- em

### mailto

```
<a href="mailto:?subject=ownCloud&amp;body=ownCloud is a great open software to sync and share your files. You can freely get it from http://owncloud.org">推荐给您的朋友们</a>
```

### readonly - 规定输入字段为只读

`<input readonly="readonly" />`

### base - specify a default url and a default target for all links on a page

```
<head>
   <base href="http://www.w3schools.com/images/" target="_blank">
</head>
<body>
   <img src="stickman.gif" width="24" height="39" alt="Stickman">
   <a href="http://www.w3schools.com">W3Schools</a>
</body>
```

### checked

`<input type="radio" class="choiceadd" name="groupname" value="<?php echo $v['groupid']; ?>" checked />`

### placehold

- `placeholder="Please enter room number"`

### Onclick Javascript

- `<a class="toolbar" onclick="javascript: submitbutton('group.save')" href="#">`

### upload after browse

- `<input type="file" name="mp3" onchange="this.form.submit();" />`

### End

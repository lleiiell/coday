## JS

- JavaScript 对大小写是敏感的。

### isNaN

- isNaN() 函数用于检查其参数是否是非数字值。
- http://www.w3school.com.cn/js/jsref_isNaN.asp

### 外部的 JavaScript 

- `<script src="myScript.js"></script>`

### 换行

- 您可以在文本字符串中使用反斜杠对代码行进行换行。下面的例子会正确地显示：

```js
document.write("Hello \
World!");
```

### 脚本语言

- JavaScript 是脚本语言。浏览器会在读取代码时，逐行地执行脚本代码。而对于传统编程来说，会在执行前对所有代码进行编译。

### 变量

- 变量是存储信息的容器。
- 一个好的编程习惯是，在代码开始处，统一对需要的变量进行声明。
- 如果重新声明 JavaScript 变量，该变量的值不会丢失：

### Array

- literal array - `var cars=["Audi","BMW","Volvo"];`

### Undefined 和 Null

- Undefined 这个值表示变量不含有值。
- 可以通过将变量的值设置为 null 来清空变量

### 对象

- 声明变量类型
- 当您声明新变量时，可以使用关键词 "new" 来声明其类型：

```js
var carname=new String;
var x=      new Number;
var y=      new Boolean;
var cars=   new Array;
var person= new Object;
```

- JavaScript 变量均为对象。当您声明一个变量时，就创建了一个新的对象。
- JavaScript 中的所有事物都是对象：字符串、数字、数组、日期，等等。
- 在 JavaScript 中，对象是拥有属性和方法的数据。

### 属性和方法

- 属性是与对象相关的值。
- 方法是能够在对象上执行的动作。

- 举例：汽车就是现实生活中的对象。

```js
汽车的属性：
car.name=Fiat

car.model=500

car.weight=850kg

car.color=white 

汽车的方法：
car.start()

car.drive()

car.brake()
```

- 汽车的属性包括名称、型号、重量、颜色等。
 - 所有汽车都有这些属性，但是每款车的属性都不尽相同。
- 汽车的方法可以是启动、驾驶、刹车等。
 - 所有汽车都拥有这些方法，但是它们被执行的时间都不尽相同。

### toUpperCase() 方法来把文本转换为大写

```js
var message="Hello world!";
var x=message.toUpperCase();
```

### camel-case

> 在面向对象的语言中，使用 camel-case 标记法的函数是很常见的。您会经常看到 someMethod() 这样的函数名，而不是 some_method()。

### 函数

- 函数是由事件驱动的或者当它被调用时执行的可重复使用的代码块。

### js json

```js
var json = '{"result":true,"count":1}',
obj = JSON.parse(json);
alert(obj.count);
```

- http://stackoverflow.com/questions/4935632/how-to-parse-json-in-javascript

### End

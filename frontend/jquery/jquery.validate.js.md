## jQuery Validation Plugin

### Basic

- http://jqueryvalidation.org/

### validate remote

```js
$('#regForm').validate({
    rules:{
        Email: {
            remote:{
                url: 'url?action=validateEmail',
                type: 'POST'
            }
        }
    }
});
```

- remote data

```js
$( "#myform" ).validate({
  rules: {
    email: {
      required: true,
      email: true,
      remote: {
        url: "check-email.php",
        type: "post",
        data: {
          username: function() {
            return $( "#username" ).val();
          }
        }
      }
    }
  }
});
```

### submitHandler

```js
 $("#myform").validate({
  submitHandler: function(form) {
    // some other code
    // maybe disabling submit button
    // then:
    $(form).submit();
  }
 });
```

### rules()

> Adds required and minlength: 2 to an element and specifies custom messages for both.

```js
$( "#myinput" ).rules( "add", {
  required: true,
  minlength: 2,
  messages: {
    required: "Required input",
    minlength: jQuery.format("Please, at least {0} characters are necessary")
  }
});
```

- rules( “remove”, rules )

```js
$( "#myinput" ).rules( "remove", "min max" );
```

- Removes all static rules from an element.

```js
$( "#myinput" ).rules( "remove" );
```

### range( range )

> Makes the element require a given value range.

```js
$( "#myform" ).validate({
  rules: {
    field: {
      required: true,
      range: [13, 23]
    }
  }
});
```

### rangelength

> Description: Makes the element require a given value range.

```js
$( "#myform" ).validate({
  rules: {
    field: {
      required: true,
      rangelength: [2, 6]
    }
  }
});
```

### unchecked Selector

> Description: Selects all elements that are unchecked.

```js
function countUnchecked() {
  var n = $( "input:unchecked" ).length;
  $( "div" ).text(n + (n == 1 ? " is" : " are") + " unchecked!" );
}
countUnchecked();
$( ":checkbox" ).click( countUnchecked );
```

### url method

> Description: Makes the element require a valid url

```js
$( "#myform" ).validate({
  rules: {
    field: {
      required: true,
      url: true
    }
  }
});
```

### .valid()

> Description: Checks whether the selected form is valid or whether all selected elements are valid.

```js
var form = $( "#myform" );
form.validate();
$( "button" ).click(function() {
  alert( "Valid: " + form.valid() );
});
```

### jQuery.validator.addClassRules()

> Description: Add a compound class method – useful to refactor common combinations of rules into a single class.

- Add two compound class rules for name and zip.

```js
jQuery.validator.addClassRules({
  name: {
    required: true,
    minlength: 2
  },
  zip: {
    required: true,
    digits: true,
    minlength: 5,
    maxlength: 5
  }
});
```

- http://jqueryvalidation.org/jQuery.validator.addClassRules/

### End

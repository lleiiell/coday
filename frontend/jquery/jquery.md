## Jquery 

### $(document).ready()

```js
$(document).ready(function(){
    console.log('Ready!');
});
```

### setTimeout var

```
var showtop5;
jQuery(".hello").mouseenter(function(){
   var target = jQuery(this);
   $.ajaxSetup({cache: false});
   if(target.hasClass("loaded")){
      showtop5 = setTimeout(function(){
         jQuery("#hello5").show();
      }, 500)           
         return false; 
   }            
   ...          
      showtop5 = setTimeout(function(){
         $.ajax({...});  
      }, 500);          
}).mouseleave(function(){
   window.clearTimeout(showtop5);
});   
```

### .map vs .each

```
// i understood it by this:
function fun1() {
    return this + 1;
}

function fun2(el) {
    return el + 1;
}

var item = [5,4,3,2,1];
var newitem1 = $.each(item, fun1);
var newitem2 = $.map(item, fun2);

console.log(newitem1); // [5, 4, 3, 2, 1] 
console.log(newitem2); // [6, 5, 4, 3, 2] 

// so, each function returns the original array while map function returns a new array 
```

### define array

```
var a = new Array();
a[1] = '..';
```

### validate submitHanddle

```
$("#jobdemandsform").validate({
   submitHandler : function(form){
```

#### jquery validate submitHandler

```js
$(".selector").validate({
  submitHandler: function(form) {
    $(form).ajaxSubmit();
    $(form).submit();
  }
})
```

### trigger

```
$("#jobdemands .req_ctskillbox input").bind('keydown', function (e) {
   var key = e.which;
   if (key == 13) {
      $("#jobdemands .addskill").trigger("click");
   }
});
```

### 选择器是否选取

1. ().length > 0

```
if(jQuery("i.undone", firstjob).length>0){ debug(hasjob);
jQuery("#hr_resumerequiredtip").show().position({
   my: "left top",
   at: "left-50 bottom",
   of: jQuery("i.undone", firstjob)
});
```

### js define 

```
var a = b = 1;
var a =1, b=2;
```

### jquery foreach 2D array

```
var hah = '';
var a = [{"a":"b", "c":"d", "e":"f"},{"a":"b2", "c":"d2", "e":"f2"}];
$.each(a, function(key, value){ hah += value["a"] + ' ' +value["c"] + ' ' +value["e"] + ' ' });
alert(hah);
```

### js 闭包

1. 子函数可以读父函数的局部变量

```
beforeSubmit : function(arr, jobdemandsform, options){
   var datacheck = 0;
   var lang_name_count = 0;
   var lang_level_count = 0;
   $.each(arr, function(i, o) { if(o.value!=''){datacheck++;
      if(o.name == 'lang_name[]' && o.value != ''){ lang_name_count++ };
      if(o.name == 'lang_level[]' && o.value != ''){ lang_level_count++ };
      
   } }); 
   if(datacheck <= 2){
      $('.showerrormes').show();
      return false;
   }
   if(lang_name_count > lang_level_count){
      $('.showerrormes').text('请选择外语熟练度').show();
      return false;
   }
   showLoading($("#jobdemandsform"), '', null, {opacity:"0.5"});
}
```

### parseInt()

0. 解析字符串, 返回一个整数
1. 默认为十进制

```
parseInt("10");			//返回 10
parseInt("19",10);		//返回 19 (10+9)
parseInt("11",2);		//返回 3 (2+1)
parseInt("17",8);		//返回 15 (8+7)
parseInt("1f",16);		//返回 31 (16+15)
parseInt("010");		//未定：返回 10 或 8
```

### first() 

- reduce the set of matched elements to the first in the set  
`$('li').first().css('background-color', 'red');`


### ajax get data from exit

exit(json_encode(array('error'=>'no value')));

### ajax dateType

dataType : "json"

### ajax post

```
jQuery.ajax({
   url : online_interviews_url,
   type : "POST",
   data : {jobid: jobid, key: key, value: "0"},
   dataType : "json"
});
```

### select brother elements

- siblings `window.location.href = target.siblings('a').attr('href');`

### delay

```
jQuery(".jobposition_tip").fadeIn().position({
   my: "center center",
   at: "center center",
   of: target2
}).delay(500).fadeOut();
```

### ajaxForm

- beforeSubmit

```
      var options = {
         dataType : 'json',
         beforeSubmit : function(arr, jobdemandsform, options){
            var datacheck = 0;
            $.each(arr, function(i, o) { if(o.value!=''){datacheck++;} }); 
            if(datacheck <= 2){
               $('.showerrormes').show();
               return false;
            }
            showLoading($("#jobdemandsform"), '', null, {opacity:"0.5"});
         },
         success : function(data){
            if(data.error){
               $('.showerrormes').show();
            } else {
               jobdemandsform.fadeOut();
               $('#baserequired_successmsg').fadeIn();
            }
         }
      }
      jQuery("#jobdemandsform").ajaxSubmit(options);
      return false;
```

### validate
1. html

```
<form action="">
<input type="" class="required" />
</form>
```

2. js - `$('form').validate();`

### corner

1. default 10px round corner - `$('#myDiv').corner();`
2. 30px round corner - `$('#myDiv').corner('30px');`
3. 15px bevel corner - `$('#myDiv').corner('15px bevel');`
4. dogeared top right corner - `$('#myDiv').corner('dog tr');`

### cookie

1. create - `$.cookie('the_cookie', 'the_value');`
2. Create expiring cookie, 7 days from then: - `$.cookie('the_cookie', 'the_value', { expires: 7 });`
3. Create expiring cookie, valid across entire site - `$.cookie('the_cookie', 'the_value', { expires: 7, path: '/' });`
4. read cookie - `$.cookie('the_cookie'); // => "the_value"`
5. read all cookies - `$.cookie(); // => { "the_cookie": "the_value", "...remaining": "cookies" }`
6. delete cookie

```
// Returns true when cookie was found, false when no cookie was found...
$.removeCookie('the_cookie');
// Same path as when the cookie was written...
$.removeCookie('the_cookie', { path: '/' });
```

### datepicker

- `<input type="text" name="" id="datepicker" />`
- `$('#datepicker').datepicker();`

### metadata

<li data="{some:'random', json:'json'}" class="someclass"></li>
$.meatdata.setType('attr', 'data');
alert($('.someclass').metadata().some);

### tagit

<ul id="tagit"></ul>
$('#tagit').tagit();

### .position()

 jQuery("#hello").position({
    my:"right top",
    at:"right bottom",
    of:target
 }).show();

### replaceWith() 

### removeClass()

jQuery(".hr_pos_numpeople.loaded").removeClass("loaded");

### hasClass()

### addClass()

target.addClass("loaded");

### setTimeout()

```javacript
jQuery(".hr_pos_numpeople:not('.hr_pos_numpeople_no')").mouseover(function(){
    var target = jQuery(this);
    $.ajaxSetup({cache: false});
    showtop5 = setTimeout(function(){
            $.ajax({...});  
            }, 500);
    }).mouseleave(function(){
            showtop5 = window.clearTimeout(showtop5);
   });
```

- function no brackets

```js
$(document).ready(function(){
    function review_show() {
        $('#show_loading').hide();
        $('.show_bill_div, #goback').show();
        return true;
    }
    setTimeout(review_show,2000);
});
```

### .remove()

$('.hello').remove();

### .inserAfter()

`$('h2').insertAfter($('.container'));`

### History back() Method

- The back() method loads the previous URL in the history list.
- This is the same as clicking the Back button or history.go(-1).

### javascript:void(0)

- `<a class="goback" href="javascript:void(0)"> back </a>`

### window.onload

- 当页面加载的时候可以调用某些函数

### js read json

```js
var json = '{"result":true,"count":1}',
obj = JSON.parse(json);
alert(obj.count);
```

### js submit form

- `$("#second_step").submit(); `

### js refresh

- `<a href="javascript:location.reload()">`

### jquery get form post

```js
$.ajax({
    ...
    data: $("#registerSubmit").serialize(),
    ...
})
```

### js check undefined

```js
typeof(obj) != 'undefined'
```

### jquery each

```js
var arr1 = [ "aaa", "bbb", "ccc" ];      
$.each(arr1, function(i,val){      
  alert(i);   
  alert(val);
});   
```

### jquery validate error message

```js
$('#regForm').validate({
  rules:{
    Email: {
      remote:{
        url: 'helper.php?action=validateEmail',
        type: 'POST'
      }
    },
    Email2: {
      equalTo: "#Email"
    }
  },
  messages:{
    Email2:{
      equalTo: "Please enter the same email again."
    }
  }
});
```

### End

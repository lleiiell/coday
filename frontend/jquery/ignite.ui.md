## Ignite UI

### Basic

- http://www.infragistics.com/community/blogs/jason_beres/archive/2012/10/12/ignite-ui-the-world-s-most-advanced-javascript-amp-html5-ui-framework.aspx
- http://labs.infragistics.com/jquery/configure/

### Add link to sheet

```js
columns: [
    { headerText: "Order No", key: "orderNO", dataType: "number", formatter: getOrderDetailURL},
    //{ headerText: "Table Name", key: "TableName", dataType: "string"},
    { headerText: "Status", key: "Status", dataType: "string"},
    { headerText: "Order Date", key: "OrderDate", dataType: "date"},
    { headerText: "Total", key: "Total", dataType: "number", format: "currency"}
],

...

function getOrderDetailURL(val) {
    return "<a href='...html?no=" + val + "' target='_blank'>" + val + "</a>";
} 
```

### End

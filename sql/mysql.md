## mysql

### regexp

- `select ... where name regexp '1000'`
- `select ... where name regexp '.000'`
- `select ... where name regexp '1000|2000'` - or
- `select ... where name regexp '[1|2]000'` - one of
- `select ... where name regexp '[1-5]000'` - match scale
- `select ... where name regexp '\\.'` - match special chars

### like

- `... where name like 'sea%'` - match jet..

- `... where name like 's_an'` - match single word

### where

- `... where price <> 2.5` - '!='
- `... where price between 2.5 and 5` - between .. and

- `... where price is null` - is null

- `... where id in (100, 102)` - in
- `... where id not in (100, 102)` - not in

### order

- `select ... order by prod_id, prod_price` - order by multi-cols

- `select ... order by .. desc` - desc

### show

- `show errors/warnings` - show server errors

### show grants

- `show grants`

### show server status

- `show status`

### show creation sql

- `show create database dbname`
- `show create table tablename`

### show columns info

- `show columns from tablename`
- `desc tablename`

### import mysql at linux

- `sudo mysql -f < 2.sql`
- `--force, -f` // Continue even if an SQL error occurs.

### mysql 导出表

- `mysqldump -u user -p dbname tablename > sqlname.sql;`

### BINARY 

- 不是函数，是类型转换运算符，它用来强制它后面的字符串为一个二进制字符串，
- 可以理解为在字符串比较的时候区分大小写

```
mysql> select binary 'ABCD'='abcd' COM1, 'ABCD'='abcd' COM2;
+--------+-----------+
| COM1 | COM2 |
+--------+-----------+
|      0     |      1    |
+---------+-----------+
1 row in set (0.00 sec)
```

- BINARY和VARBINARY类类似于CHAR和VARCHAR，不同的是它们包含二进制字符串而不要非二进制字符串。
- 也就是说，它们包含字节字符串而不是字符字符串。

### join

- inner join equal cross join if hasn't on after inner join

```
// inner join
select * from jos_persontest_jobsheet as js inner join jos_jobtest_jobsheet as pjs; 
// cross join
select * from jos_persontest_jobsheet as js cross join jos_jobtest_jobsheet as pjs; 
// inner join .. on ..
select * from jos_persontest_jobsheet as js inner join jos_jobtest_jobsheet as pjs on js.jobid=pjs.jobid; 
```

- 'left join ... and ...' not affected rows count but the join result;

```
select * from jos_persontest_jobsheet as js left join jos_jobtest_jobsheet as pjs on js.jobid=pjs.jobid and js.jobid=3;
| 234 |     2 |       1 |          1 | NULL |  NULL |    NULL |       NULL |
| 235 |   539 |       2 |          2 | NULL |  NULL |    NULL |       NULL 
```

- 'left join ... where ...' not only affected rows count but also the join result; 
- where choose results after select got results;

```
select * from jos_persontest_jobsheet as js left join jos_jobtest_jobsheet as pjs on js.jobid=pjs.jobid where js.jobid=3;
```

### locate

- 返回子串 substr 在字符串 str 中第一次出现的位置

```sql
mysql> SELECT LOCATE(’bar’, ‘foobarbar’);
-> 4
mysql> SELECT LOCATE(’xbar’, ‘foobar’);
-> 0
```

### SQL_CALC_FOUND_ROWS 

> Just a quick summary: Peter says that it depends on your indexes and other factors. 
> Many of the comments to the post seem to say that SQL_CALC_FOUND_ROWS is almost always 
> slower - sometimes up to 10x slower - than running two queries.
> - stack overflow

### group_concat()

returns a string result with the concatenated non-null values from a group;

### insert

`insert into hellotable (jobid, groupid) values ('.intval($job_id).', "'.$groupname.'") on duplicate key update groupid=values(groupid);`

### add field

`alter table hellotable add column gid int(11) unsigned null default null comment 'sheet group id' after title;`

### create table

```sql
CREATE TABLE `hellotable` (
  `gid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `gname` varchar(80) DEFAULT NULL COMMENT 'sheet group name',
  PRIMARY KEY (`gid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
```

### concat

- `select concat(name, '-', id) from vendors`

### functions

- `select trim(name) ...` - trim space

- length(), locate(), lower(), ltrim(), rtrime(), upper() ..

#### time/date

- curdate(), curtime(), date(), datediff(), date_format,  
day(), dayofweek(), hour(), minute(), month(), now(),  
second(), time(), year()

- `select ... where date(order_date) = '2005-09-01'` - date()

### math

- abs(), cos(), exp(), mod(), pi(), rand(), sin(), sqrt(), tan()

- `select avg(prod_price) ..`

- `select count(*) ...` - count with null
- `select count(id) ...` - count without null

- `select max(price) from products` - max()/min()/sum()

### calculate

- `select price*100 from ..`

### group

#### having

- `select id from ... group by id having count(*) >= 2` - filter group

#### where vs having

- 'where' filters before 'group', while 'having' behind;

### create

- create database with utf8
- `CREATE DATABASE IF NOT EXISTS my_db default charset utf8 COLLATE utf8_general_ci; `

### grant

- `grant all privileges on jeecnDB.* to jeecn@localhost identified by ‘jeecn’;`

- `grant select,update on phplampDB.* to phplamp@localhost identified by '1234';`

- `flush privileges;` - 刷新系统权限表

### query repeat records

- `where peopleId in (select peopleId from people group by peopleId having count(peopleId) > 1)` 

### ifnull

- `ifnull(DF_Jine,0)`

### group and one

- `select count(id), Name from city group by Name limit 10;`

### sqlserver getdate to mysql

- `CURRENT_TIMESTAMP`

### procedure

- `DROP PROCEDURE usp;`

```sql
SET @cardid=1;
SET @optype=1;
SET @inmoney=1;
SET @opid =1;
SET @output=0;
call usp(@cardid, @optype, @inmoney, @opid, @output);
```

- `show procedure status;`

```sql
delimiter //
create procedure hah()
begin
select * from jos_users;
end //
delimiter ;
```

- `SELECT field1 INTO old-data FROM table1 WHERE pk_cid = cid;`
- `Declare old-data NUMERIC(18,2);`
- `case when`

```sql
    CASE ctype
    WHEN 1 THEN
        SET output = 3;
    WHEN 2 THEN
        ..
        set output = old-data +  imoney;
    WHEN 3 THEN
        ..
    END CASE;
```

```sql
CREATE DEFINER=`root`@`localhost` PROCEDURE `usp`(IN cid CHAR(10), INOUT output NUMERIC(18,2)  )
```

```sql
IF old-data < imoney THEN
    SET output = -1;
END IF;
```

### mysql next some days

- `select CURRENT_TIMESTAMP + INTERVAL 7 DAY;`
- http://stackoverflow.com/questions/3887509/mysqls-now-1-day

### mysql restore with utf8

- `mysql -D <DB-Name> -u root -p --default-character-set=utf8`
- http://forums.mysql.com/read.php?103,275798,275798

### End

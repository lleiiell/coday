## SQL Server

### getdate of hour

- SELECT DATEPART(hh, GETDATE());

### select .. join .. group ..

```sql
SELECT SUM(od.DF_Num) as count, SUM(od.DF_Jine) as sales, Hours 
from TBL_FOO_OpDetail as od 
inner join 
   (Select DATEPART(hh, DF_PayTime) as Hours, PK_Id from TBL_FOO_OpMaster group by DATEPART(hh, DF_PayTime), PK_Id) 
as om on od.FK_OpId=om.PK_Id 
where od.FK_Category = 01 and od.FK_BigClass != 98
group by Hours
```

### sql change way

```sql
SELECT isnull(SC.DF_SMALLCLASSName,'-') as SmallClass, OO.Qty, OO.Total 
FROM 
(SELECT SUM(OPD.DF_Num) as Qty, SUM(OPD.DF_Jine) as Total, OPD.FK_SMALLCLASS 
FROM TBL_FOO_OpMaster OPM 
INNER JOIN TBL_FOO_OpDetail OPD ON OPM.PK_Id = OPD.FK_OpId 
WHERE OPM.FK_Status = 2 and OPD.FK_SMALLCLASS > 5 
GROUP BY OPD.FK_SMALLCLASS 
) as OO 
INNER JOIN TBL_SSB_SmallClass SC on OO.FK_SMALLCLASS= SC.PK_SMALLCLASS 
GROUP BY SC.DF_SMALLCLASSName 
ORDER BY SC.DF_SMALLCLASSName 

SELECT SUM(OPD.DF_Num) as Qty, SUM(OPD.DF_Jine) as Total, isnull(SC.DF_SMALLCLASSName,'-') as SmallClass 
FROM TBL_FOO_OpMaster OPM 
INNER JOIN TBL_FOO_OpDetail OPD ON OPM.PK_Id = OPD.FK_OpId 
INNER JOIN TBL_SSB_SmallClass SC ON OPD.FK_SMALLCLASS= SC.PK_SMALLCLASS  
WHERE OPM.FK_Status = 2 and OPD.FK_SMALLCLASS > 5 
GROUP BY OPD.FK_SMALLCLASS,  SC.DF_SMALLCLASSName
```

### case

```sql
USE pubs 
GO 
SELECT 
    Title, 
    'Price Range' = 
    CASE 
        WHEN price IS NULL THEN 'Unpriced' 
        WHEN price < 10 THEN 'Bargain' 
        WHEN price BETWEEN 10 and 20 THEN 'Average' 
        ELSE 'Gift to impress relatives' 
    END 
FROM titles 
ORDER BY price 
GO
```

### sql up 131010

- sub select

```sql
SELECT OP.PK_Id as orderNO,  OP.DF_OrderDate as OrderDate, 
                OP.DF_PayTime as PayDate, OP.DF_YJine as Total, 
                (select DF_StatusName from TBL_SSB_FrontOpStatus where PK_Status=OP.FK_Status) as Status
                FROM TBL_FOO_OpMaster as OP 
                where 1=1  and OP.DF_OrderTime > '2013/9/3 0:0:0' and OP.DF_OrderTime <= '2013/10/10 23:59:59'
```

### End

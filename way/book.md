## Books

### php

#### 细说PHP

- http://book.douban.com/subject/4090200/

#### CodeIgniter for Rapid PHP Application Development

- http://it-ebooks.info/book/2760/

### Joomla

#### Joomla! 1.5: Beginner's Guide

- http://it-ebooks.info/book/2764/

#### Joomla! 1.5 Cookbook

- http://it-ebooks.info/book/2763/

#### Joomla! Mobile Development

- http://it-ebooks.info/book/1226/

#### Advanced Joomla!

- http://it-ebooks.info/book/2395/

#### Joomla! Templates

- http://it-ebooks.info/book/1042/

#### Joomla! For Dummies, 2nd Edition

- http://it-ebooks.info/book/776/

#### Building job sites with Joomla!

- http://it-ebooks.info/book/2766/

#### The Official Joomla! Book

- http://it-ebooks.info/book/2615/

#### Mastering Joomla! 1.5 Extension and Framework Development, 2nd Edition

- http://it-ebooks.info/book/1994/

#### Joomla! Start to Finish

- http://it-ebooks.info/book/891/

#### Joomla! E-Commerce with VirtueMart

- http://it-ebooks.info/book/1651/

#### Using Joomla

- http://it-ebooks.info/book/98/

### others

### List of freely available programming books

- http://stackoverflow.com/questions/194812/list-of-freely-available-programming-books/

#### 黑客与画家

- 作者Paul Graham，译者阮一峰，双重靠谱。

#### 精益创业

- 一种经过实践证明的创业理论。

#### 精益创业实战

- 精益创业的具体实践。
- http://book.douban.com/subject/20505765/

#### 商业模式新生代

- 一个有理有据，可操作性强的商业模式分析框架。
（切记：“商业模式“ != “怎么赚钱“）

- http://book.douban.com/subject/6718487/

### End

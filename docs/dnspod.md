## DNSPOD

### 主机记录

> 阿D提示您：要解析 www.lleiiell.info, 请填写 www 哦～

    主机记录就是域名前缀，常见用法有：

    www：解析后的域名为 www.lleiiell.info
    @：直接解析主域名 lleiiell.info
    *：泛解析，匹配其他所有域名 *.lleiiell.info

### 记录类型

> 阿D提示您：要指向空间商提供的 IP 地址，选择「类型 A」，要指向一个域名，选择「类型 CNAME」

    A记录：地址记录，用来指定域名的IPv4地址（如：8.8.8.8），如果需要将域名指向一个IP地址，就需要添加A记录。
    CNAME： 如果需要将域名指向另一个域名，再由另一个域名提供ip地址，就需要添加CNAME记录。
    TXT：在这里可以填写任何东西，长度限制255。绝大多数的TXT记录是用来做SPF记录（反垃圾邮件）。
    NS：域名服务器记录，如果需要把子域名交给其他DNS服务商解析，就需要添加NS记录。
    AAAA：用来指定主机名（或域名）对应的IPv6地址（例如：ff06:0:0:0:0:0:0:c3）记录。
    MX：如果需要设置邮箱，让邮箱能收到邮件，就需要添加MX记录。
    显性URL：从一个地址301重定向到另一个地址的时候，就需要添加显性URL记录（注：DNSPod目前只支持301重定向）。
    隐性URL：类似于显性URL，区别在于隐性URL不会改变地址栏中的域名。
    SRV：记录了哪台计算机提供了哪个服务。格式为：服务的名字、点、协议的类型，例如：_xmpp-server._tcp。

### 线路类型

> 阿D提示您：若空间商只提供了一个 IP 地址或域名，选择「默认」就可以了

    让指定线路的用户访问这个IP

    常见用法有：
    默认：必须添加，否则只有单独指定的线路才能访问您的网站。如果双线解析，建议「默认」线路填写「电信IP」
    联通：单独为「联通用户」指定服务器 IP，其他用户依然访问「默认」
    搜索引擎：指定一个服务器 IP 让蜘蛛抓取

### 记录值

> 阿D提示您：最常见的是将空间商提供的「IP地址」填写在这里哦～

    各类型的记录值一般是这样的：

    A记录：填写您服务器 IP，如果您不知道，请咨询您的空间商
    CNAME记录：填写空间商给您提供的域名，例如：dnspod.cn
    MX记录：填写您邮件服务器的IP地址或企业邮局给您提供的域名，如果您不知道，请咨询您的邮件服务提供商
    TXT记录：一般用于 Google、QQ等企业邮箱的反垃圾邮件设置
    显性URL记录：填写要跳转到的网址，例如：http://www.baidu.com
    隐性URL记录：填写要引用内容的网址，例如：http://www.baidu.com
    AAAA：不常用。解析到 IPv6 的地址。
    NS记录：不常用。系统默认添加的两个NS记录请不要修改。NS向下授权，填写dns域名，例如：f1g1ns1.dnspod.net
    SRV记录：不常用。格式为：优先级、空格、权重、空格、端口、空格、主机名，记录生成后会自动在域名后面补一个“.”，这是正常现象。例如：5 0 5269 xmpp-server.l.google.com.

### MX记录

> MX优先级，用来指定邮件服务器接收邮件的先后顺序（1-50），一般默认设置为5、10、15等

### TTL

> 阿D提示您：我们默认的 600 秒是最常用的，不用修改

    即 Time To Live，缓存的生存时间。指地方dns缓存您域名记录信息的时间，缓存失效后会再次到DNSPod获取记录值。

    600（10分钟）：建议正常情况下使用 600。
    60（1分钟）：如果您经常修改IP，修改记录一分钟即可生效。长期使用 60，解析速度会略受影响。
    3600（1小时）：如果您IP极少变动（一年几次），建议选择 3600，解析速度快。如果要修改IP，提前一天改为 60，即可快速生效。

### Ref

- https://www.dnspod.cn/

### End

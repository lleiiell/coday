## vim - 专注而生

### shiftwidth

auto intend width

### ab - 缩写

- `:ab mail mail@provider.org` // Define mail as abbreviation of mail@provider.org
- :unab mail // cancel
- :ab // show ab

### how to execute an external command

type  `:!`  followed by an external command to execute that command

### the substitute command

1. Type **:s/old/new/g** to substitute 'new' for 'old'
2. Adding the **g** flag means to substitute  
   globally in the line, change all occurrences of "thee" in the line.
3. type **:#,#s/old/new/g** where #,# are the line numbers of the range  
                              of lines where the substitution is to be done.
4. Type **:%s/old/new/g** to change every occurrence in the whole file.
5. Type **:%s/old/new/gc** to find every occurrence in the whole file,  
                               with a prompt whether to substitute or not.

### go back

> To go back to where you came from press  `CTRL-O`  (Keep Ctrl down while 
>  pressing the letter o).  Repeat to go back further.  `CTRL-I` goes forward

### replace change 

`r word`

### To change until the end of a word

`ce`

### zf/zo

折叠/展开（需加方向键）

### %

匹配括号移动，包括 (, {, [

### nG或者:n

移到文件的第n行

### find/replace

`/` (查找)  
`?` (反查)

### 底行模式

`:`

### :f filename

- 改变编辑中的文件名(file)
- 相当于复制了一个文件，执行这个命令后，新编辑不会应用于原文件

### :r filename

在光标所在处插入一个文件的内容(read) 可加入数字  
`:nr filename`

### :n1,n2 w ! command

- 将文件中n1行至n2行的内容作为command的输入并执行之，  
若不指定n1，n2，则表示将整个文件内容作为command的输入【注意空格】  
例如  :1,4 w! grep tom

### :%!nl

- 要对包含空行的所有行进行 **编号** (所有行之前插入行号^I)

### vim -x exam.txt

- 新编辑的文件加密，会提示输入密码
- `:X`

### :f 或 Ctrl-g

- 显示文件名,当前光标所在行的行号,总的行数,以及当前行所在文件中的百分比和当前光标所在的列的信息

### ctrl + z  /    fg

- 暂时挂到后台/跳回编辑页面
- :sh - 暂时退出vi到系统下，结束时按Ctrl + d则回到vi。

### :e filename

- 退出当前的并打开另一个文件
- :e 重新载入【类似刷新】
- :e! 放弃修改文件内容，重新载入该文件编辑

### :w /tmp/1

- 既然没法存盘，不想放弃所做的所有修改，先临时存到/tmp/1
- saveas

### :w filename

- save as
- :n1,n2 w filename

### vim -R file

只读方式打开文件

### vimdiff linux.php php.php

比较编辑两个不同的文件

### vim +/pattern + file

edit from match pattern

### vim + vim.php

从结尾开始编辑

### 编辑多文件

- `prev`
- `next`

### vimrc

- 系统 vimrc 文件: "$VIM/vimrc" 
- 用户 vimrc 文件: "$HOME/.vimrc" 

### vim显示匹配个数

- `:%s/xxx//gn` - 关键是最后的n，代表只报告匹配的个数，而不进行实际的替换。

### 查找/替换/正则

- 我们把光标所在的行，把所有单词the，替换成THE，应该是：`:s /the/THE/g`
- 我们把整篇文档的所有的the都替换成THE，应该是： `:%s /the/THE`
- 我们仅仅是把第1行到第10行中的the，替换成THE，应该是: `:1,10 s /the/THE/g`

### search current words

  `*` 

### C+d

半屏阅读

### vimtutor

运行`vimtutor`直到你熟悉了那些基本命令

### 分屏: :split 和 vsplit.

`C+w` - 切屏

### 在所有被选择的行后加上点东西

- `<C-v>`
- 选中相关的行 (可使用 j 或 `<C-d>` 或是 /pattern 或是 % 等……)
- $ 到行最后
- A, 输入字符串，按 ESC。

### 自动缩进

`=` - with C+r

### 把所有的行连接起来

1. `<C-v>` 
2. `J` → 把所有的行连接起来（变成一行）

### 宏录制： qa 操作序列 q, @a, @@

- qa 把你的操作记录在寄存器 a。
- 于是 @a 会replay被录制的宏。
- @@ 是一个快捷键用来replay最新录制的宏。

1. 示例
   - 在一个只有一行且这一行只有“1”的文本中，键入如下命令：

   - qaYp<C-a>q→
   - qa 开始录制
   - Yp 复制行.
   - <C-a> 增加1.
   - q 停止录制.
   - @a → 在1下面写下 2
   - @@ → 在2 正面写下3
   - 现在做 100@@ 会创建新的100行，并把数据增加到 103.

### 块操作: `<C-v>`

      ^ → 到行头
      <C-v> → 开始块操作
      <C-d> → 向下移动 (你也可以使用hjkl来移动光标，或是使用%，或是别的)
      I-- [ESC] → I是插入，插入“--”，按ESC键来为每一行生效。

### 自动提示

在 Insert 模式下，你可以输入一个词的开头，然后按 `<C-p>` 或是 `<C-n>` ，自动补齐功能就出现了……

### 删除所有的内容，直到遇到双引号

`dt"`

### 到下一个为a的字符处

1. `fa`
2. `3fa`
3. `F` - 反方向

### <start position><command><end position>

      0 → 先到行头
      y → 从这里开始拷贝
      $ → 拷贝到本行最后一个字符

### 匹配光标当前所在的单词

1. `*` - next 
2. `#` - prev

### 匹配括号移动

`%` - 包括 (, {, [. （陈皓注：你需要把光标先移到括号上）

### 到下一个单词的开头/结尾

1. `w` - begin
2. `e` - end

> 如果你认为单词是由默认方式，那么就用小写的e和w。默认上来说，一个单词由字母，数字和下划线组成（陈皓注：程序变量）  
> 如果你认为单词是由blank字符分隔符，那么你需要使用大写的E和W。（陈皓注：程序语句）

### 到第 N 行

`NG` - （陈皓注：注意命令中的G是大写的，另我一般使用 : N 到第N行，如 :137 到第137行）

### 100遍

`100i2+esc`

### repeat last command

1. `.`
2. `2.` - repeat twice

### :bn 和 :bp

1. 你可以同时打开很多文件，使用这两个命令来切换下一个或上一个文件。
2. 强行退出所有的正在编辑的文件，就算别的文件有更改。  
:qa!

### 保存并退出

1. // :x
2. // ZZ
3. // :wq

### 另存为

`:saveas <path/to/file>`

### 存盘

:w

### 打开一个文件

`:e <path/to/file>`

### Undo/Redo

u → undo  
<C-r> → redo

### 拷贝当前行

1. yy
2. ddp - 上下行调换位置
3. （陈皓注：p/P都可以，p是表示在当前位置之后，P表示在当前位置之前）

### ibookmark
### 搜索 pattern 的字符串

（陈皓注：如果搜索出多个匹配，可按n键到下一个）  
`/pattern`

### blank 位置跳转

- 到本行第一个不是blank字符的位置（所谓blank字符就是空格，tab，换行，回车等）  
`^`
- 到本行最后一个不是blank字符的位置。  
`g_`

### 到本行行尾

`$`

### 数字零，到行头

`0`

### 替换插入

替换从光标所在位置后到一个单词结尾的字符  
`cw`

### 在光标后插入

`a`

### 功能键

在VIM的Normal模式下，所有的键就是功能键了

### 粘贴剪贴板

`p`

### 删当前光标所在的一个字符

`x`

### jump

      在当前屏幕中
      H 跳到第一行
      M 跳到中间一行
      L 跳到最后一行

### change position

`xp`

### change 2 lines

- `ddp`
- 删除当前行，并把删除的行存到剪贴板里: `dd`

### merge 2 lines

`J`

### delete current to the end

`d$` = `D`

### delete current line to the begin

`d^`

### copy to the end

- `y$`
- `y^`
- copy n lines: `nyy`

### :ab string strings 

快捷输入

### :set nu

1. set number
2. set no line number: `:set nonu`

### replace one char

      单个字符替换用 r，
      覆盖多个字符用 R，
      用多个字符替换一个字符用 s，
      整行替换用 S 

### find & replace

`:%s/man/men/g` - all lines

### 请求帮助

`:help`

### 重复上次操作

`.`

### 删除单词

1. `dw`
2. `d2e`

### 替换文本

当需要修改某个文本时,可通过 `r` 进行快速的修改与替换.

### 跳转到第二行

`2G`

### */# 

- 通过 `*` 可往后查找光标停留位置相同的词组
- `#` 则是向前查找

> 这是一个例程：Hey, if name fo by if googd of if of

### 于上一行编辑/反之

- `O` - prev line
- `o` - next line

### 行跳转

`：`后直接加数字可以使光标跳到相应的行

### go to header/footer

- `gg` - header
- `G` - footer

### delete a word

`dw`

### 撤销与反撤销

- `u` - back
- `ctrl+r` - reset

### copy

`2yy` - copy 2 lines;

### paste

`p` - paste;  
`P` - upper p, paste upper;

### delete

`2dd` - delete or cut;

### Reference

http://coolshell.cn/articles/5426.html 简明 Vim 练级攻略

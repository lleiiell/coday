### computer english - 计算机英语

- A - ipv4 地址
- AAAA - ipv6 地址
- CNAME - 别名
- MDA - mail deliver agent - 邮件保存代理
- MRA - mail recieve agent - 邮件接收代理
- MTA - mail transfer agent - 邮件传输代理
- MX - 邮件服务
- MUA - mail user agent - 邮件客户端
- PTR - 指针（逆向解析）
- spam(stupid pointless annoying messages) - 垃圾邮件 
- SRV - 服务资源
- virus - 病毒

### word - 单词

- arithmetic - 算术
- asymmetric - 非对称的
- backslash - 反斜杠
- bonus - 奖金，红利
- brace - 花括号，大括号
- encryption - 加密
- foreground - 前台
- fork - 叉；餐叉
- Nanjing University of Finance - 南京财经大学
- override - 覆盖
- parameter - 参数
- pathname - 路径名
- principle - 原理，原则
- privileges - 特权
- processes - 进程
- regexp - 正则表达式
- repositories - 贮藏室
- signal - 信号
- sissy - 胆小鬼，娘娘腔
- snapshot - 快照
- substitute - 代替者
- substitution - 代入
- suspended - 暂停的
- synchronization - 同步
- syntax - 语法
- tilde - 波浪线
- transmission - 传输
- typical - 典型的
- variable - 变量
- wildcards - 通配符

### phrase - 短语, 词组

- first name 名 - Sean
- last name 姓 - Don
- summing up - 总结
- up to you - 由你做主

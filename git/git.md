### 创建远程分支

- git checkout -b develop
- git push origin develop #first time
- git push  #later

### log

- `git log --pretty="%h - %s" --author=gitster --since="2008-10-01" --before="2008-11-01" --no-merges -- t/ `

      选项  说明
      -(n)    仅显示最近的 n 条提交
      --since, --after    仅显示指定时间之后的提交。
      --until, --before   仅显示指定时间之前的提交。
      --author    仅显示指定作者相关的提交。
      --committer 仅显示指定提交者相关的提交。

#### git log --since=2.weeks

你可以给出各种时间格式，比如说具体的某一天（“2008-01-15”），
或者是多久以前（“2 years 1 day 3 minutes ago”）。

#### git log

      选项  说明
      -p  按补丁格式显示每个更新之间的差异。
      --stat  显示每次更新的文件修改统计信息。
      --shortstat 只显示 --stat 中最后的行数修改添加移除统计。
      --name-only 仅在提交信息后显示已修改的文件清单。
      --name-status   显示新增、修改、删除的文件清单。
      --abbrev-commit 仅显示 SHA-1 的前几个字符，而非所有的 40 个字符。
      --relative-date 使用较短的相对时间显示（比如，“2 weeks ago”）。
      --graph 显示 ASCII 图形表示的分支合并历史。
      --pretty    使用其他格式显示历史提交信息。
      可用的选项包括 oneline，short，full，fuller 和 format（后跟指定格式）。

#### git log --pretty=format:"%h - %an, %ar : %s"

- ca82a6d - Scott Chacon, 11 months ago : changed the version number

```
选项   说明
%H  提交对象（commit）的完整哈希字串
%h  提交对象的简短哈希字串
%T  树对象（tree）的完整哈希字串
%t  树对象的简短哈希字串
%P  父对象（parent）的完整哈希字串
%p  父对象的简短哈希字串
%an 作者（author）的名字
%ae 作者的电子邮件地址
%ad 作者修订日期（可以用 -date= 选项定制格式）
%ar 作者修订日期，按多久以前的方式显示
%cn 提交者(committer)的名字
%ce 提交者的电子邮件地址
%cd 提交日期
%cr 提交日期，按多久以前的方式显示
%s  提交说明
```

#### git log --pretty=oneline

指定使用完全不同于默认格式的方式展示提交历史

#### git log --stat

显示简要的增改行数统计

#### git log -p -2

- -p 选项展开显示每次提交的内容差异，用 -2 则仅显示最近的两次更新

#### git log --pretty=format:"%h %s" --graph

开头多出一些 ASCII 字符串表示的简单图形

#### git log --pretty=format:"%h - %an "

ca82a6d - Scott Chacon, 11 months ago : changed the version number

#### git log --stat -2

显示简要的增改行数统计

#### git log -p -2

`-p` 选项展开显示每次提交的内容差异，用 `-2` 则仅显示最近的两次更新

#### who changed this file?

- git log a/b/c.php

- I was also looking for the history of files 
that were previously renamed and found this thread first. 
The solution is to use "git log --follow <filename>" 
as Phil pointed out here. – Florian Gutmann Apr 26 '11 at 9:05

- with diff  
`git log -p filename`  
`git whatchanged -p database/a/b.sql`

- see when a specific line of code inside a file was changed  
`git blame database/a/b.sql`

- git log --follow -p file

### git push new remote

- git remote add origin git@github.com:foo/bar.git
- git push origin master


### rename branch

git branch -m 1246 1246-table

### git diff HEAD^^

前两次提交diff

### git set url

git remote set-url origin git@gitcafe.com...

### git tag -l '2.*'

```
1.
1.0
```

### 显示对应的克隆地址

git remote -v

### 对文件改名
 
- git mv filefrom fileto

### commit

- git commit -a -m 'added new benchmarks'

### pull

- git pull --rebase origin master

### remote

- git remote rm origin
- git remote add origin git@gitcafe.com..

#### git remote rename pb paul

rename remote;

#### git remote show origin

查看某个远程仓库的详细信息

#### git remote -v

显示对应的克隆地址

### config

- git config --list
- git config color.ui true
- git config --global user.name Sean
- git config user.name
- git config -e

#### set alias

- `g config alias.s "status"`

### .gitignore

```
*.[oa] // 忽略所有以 .o 或 .a 结尾的文件

hello/try 
hello/try/ (diff)

*~ // 忽略所有以波浪符（~） 结尾的文件

// 忽略所有 .a 结尾的文件, 但 lib.a 除外
*.a
!lib.a
```

#### can you ignore me?

- 已经有的文件, 添加进git ignore? - temp.php
- 没有的文件, temp2.php 添加进git ignore? - temp2.php

### add git repository

- mkdir gitrepo
- cd gitrepo
- git init
- git clone ssh://sean@192.../sean/../.git/
- updating the current branch in a non-bare repository is denied

   - git config --bool core.bare true
   - http://stackoverflow.com/questions/2816369/git-push-error-remote-rejected-master-master-branch-is-currently-checked

### Conflict

```bash
<<<<<<< HEAD
- Hello world, This is root;
=======
- Good morning!
>>>>>>> up by sean
```

### git删除远程分支

- `git push origin --delete develop`

### end

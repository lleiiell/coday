## getweb setup

### gitweb

- yum install gitweb
- vi /etc/gitweb.conf

```
# Change This
$projectroot = '/var/www/git';
# Change This
$site_name = "my gitweb";
# Don't Change the variables below
$my_uri = "/";
$home_link = '/';
@stylesheets = ("/gitweb.css");
$favicon = "/git-favicon.png";
$logo = "/git-logo.png";
```

### git repository configuration

- vi .git/description - add description
- vi .git/config

```
[gitweb]
   owner = "Sean"
```

- `cp -r /home/sean/project/coday/.git /var/www/git/coday.git`

### virtualhost

- vi /etc/httpd/conf/httpd.conf

```
<VirtualHost *:80>
    ServerAdmin zeentang@gmail.com
    DocumentRoot /var/www/git
    ServerName git.llei.com
</VirtualHost>
```

- service httpd restart

### hosts

- vi /etc/hosts
- 127.0.0.1 git.llei.com

### success

- visit http://git.llei.com

### Ref

- http://gofedora.com/how-to-install-configure-gitweb/

### end

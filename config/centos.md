## centos setup

### install guest additions

- http://www.lleiiell.info/post/2013-08-21/40052484356

### git

`yum install git`

### zsh

`yum install zsh`

#### oh-my-zsh

- ZSH_THEME="jreese"
- chsh -s /bin/zsh
- https://github.com/robbyrussell/oh-my-zsh
- restart (install autojump needs)

### autojump

- https://github.com/joelthelion/autojump

### vim

#### .vimrc

#### vundle

- https://github.com/gmarik/vundle

#### vim markdown

- https://github.com/plasticboy/vim-markdown

### visudo 

- sean    ALL=(ALL)       ALL

### ibus

- system | preference | input method

### repoforge

- http://repoforge.org/
- wget ..
- rpm install
- yum clean all - clean cache

### php env

#### httpd

- `yum install httpd`
- `chkconfig --levels 235 httpd on`

- `tail -f /var/log/httpd/error_log` - error log

#### mysql

- `yum install mysql mysql-server`
- `service mysqld start`
- `mysql_secure_installation`
- `chkconfig --levels 235 mysqld on` - onboot

#### php

- `yum install php`
- `yum search php`
- `yum install php-mysql` - add packet to use mysql  
php-common php-mbstring php-gd php-imap php-ldap php-odbc php-pear php-xml php-xmlrpc

- http://www.webtatic.com/packages/php55/

#### phpMyAdmin

- `unzip ..`
- `setenforce 0` - close selinux
- `yum install php-mbstring`
- `mv phpMyAdmin /usr/share/phpMyAdmin`
- `cp config.sample.inc.php config.inc.php`

- `vi /etc/httpd/conf.d/phpmyadmin.conf`

```
Alias /phpmyadmin /usr/share/phpMyAdmin
Alias /phpMyAdmin /usr/share/phpMyAdmin
```

- `/etc/init.d/httpd restart`

### svn

- `yum install subversion`

### centos pptp

- rpm -q  iptables - check if installed
- rpm -q  perl

`iptables -t nat -A POSTROUTING -o eth0 -s 192.168.0.0/24 -j MASQUERADE`

- install pptp server
 - `rpm -i http://poptop.sourceforge.net/yum/stable/rhel6/pptp-release-current.noarch.rpm`
 - `yum -y install pptpd`
- 安装ppp和iptables
 - `yum install  -y perl ppp iptables`
- `vim /etc/ppp/options.pptpd`
 - options.pptpd内容如下：

```bash
name pptpd
refuse-pap
refuse-chap
refuse-mschap
require-mschap-v2
require-mppe-128
proxyarp
lock
nobsdcomp
novj
novjccomp
nologfd
# from here
idle 2592000
ms-dns 8.8.8.8
ms-dns 8.8.4.4
# 解析：ms-dns 8.8.8.8， ms-dns 8.8.4.4是使用google的dns服务器。
```

- `vim /etc/ppp/chap-secrets`
 - `myusername     pptpd       mypassword       * `
 - myusername是你的vpn帐号，mypassword是你的vpn的密码，* 表示对任何ip，记得不要丢了这个星号。

- `vim /etc/pptpd.conf`
 - `option /etc/ppp/options.pptpd`
 - `localip 192.168.0.1`
 - `remoteip 192.168.0.234-238,192.168.0.245`
 - 关键点：pptpd.conf这个配置文件必须保证最后是以空行结尾才行，否则会导致启动pptpd服务时，出现“Starting pptpd”， 一直卡着不动的问题，无法启动服务，切记呀！

- `vim /etc/sysctl.conf`
 -  //修改内核设置，使其支持转发
 - 将 `net.ipv4.ip_forward = 0`   改成   `net.ipv4.ip_forward = 1`
 - `#/sbin/sysctl -p`  or  `sysctl -p`

- 启动pptp vpn服务和iptables
 - `#/sbin/service pptpd start`    或者   `#service pptpd start`

- 经过前面步骤，我们的VPN已经可以拨号登录了，但是还不能访问任何网页。最后一步就是添加iptables转发规则了
 - 启动iptables和nat转发功能，很关键的呀
 - `#/sbin/service iptables  start`   //启动iptables
 - `#/sbin/iptables -t nat -A POSTROUTING -o eth0 -s 192.168.0.0/24 -j MASQUERADE`
 - `#/etc/init.d/iptables save`   //保存iptables的转发规则
 - `#/sbin/service iptables restart`    //重新启动iptables

- 最后一步：重启pptp vpn
 - `#/sbin/service pptpd retart`    或者   #service pptpd restart

- 客户端的我就不写了，大家可以自行google。

#### Ref 

- http://www.ksharpdabu.info/centos6-4-structures-pptp-vpn.html#comment-1009
- https://www.digitalocean.com/community/articles/how-to-setup-your-own-vpn-with-pptp

### Nginx

#### Install Nginx, PHP 5.5.4 and PHP-FPM

-  rpm -Uvh http://download.fedoraproject.org/pub/epel/6/x86_64/epel-release-6-8.noarch.rpm
-  rpm -Uvh http://rpms.famillecollet.com/enterprise/remi-release-6.rpm

#### Create file /etc/yum.repos.d/nginx.repo and add following content to repo file

```bash
[nginx]
name=nginx repo
baseurl=http://nginx.org/packages/centos/$releasever/$basearch/
gpgcheck=0
enabled=1
```

#### Install Nginx, PHP 5.5.4 and PHP-FPM

- `yum --enablerepo=remi,remi-php55 install nginx php-fpm php-common`

#### Install PHP 5.5.4 modules

- `yum --enablerepo=remi,remi-php55 install php-pecl-apc php-cli php-pear php-pdo php-mysqlnd php-pgsql php-pecl-mongo php-pecl-memcache php-pecl-memcached php-gd php-mbstring php-mcrypt php-xml`

#### Stop httpd (Apache) server, Start Nginx HTTP server and PHP-FPM (FastCGI Process Manager)

```bash
/etc/init.d/httpd stop
## OR ##
service httpd stop
```

- Start Nginx

```bash
/etc/init.d/nginx start ## use restart after update
## OR ##
service nginx start ## use restart after update
```

- Start PHP-FPM

```bash
/etc/init.d/php-fpm start ## use restart after update
## OR ##
service php-fpm start ## use restart after update
```

#### Autostart Nginx and PHP-FPM on boot, also prevent httpd (Apache) autostarting on boot

- `chkconfig httpd off`

```bash
chkconfig --add nginx
chkconfig --levels 235 nginx on
chkconfig --add php-fpm
chkconfig --levels 235 php-fpm on
``

#### Configure Nginx and PHP-FPM

- Create directory layout for your site

```bash
## public_html directory and logs directory ##
mkdir -p /srv/www/testsite.local/public_html
mkdir /srv/www/testsite.local/logs
chown -R apache:apache /srv/www/testsite.local

```

#### Create and configure virtual host directories under /etc/nginx

- mkdir /etc/nginx/sites-available
- mkdir /etc/nginx/sites-enabled

- Add following lines to /etc/nginx/nginx.conf file, after “include /etc/nginx/conf.d/*.conf” line (inside http block).

```bash
## Load virtual host conf files. ##
include /etc/nginx/sites-enabled/*;

```

#### Create testsite.local virtual host file

- Add following content to /etc/nginx/sites-available/testsite.local file. This is very basic virtual host config.

```bash
server {
    server_name testsite.local;
    access_log /srv/www/testsite.local/logs/access.log;
    error_log /srv/www/testsite.local/logs/error.log;
    root /srv/www/testsite.local/public_html;
 
    location / {
        index index.html index.htm index.php;
    }
 
    location ~ \.php$ {
        include /etc/nginx/fastcgi_params;
        fastcgi_pass  127.0.0.1:9000;
        fastcgi_index index.php;
        fastcgi_param SCRIPT_FILENAME /srv/www/testsite.local/public_html$fastcgi_script_name;
    }
}
```

#### Link your virtual host to /etc/nginx/sites-enabled

```bash
cd /etc/nginx/sites-enabled/
ln -s /etc/nginx/sites-available/testsite.local
service nginx restart
```

#### Add your testsite.local “domain” to /etc/hosts file

- `127.0.0.1               localhost.localdomain localhost testsite.local`

- And if you use another machine where you are running your Nginx server, then add it’s public IP as following:

```bash
10.0.2.19               wordpress
```

#### Test your Nginx and PHP-FPM setup

- Create /srv/www/testsite.local/public_html/index.php file with following content:

#### Refs

- http://www.if-not-true-then-false.com/2011/install-nginx-php-fpm-on-fedora-centos-red-hat-rhel/

### end


## httpd.conf

### apache module mod_expires

- http://httpd.apache.org/docs/current/mod/mod_expires.html

### Apache Module mod_headers

- Customization of HTTP request and response headers

- http://httpd.apache.org/docs/2.2/mod/mod_headers.html

### Apache 虚拟主机 VirtualHost 配置

- httpd.conf

```bash
# Virtual hosts
Include conf/extra/httpd-vhosts.conf
```

- 打开目录 {Apache2 安装目录}\conf\extra\, 找到 httpd-vhosts.conf 文件.

```bash
#
# DocumentRoot 是网站文件存放的根目录
# ServerName 是网站域名, 需要跟 DNS 指向的域名一致
#
<VirtualHost *:80>
    ServerAdmin webmaster@dummy-host.example.com
    DocumentRoot "D:/workspace/php/demo_neoease_com"
    ServerName demo.neoease.com
    ErrorLog "logs/demo.neoease.com-error.log"
    CustomLog "logs/demo.neoease.com-access.log" common
</VirtualHost>
```

- http://www.neoease.com/apache-virtual-host/

### Listen

- Listen 8082
- localhost:8082

### end

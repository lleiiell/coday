### 360

#### 360 安全卫士

- 漏洞，补丁
- 开机启动

#### 360 安全桌面

- beautify desktop
- wallpaper

### avast

- safety
- http://www.avast.com/

### chrome

- AdBlock
- Adkill and Media Download - kill ads;
- AutoPager Chrome
- Delicious Tools
- Gestures for Google Chrome
- Google Keep
- Google翻译(中英文)
- Holmes 3.1.7 - bookmarks search
- LogMeIn
- Page load time
- PageSpeed Insights (by Google)
- Pocket (formerly Read It Later)
- Proxy SwitchySharp
- Readability 3.0.13
- Remove Google Redirects 1.0.8
- Resolution Test 2.0
- SwiftPreview 2.2.4 - link preview
- Tabs Outliner 0.4.78 - for many tabs
- Unused CSS Selectors 0.8
- YSlow 3.1.2
- 麦库记事·剪集
- 印象笔记·悦读
- 网页截图:注释&批注
- Pocket (formerly Read It Later)

### dropbox

- https://www.dropbox.com

### evernote

- http://www.yinxiang.com/

### everything

- find files
- http://www.voidtools.com/

### firefox

- http://firefox.com.cn/

### foobar

- http://www.foobar2000.org/

### foxmail

- http://foxmail.com.cn/

### git

- http://dl.oschina.net/soft/git/

#### github

- http://windows.github.com/

### green apps

- "C:\Program Files (green)"

### goagent

- https://code.google.com/p/goagent/
- https://appengine.google.com/ - for appid

### libreoffice

- http://libreoffice.org/

### 猎豹

- browser
- http://www.liebao.cn/

### 鲁大师

- pc test

### notepad++

- http://notepad-plus-plus.org/

#### setup

- format: unix
- utf-8(without bom)
- tab: replace by space, tab size 3;
- backup: forbit;
- font: droid sans mono 9;

#### Plugins

- Json viewer 
- Zen Coding

### potplayer

- http://www.potplayer.org/

### putty

- http://www.chiark.greenend.org.uk/~sgtatham/putty/

### 驱动精灵

- pc drivers
- http://www.drivergenius.com/

### skype

- international

### smarthosts

- https://code.google.com/p/smarthosts/
- https://github.com/smarthosts/smarthosts

### QQ

- http://www.qq.com/

### virtualbox

- https://www.virtualbox.org/
- centos/ubuntu

### 维棠

- flv download
- http://www.vidown.cn/

### winscp

- file transmission
- http://winscp.net/

### wps

- word file
- http://www.wps.cn/

### picasa

- http://picasa.google.com/

### 豌豆荚

- For phone APPs;
- http://www.wandoujia.com/

### 一键 ghost

- backup system
- http://doshome.com/yj/

### Sublime Text 

- http://www.sublimetext.com/3 - ST
- https://sublime.wbond.net/ - package control
- 插件安装实例: https://github.com/revolunet/sublimetext – markdown-preview Markdown Preview;

#### Plugins

- Emmets
- Sublime Linter——代码校验插件，支持 HTML、CSS、JS、PHP、Java、C++ 等16种语言；
- BracketHighlighter——括号高亮匹配；
- Git——整合 Git 功能的插件；
- Pretty JSON——JSON美化扩展；

### end

### ssh

- `sudo apt-get install openssh-server`
- `ps -e | grep ssh` - check ssh if started
- `/ etc/ssh/sshd_config` - config is here
- `sudo /etc/init.d/ssh restart` - restart sshd service

## Linux

### ab

- `which ab`
- `./ab -n 3000 -c 3000 http://www.test.com/`

### Apache subdomain

- At windows
- `127.0.0.1 www.sean.com news.sean.com`
- `LoadModule rewrite_module modules/mod_rewrite.so`

```bash
<VirtualHost *:80>
   DocumentRoot "G:\www32\apache2.2.25\htdocs\sean" 
   ServerName www.sean.com
   ServerAlias *.sean.com
   <Directory "G:\www32\apache2.2.25\htdocs\sean">
       Options Indexes FollowSymLinks
       AllowOverride all
       Order Deny,Allow
       Deny from all
       Allow from All
   </Directory>
</VirtualHost>
```
- restart apache

- .htaccess one - at sean dir

```bash
RewriteEngine on

#让www.sean.com 访问根目录

RewriteCond %{HTTP_HOST} ^www.sean.com$
RewriteCond %{REQUEST_URI} !^/
RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d
RewriteRule ^(.*)$ /$1
RewriteCond %{HTTP_HOST} ^www.sean.com$
RewriteRule ^(/)?$ /index.php [L]

#让news.sean.com 访问news目录

RewriteCond %{HTTP_HOST} ^news.sean.com$
RewriteCond %{REQUEST_URI} !^/news/
RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d
RewriteRule ^(.*)$ /news/$1
RewriteCond %{HTTP_HOST} ^news.sean.com$
RewriteRule ^(/)?$ news/index.php [L]
```

- .htaccess two - at news dir

```bash
RewriteEngine On
RewriteBase /
RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d
ErrorDocument 403 /404.htm
ErrorDocument 404 /404.htm
```

- ok;

### iptables

#### 清空防火墙(iptables)的默认规则后，重启又会出现，肿么会这样？

> 配置后，没保存吧，/etc/init.d/iptables save 
> 或者直接编辑vi /etc/sysconfig/iptables 我喜欢这种

### linux下一行插入

`o`  
`O` - 上一行插入

### mv

- `mv log LOG.log` -  change file name

### sh

`#!/bin/sh`  
编译 `chmod +x filename` 这样才能用, `./filename` 来运行

### 变量

在 shell 编程中，所有的变量都由字符串组成，并且不需要对变量进行声明。  

```bash
#!/bin/sh
#对变量赋值：
a=”hello world”
# 现在打印变量a的内容：
echo “A is:”
echo $a
```

`echo “this is the ${num}nd”` - **{}**

### centos shutdown

- `halt`
- `poweroff`
- `shutdown -h now/10/10:10` halt
- `shutdown -r now/10/10:10` reboot
- `reboot`

### centos 开机自动联网

- edit **/etc/sysconfig/network-scripts/ifcfg-eth0**
- `onboot=yes`

```
   DEVICE=eth0                 //指出设备名称
   BOOTPROTO=static            //启动类型 dhcp|static
   BROADCAST=192.168.1.255     //广播地址
   HWADDR=00:06:5B:FE:DF:7C    //硬件Mac地址
   IPADDR=192.168.0.2          //IP地址
   NETMASK=255.255.255.0       //子网掩码
   NETWORK=192.168.0.0         //网络地址
   GATEWAY=192.168.0.1         //网关地址
   ONBOOT=yes                  //是否启动应用
   TYPE=Ethernet               //网络类型
```

### yum

- `yum install php-fpm` - install

#### grouplist

- `yum grouplist | grep -i php` - *grouplist*

#### groupinstall
- `sudo yum groupinstall 'PHP Support'` - *php support*
- `sudo yum groupinstall "Development Tools"` - *groupinstall*

#### info
- `yum info php` - *info*

#### list
- `yum list | grep -i PCRE` - *list*

### php

- install: `yum install php`

#### setup

- vi *etc/php.ini*

```php
memory_limit = 128M
max_execution_time = 120
max_upload_size = 50M
error_reporting
display_errors = On
```

### httpd - apache

- install `yum install httpd`

#### setup httpd.conf

- `/etc/httpd/conf/httpd.conf`
- DocumentRoot "/var/www/html"
- Order deny,allow
- AllowOverride All

#### chown

- chown -R apache joomla

### mysql

- `yum install mysql-server mysql php-mysql` - install mysql
- `chkconfig --levels 235 mysqld on` - Set the MySQL service to start on boot
- `service mysqld start` - Start the MySQL service
- `mysql -u root` - Log into MySQL

#### mysql 安全设置

- `/usr/local/mysql/bin/mysqladmin -u root password “upassword”` //使用mysqladmin
- mysql> use mysql;
- mysql> update user set password=password('upassword') where user='root';
- mysql> flush privileges; //强制刷新内存授权表，否则用的还是在内存缓冲的口令

#### mysql_secure_installation

- `mysql_secure_installation`
- Set root password
- Remove anonymous users
- Disallow root login remotely
- Remove test database and access to it
- Reload privilege tables

#### change mysql password

- mysqladmin -u root -p password newpasswd

#### create utf-8 db

- CREATE DATABASE `wiki` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
- 注意后面这句话 “COLLATE utf8_general_ci“,大致意思是在排序时根据utf8编码格式来排序 

### chkconfig 

- set to run on startup

- `vi /etc/rc.d/rc.local`
- `chkconfig httpd on/off`
- `chkconfig --list`
- `chkconfig --list httpd`
- 这个时候2~5都是on，就表明会自动启动了。

### cat

将几个文件合并为一个文件: `cat file1 file2  > file`

### autojump

- directory to jump to

- ./install.sh
- ./uninstall.sh
- `j --stat` - Show database entries and their respective key weights

#### errors

- `/etc/profile.d/autojump.bash:13: command not found: complete` - 不要用sudo安装

### zsh

- the Z shell 

- `yum install zsh`
- `chsh`

### systemctl

#### restart nginx service

- systemctl restart nginx.service

#### restart httpd service

- systemctl restart httpd.service

#### restart php-fpm service

- systemctl restart php-fpm.service

### 生成ssh

- `ssh-keygen -t rsa`
- `-t` = type - Specifies the type of key to create

### search history command

`ctrl-r`

### 清除命令行中光标所处位置之前的所有字符

```
ctrl + u
ctrl-w - 清除左边的字段
ctrl-y - 将会贴上被ctrl-u 或者 ctrl-k 或者 ctrl-w 清除的部分。
ctrl-k - 清除从提示符所在位置到行末尾之间的字符。
```

### 把光标移动到命令行最开始的地方

- `ctrl + a`
- 把光标移动到命令行末尾: `ctrl + e`

### chage

- set user password lifetime
- `chage -E 2005-12-31 user1`

### passwd

- `passwd` - change password of current user
- `passwd user2` - change password of user2

### user

- 查看所有用户 - `cat /etc/passwd` 

#### userdel

- `userdel zhangsan`

#### usermod

- `usermod -l newuser olduser` - 修改用户名

- `usermod -u 777 sean` - modify user id

#### 新建用户

新建用户之初始化内容于目录: `/etc/skel`

#### etc files

- `/etc/passwd, /etc/shadow, /etc/group` 分包保存用户信息, 密码, 组信息

#### whoami

- `whoami` 显示用户名
- `who` 登录用户
- `w` 登录用户, 及在干什么
- `id` 显示用户信息

#### useradd

- `useradd user1` - add one user

#### userdel

- `userdel user1`

### group

- `cat /etc/group | cut -d: -f 1` - show all groups
- `cat /etc/group` - linux 查看所有组

#### groupadd

- `groupadd gname` - 创建一个新用户组

#### groupdel

- `groupdel gname` - delete a group

#### groupmod

- `groupmod -n new_gname old_gname` - rename a group

### ln

- `ln vim.php lib/vim.php` - 创建硬链接

- `ln -fs ~/project/vim.php lib/vim.php` - 创建软链接

### mkdir

- `mkdir -p 1/2/3` - 创建一个目录树

- `mkdir 1 2` - 同时创建两个目录

### tree

- File system tree viewer
- yum install tree

#### 显示目录树形结构

- `tree`
- `tree lib/`

### ls

#### 显示包含数字的文件名和目录名

`ll *[0-9]* `

### df 

- report file system disk space usage
- `df -h`

### ps

- `ps -ef | grep weibo` - 进程显示

### GNOME

及no么 - 仿mac

### kdump

内存镜像 when 服务器死机;

### el6

- 红帽
- centos is free redhat;

### 2.6.32

linux内核版本

### grub

引导文件

### bootloader

Boot Loader 是在操作系统内核运行之前运行的一段小程序。

### Linux软件依赖关系

装B, 需要已安装A.

### service

#### is apache started?

- `service httpd status`

### netstat

- `sudo netstat -anp | grep 80`

#### Check if port is open or closed on a Linux server using SSH?

`netstat -tuplen`

### bc

启动计算器

### grep

#### 查找指定文件, 指定内容

`grep -rw --include='*.js' requesttoken`

### du

#### linux查看文件夹大小

- `du -sh ../job`
- `-s` summarize - display only a total for each argument
- `-h` = `--human-readable`

### 查看 Linux 已安装软件
- 查看全部  
`rpm -qa`

- `rpm -qa git`
- `rpm -qa | grep git`

### ssh -T git@gitcafe.com

// Hi lleiiell! You've successfully authenticated, but GitCafe does not provide shell access.

### add to the end of file 

`echo 'hello2' >> temp.php`

### Failed to add the host to the list of known hosts

- `sudo chmod 777 /home/sean/.ssh/`
- `sudo chmod 777 /home/sean/.ssh/*`

### linux设置时间

`sudo date --set "05/04/13 16:30"`

### alias

`alias ls='ls -l --color=auto'`

### rm

- `rm -r helpers/`

- `rm -rf softname` - delete soft link

#### delete目录并同时删除其内容

`rm -rf dir1`

### hostname

- hostname piuandme.info
- relogin

### mount

- mkdir share

- mount -t vboxsf share share

### scp

- `scp -C -P 22200 user@host.com:file.sql .`
- http://stackoverflow.com/questions/9427553/how-to-download-a-file-from-server-using-ssh

#### scp not a regular file

- scp -r 

### find

- linux find 目录
- `find / -name svnrepos -type d`

### linux 查看系统版本

- `lsb_release -a`

### linux zip except direction

- `zip -r ../backup/showcase_menu-131205.zip ./* -x .git\* tmp\*`

### linux list directory structure

- `tree -d /proc/self/`

### end

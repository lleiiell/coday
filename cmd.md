### shutdown

- shutdown -s -t 3600 - 计划一小时后关机：
- shutdown -a - 取消关机计划

### 查询mx记录

nslookup -qt=mx lleiiell.info

### 获取本机ip地址等

- ipconfig
- ipconfig /all

### path 环境变量

- `G:\www32\PHP5.4;G:\www32\PHP5.4\ext;`

### kill pid 

- `taskkill /f /pid 7496`

### End

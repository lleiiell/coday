## Love Colors

### Red

- `#AB423F`
- `#600`

### Black

- `#3b3c44`

### Blue

- `#36414d`
- `#a3b8cc`
- `#0b1022`
- `#2d67a3`
  - [https://bitbucket.org/](https://bitbucket.org/)

### Gray

- `#e2e0da`
- `#5A504E`
- `#9BA2B0`

- `#282c34` from https://www.humblebundle.com/

### Yellow

- `#BA6934`
- `#FFCC2F` - http://bower.io/

### Refs

- http://www.liyao.name/2013/05/10-tips-for-learning-a-new-technology.html

### End

## Dict

### 扁平化

> 扁平化完全属于二次元，这个概念最核心的地方就是放弃一切装饰效果，诸如阴影，透视，纹理，渐变等等能做出3D效果的元素一概不用。所有的元素的边界都干净利落，没有任何羽化，渐变，或者阴影。尤其在手机上，更少的按钮和选项使得界面干净整齐，使用起来格外简洁。可以更加简单直接的将信息和事物的工作方式展示出来，减少认知障碍的产生。

### 大光比

- 光比是摄影的重要参数之一，指被摄物体受光面亮度与阴影面亮度的比值。
- 被摄物体在自然光及人工布光条件下，受光面亮度较高，阴影面虽不直接受光（或受光较少），但由于散射光（或辅助光照射）仍有一定亮度。常用“受光面亮度/阴影面亮度”比例形式表示光比。
- 光比还指对象相邻部分亮度之比，被摄体主要部位明亮与阴暗之间的反差。光比大，反差则大，光比小，反差则小。光比的大小，决定着画面明暗反差，形成不同的影调和色调构成。
- 直射光比较容易形成大光比，散射光比较容易形成小光比。

### HDR

- 簡單的認為HDR就是讓照片拍得更好看

- HDR是在攝影中常用到的一種技術，是英文High-Dynamic Range的縮寫，意為“高動態範圍”。

- HDR技術可以克服多數相機傳感器動態範圍有限的缺點，並將圖片色調控制在人眼識別範圍之內。它能將多張曝光不同的照片疊加處理成一張精妙絕倫的圖像。

- HDR簡單的說就是讓你的照片無論高光還是陰影部分細節都很清晰。再說明白點，就是當你拍攝光比比較大的作品時，縮小光比，營造一種高光不過曝，暗調不欠曝。讓亮處的效果鮮亮，而黑暗保留更多的細節，能分辨物體的輪廓和深度，而不是以往的一團黑。

- 就是比如說我們拍攝一張天空很亮的照片，在一般情況下，拍攝的結果要不就是天空過爆，要不就是地面的景物曝光不足。

- 隨著手機性能的提升，現在我們也可以直接在手機上通過拍攝多張曝光不同的照片來實現HDR。

- 就比我們常見iPhone ，當我們開啟HDR功能後，iPhone 就會連拍3張中照片，這三張照片的曝光情況會有不同，分別是欠爆、正常曝光和過曝，然後會將這三張照片合成一張。通過HDR合成後，可以提升照片暗部和亮部的細節表現。

- 在大光比环境下拍摄，普通相机因受到动态范围的限制，不能纪录极端亮或者暗的细节。经HDR程序处理的照片，即使在大光比情况拍摄下，无论高光、暗位都能够获得比普通照片更佳的层次。

- http://blog.palapple.com/2012/11/hdr.html

### less

- The dynamic stylesheet language.

- css本身就是比较简单的语言，使用less反而把简单的东西复杂化了，虽然less加入了模块化的东西，看似方便管理了，可是在多人合作的团队中我不推荐使用，只要团队中的成员都有模块化的思想，并有统一的规范，less其实没有使用的价值

- LESS 做为 CSS 的一种形式的扩展，它并没有减少 CSS 的功能，而是在现有的 CSS 语法之上，添加了许多其它的功能，所以学习 LESS 是一件轻松快乐的事情，让我们一起来学习它吧！

- http://www.lesscss.net/
- http://baike.baidu.com/view/1229034.htm#4

### npm

- Node Package Manager
- 是一个NodeJS包管理和分发工具，已经成为了非官方的发布Node模块（包）的标准。
- 如果你熟悉ruby的gem，Python的pypi、setuptools，PHP的pear，那么你就知道NPM的作用是什么了。
- http://baike.baidu.com/view/2459839.htm#2

### unicode

> Unicode（统一码、万国码、单一码）是一种在计算机上使用的字符编码。它为每种语言中的每个字符设定了统一并且唯一的二进制编码，以满足跨语言、跨平台进行文本转换、处理的要求。1990年开始研发，1994年正式公布。随着计算机工作能力的增强，Unicode也在面世以来的十多年里得到普及。

### UTF-8

> UTF-8（8-bit Unicode Transformation Format）是一种针对Unicode的可变长度字符编码，又称万国码。由Ken Thompson于1992年创建。现在已经标准化为RFC 3629。UTF-8用1到4个字节编码UNICODE字符。用在网页上可以同一页面显示中文简体繁体及其它语言（如日文，韩文）

### X理论和Y理论(Theory X and Theory Y)

- X理论把人的行为视为机器，需要外力作用才能产生，Y理论把人视为一个有机的系统，其行为不但受外力影响，而且也受内力影响。这是两种截然不同的世界观价值观。

#### Theory X

- 这是一对完全基于两种完全相反假设的理论，X理论认为人们有消极的工作源动力，而Y理论则认为人们有积极的工作源动力。
- 人类本性懒惰，厌恶工作，尽可能逃避；绝大多数人没有雄心壮志，怕负责任；
- 多数人必须用强制办法乃至惩罚、威胁，使他们为达到组织目标而努力；
- 激励只在生理和安全需要层次上起作用；
- 绝大多数人只有极少的创造力。
- 因此企业管理的唯一激励办法，就是以经济报酬来激励生产，只要增加金钱奖励，便能取得更高的产量。所以这种理论特别重视满足职工生理及安全的需要，同时也很重视惩罚，认为惩罚是最有效的管理工具。

#### Theory y

- 一般人本性不是厌恶工作，如果给予适当机会，人们喜欢工作，并渴望发挥其才能；
- 多数人愿意对工作负责，寻求发挥能力的机会；
- 能力的限制和惩罚不是使人去为组织目标而努力的唯一办法；
- 激励在需要的各个层次上都起作用；
- 想象力和创造力是人类广泛具有的。
- 因此，人是“自动人”。激励的办法是：扩大工作范围；尽可能把职工工作安排得富有意义，并具挑战性；工作之后引起自豪，满足其自尊和自我实现的需要；使职工达到自己激励。只要启发内因，实行自我控制和自我指导，在条件适合的情况下就能实现组织目标与个人需要统一起来的最理想状态。

### Refs

- http://baike.baidu.com/view/40801.htm unicode
- http://baike.baidu.com/view/25412.htm UTF-8

### End

## BudgetVM

### ping

#### ping myip

    64 bytes from 192.157.220.67: icmp_seq=1 ttl=52 time=201 ms
    64 bytes from 192.157.220.67: icmp_seq=2 ttl=52 time=204 ms
    64 bytes from 192.157.220.67: icmp_seq=3 ttl=52 time=209 ms
    64 bytes from 192.157.220.67: icmp_seq=4 ttl=52 time=201 ms
    64 bytes from 192.157.220.67: icmp_seq=5 ttl=52 time=192 ms

#### ping aliyun

    64 bytes from aliyun (121.199.20.75): icmp_seq=1 ttl=53 time=63.6 ms
    64 bytes from aliyun (121.199.20.75): icmp_seq=2 ttl=53 time=66.6 ms
    64 bytes from aliyun (121.199.20.75): icmp_seq=3 ttl=53 time=63.9 ms
    64 bytes from aliyun (121.199.20.75): icmp_seq=4 ttl=53 time=66.9 ms
    64 bytes from aliyun (121.199.20.75): icmp_seq=5 ttl=53 time=64.6 ms

#### ping bae

    64 bytes from 61.135.185.83: icmp_seq=1 ttl=55 time=36.1 ms
    64 bytes from 61.135.185.83: icmp_seq=2 ttl=55 time=33.9 ms
    64 bytes from 61.135.185.83: icmp_seq=3 ttl=55 time=43.9 ms
    64 bytes from 61.135.185.83: icmp_seq=4 ttl=55 time=35.4 ms

#### ping digitalocean 1312110950

    64 bytes from 192.241.236.102: icmp_seq=1 ttl=52 time=158 ms
    64 bytes from 192.241.236.102: icmp_seq=2 ttl=52 time=156 ms
    64 bytes from 192.241.236.102: icmp_seq=3 ttl=52 time=158 ms
    64 bytes from 192.241.236.102: icmp_seq=4 ttl=52 time=157 ms
    64 bytes from 192.241.236.102: icmp_seq=5 ttl=52 time=158 ms

### End

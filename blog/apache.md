## Apache

### You don't have permission to access /phpmyadmin on this server.

- vi /etc/httpd/conf.d/phpmyadmin.conf

```bash
<Directory "/var/www/html/phpmyadmin">
    Options Indexes FollowSymLinks MultiViews
    AllowOverride all
        Order Deny,Allow
    Deny from all
    Allow from 127.0.0.1
    Allow from localhost
</Directory>
```

- setenforce 0

### [Sat Sep 07 16:32:58 2013] [warn] _default_ VirtualHost overlap on port 80, the first has precedence

- vi httpd.conf

- NameVirtualHost *:80

- service httpd restart

- Ref  
http://www.cyberciti.biz/faq/warn-_default_-virtualhost-overlap-port80-first-hasprecedence/

### End

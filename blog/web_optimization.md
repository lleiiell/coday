## Web Optimization

### 使用浏览器缓存

- 在 HTTP 标头中为静态资源设置有效期或最长存在时间，可指示浏览器从本地磁盘中加载以前下载的资源而不是从网络中加载。

### MinifyHTML

- Compacting HTML code, 
including any inline JavaScript and CSS contained in it, 
can save many bytes of data and speed up downloading, 
parsing, and execution time.

- https://developers.google.com/speed/docs/best-practices/payload#MinifyHTML

### open Keep-Alive

### Enable compression

- Compressing resources with gzip or deflate can reduce the number of bytes sent over the network.

### Put CSS in the document head

- Moving inline style blocks and i`<link>` elements from the document body to the document head improves rendering performance.

### Combine images using CSS sprites

- Combining images into as few files as possible using CSS sprites reduces the number of round-trips and delays in downloading other resources, 
reduces request overhead, and can reduce the total number of bytes downloaded by a web page.

### Minimize request size

- Keeping cookies and request headers as small as possible ensures that an HTTP request can fit into a single packet.

### Minimize redirects

- Minimizing HTTP redirects from one URL to another cuts out additional RTTs and wait time for users.

### Specify a character set

- Specifying a character set in the HTTP response headers of your HTML documents 
allows the browser to begin parsing HTML and executing scripts immediately.

### Serve scaled images

- Properly sizing images can save many bytes of data.
- https://developers.google.com/speed/docs/best-practices/payload#ScaleImages

### Serve resources from a consistent URL

- It's important to serve a resource from a unique URL, to eliminate duplicate download bytes and additional RTTs.

### Avoid bad requests

- Removing "broken links", or requests that result in 404/410 errors, avoids wasteful requests.

### Combine external CSS

- Combining external stylesheets into as few files as possible cuts down on RTTs and delays in downloading other resources.

### Optimize the order of styles and scripts

- Correctly ordering external stylesheets and external and inline scripts enables better parallelization of downloads 
and speeds up browser rendering time.

### Avoid document.write

- Using document.write() to fetch external resources, especially early in the document, 
can significantly increase the time it takes to display a web page.

```js
document.write('<script src="example.js"><\/script>'); // no

<script src="example.js"></script> // yes
```

### Avoid CSS `@import`

- Using CSS `@import` in an external stylesheet can add additional delays during the loading of a web page.

### Prefer asynchronous resources

- Fetching resources asynchronously prevents those resources from blocking the page load.

### Parallelize downloads across hostnames

- Serving resources from two different hostnames increases parallelization of downloads.

### Serve static content from a cookieless domain

- Serving static resources from a cookieless domain reduces the total size of requests made for a page.

### Refs

- https://developers.google.com/speed/docs/insights/rules - PageSpeed Insights Rules

- https://developers.google.com/speed/docs/best-practices/rules_intro - Web Performance Best Practices

- http://browserdiet.com/ HOW TO LOSE WEIGHT

- https://github.com/zenorocha/browser-diet/wiki/References

### end
